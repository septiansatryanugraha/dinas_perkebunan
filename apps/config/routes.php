<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

/*   === FRONT PANEL ===  */
$route['default_controller'] = "Publik/Web";
$route['404_override'] = 'Default/Not_found';
$route['login-pengajuan'] = 'Opd/Login_opd';

$route['search'] = 'Publik/Web/Cari';

$route['beranda'] = 'Opd/Dashboard';
$route['dashboard-history'] = 'Opd/Dashboard/history';
$route['daftar-akun'] = 'Opd/Register';
$route['proses-daftar'] = 'Opd/Register/submit';

$route['data-rekomendasi'] = 'Opd/Products/home';
$route['ajax-rekomendasi'] = 'Opd/Products/ajax_list';
$route['add-rekomendasi'] = 'Opd/Products/add_dokumen';
$route['save-rekomendasi'] = 'Opd/Products/prosesAdd';
$route['save2-rekomendasi'] = 'Opd/Products/prosesAdd2';
$route['data-history'] = 'Opd/Products/history';
$route['upload-iup/(:any)'] = 'Opd/Products/edit/$1';
$route['add-rekomendasi-lama'] = 'Opd/Products/add_baru';
$route['update-data-rekomendasi'] = 'Opd/Products/prosesUpdate';

$route['profil-user'] = 'Opd/Profil_opd/profil';
$route['update-profil-user'] = 'Opd/Profil_opd/prosesUpdate';

$route['data-sertifikasi'] = 'Opd/Sertifikasi/home';
$route['ajax-sertifikasi'] = 'Opd/Sertifikasi/ajax_list';
$route['get-data-sertifikasi'] = 'Opd/Sertifikasi/ambil_data';
$route['add-sertifikasi'] = 'Opd/Sertifikasi/add_dokumen';
$route['save-sertifikasi'] = 'Opd/Sertifikasi/prosesAdd';
$route['view-sertifikasi/(:any)'] = 'Opd/Sertifikasi/edit/$1';
$route['edit-pengajuan-sertifikasi/(:any)'] = 'Opd/Sertifikasi/edit_pengajuan/$1';
$route['update-data-sertifikasi'] = 'Opd/Sertifikasi/prosesUpdateSertifikasi';
$route['update2-data-sertifikasi'] = 'Opd/Sertifikasi/prosesUpdate';

$route['data-label'] = 'Opd/Pelabelan/home';
$route['ajax-label'] = 'Opd/Pelabelan/ajax_list';
$route['get-data-label'] = 'Opd/Pelabelan/ambil_data';
$route['add-label'] = 'Opd/Pelabelan/add_pengajuan';
$route['save-label'] = 'Opd/Pelabelan/prosesAdd';
$route['edit-pengajuan-label/(:any)'] = 'Opd/Pelabelan/edit_pengajuan/$1';
$route['update-data-label'] = 'Opd/Pelabelan/prosesUpdateLabel';

/*   route modul beban  */
$route['kategori-opd'] = 'Category/Opd/index';
$route['ajax-kategori-opd'] = 'Category/Opd/ajax_list';
$route['add-opd'] = 'Category/Opd/add';
$route['save-kategori-opd'] = 'Category/Opd/prosesAdd';
$route['edit-opd/(:any)'] = 'Category/Opd/edit/$1';
$route['update-kategori-opd'] = 'Category/Opd/prosesUpdate';
$route['delete-kategori-opd'] = 'Category/Opd/hapus';

$route['qrcode-results/(:any)'] = 'WebService/getDataHasil/$1';


/*   === ADMIN PANEL ===  */
$route['login-admin'] = 'Default/Auth';
$route['logout-admin'] = 'Default/Auth/logout';

/*   route modul dashboard  */
$route['dashboard'] = 'Dashboard/Dashboard/index';

/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';
$route['update-profile'] = 'Setting/Profile/update';

/*   route modul beban  */
$route['file-pengajuan'] = 'Category/File_upload/index';
$route['ajax-file-pengajuan'] = 'Category/File_upload/ajax_list';
$route['add-file-pengajuan'] = 'Category/File_upload/add';
$route['save-file-pengajuan'] = 'Category/File_upload/prosesAdd';
$route['edit-file-pengajuan/(:any)'] = 'Category/File_upload/edit/$1';
$route['update-file-pengajuan'] = 'Category/File_upload/prosesUpdate';
$route['delete-file-pengajuan'] = 'Category/File_upload/hapus';

/*   route modul master opd  */
$route['master-opd'] = 'Master/Master_opd/index';
$route['ajax-master-opd'] = 'Master/Master_opd/ajax_list';
$route['edit-master-opd/(:any)'] = 'Master/Master_opd/edit/$1';
$route['update-master-opd'] = 'Master/Master_opd/prosesUpdate';
$route['delete-master-opd'] = 'Master/Master_opd/hapus';

/*   route modul master dokumen  */
$route['master-sertifikasi'] = 'Master/Master_sertifikasi/index';
$route['ajax-master-sertifikasi'] = 'Master/Master_sertifikasi/ajax_list';
$route['edit-sertifikasi/(:any)'] = 'Master/Master_sertifikasi/edit/$1';
$route['update-master-sertifikasi'] = 'Master/Master_sertifikasi/prosesUpdate';
$route['edit-qr/(:any)'] = 'Master/Master_sertifikasi/Editqr/$1';
$route['update-qr'] = 'Master/Master_sertifikasi/prosesUpdateqr';
$route['delete-master-sertifikasi'] = 'Master/Master_sertifikasi/hapus';

/*   route modul master dokumen  */
$route['master-sts'] = 'Master/Master_sts/index';
$route['ajax-master-sts'] = 'Master/Master_sts/ajax_list';
$route['edit-sts/(:any)'] = 'Master/Master_sts/edit/$1';
$route['update-sts'] = 'Master/Master_sts/prosesUpdate';
$route['delete-master-sts'] = 'Master/Master_sts/hapus';

/*   route modul master dokumen  */
$route['master-label'] = 'Master/Master_label/index';
$route['ajax-master-label'] = 'Master/Master_label/ajax_list';
$route['edit-label/(:any)'] = 'Master/Master_label/edit/$1';
$route['update-label'] = 'Master/Master_label/prosesUpdate';
$route['cetak-label/(:any)'] = 'Master/Master_label/edit/$1';
$route['delete-master-label'] = 'Master/Master_label/hapus';

/*   route modul master dokumen  */
$route['master-rekomendasi'] = 'Master/Master_rekomendasi/index';
$route['ajax-master-rekomendasi'] = 'Master/Master_rekomendasi/ajax_list';
$route['edit-rekomendasi/(:any)'] = 'Master/Master_rekomendasi/edit/$1';
$route['update-rekomendasi'] = 'Master/Master_rekomendasi/prosesUpdate';
$route['delete-rekomendasi'] = 'Master/Master_rekomendasi/hapus';

/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['ajax-user'] = 'Setting/User/ajax_list';
$route['add-user'] = 'Setting/User/Add';
$route['save-user'] = 'Setting/User/prosesTambah';
$route['edit-user/(:any)'] = 'Setting/User/edit/$1';
$route['update-user'] = 'Setting/User/prosesUpdate';
$route['delete-user'] = 'Setting/User/hapus';

/*   route modul menu  */
$route['menu'] = 'Setting/Menu/index';
$route['ajax-menu'] = 'Setting/Menu/ajax_list';
$route['menu-ajax'] = 'Setting/Menu/menu_ajak';
$route['add-menu'] = 'Setting/Menu/Add';
$route['save-menu'] = 'Setting/Menu/prosesTambah';
$route['edit-menu/(:any)'] = 'Setting/Menu/edit/$1';
$route['update-menu'] = 'Setting/Menu/prosesUpdate';
$route['delete-menu'] = 'Setting/Menu/hapus';
$route['icon'] = 'Setting/Menu/icon';

/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/add';
$route['save-grup'] = 'Setting/Grup/prosesTambah';
$route['edit-grup/(:any)'] = 'Setting/Grup/edit/$1';
$route['update-grup'] = 'Setting/Grup/prosesUpdate';
$route['delete-grup'] = 'Setting/Grup/hapus';

/*    route modul Akses  */
$route['hak-akses/(:any)'] = 'Setting/Akses/hak_akses/$1';
$route['update-hak-akses/(:any)'] = 'Setting/Akses/update_hak_akses/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */