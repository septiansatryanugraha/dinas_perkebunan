<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class WebService extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_service');
    }

    public function index()
    {
        echo "ini harusnya muncul";
    }

    public function checklogin()
    {
        $data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
        );
        $res = $this->M_service->login($data);

        $user = $this->input->post('email');

        $data2 = array(
            'last_login_user' => date('Y-m-d H:i:s')
        );

        $this->M_service->update_user($user, $data2);

        if ($res) {
            if ($res[0]->status == "Non aktif") {
                $result = array(
                    'status' => 'false',
                    'pesan' => 'Maaf akun belum aktif',
                );
                echo json_encode($result);
            } else {
                $result = array(
                    'status' => 'true',
                    'pesan' => 'Terima kasih silahkan Login',
                    'Profile' => $res,
                );
                echo json_encode($result);
            }
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'Maaf email atau password anda salah',
            );
            echo json_encode($result);
        }
    }

    // registrasi user
    public function daftar()
    {

        $date = date('Y-m-d H:i:s');
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'nama' => $this->input->post('nama_lengkap'),
            'email' => $this->input->post('email'),
            'password' => base64_encode($this->input->post('password')),
            'status' => "Non aktif",
            'created_date' => $date,
            'updated_date' => $date,
        );

        $checkuser = $this->M_service->checkUsername($data);
        if (!$checkuser) {
            $res = $this->db->insert('tbl_user', $data);
            $result = array(
                'status' => 'true',
                'pesan' => 'Selamat pendaftaran berhasil !!',
            );
            echo json_encode($result);
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'Email sudah terdaftar, gunakan Email yang lain !!',
            );
            echo json_encode($result);
        }
    }

    public function selekSertifikasi()
    {
        $id_user = $this->input->post('id_user');
        $res = $this->M_service->getSertifikasi($id_user);
        if ($res) {
            $result = array(
                'status' => 'true',
                'data' => $res
            );
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'Data masih kosong, belum pernah melakukan pengajuan'
            );
        }
        echo json_encode($result);
    }

    public function prosesSimpanSertifikasi()
    {

        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');
        $kode2 = $this->M_service->kode();

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "berkas_kelengkapan_" . $new_name;
        $config['upload_path'] = "./upload/kelengkapan_sertifikasi/";
        $config['allowed_types'] = 'pdf|PDF|jpg|JPG|png|PNG|jpeg|JPEG';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $this->upload->do_upload('surat_permohonan_sertifikat');
        $surat_permohonan_sertifikat = $this->upload->data();
        $data_surat_permohonan_sertifikat = $surat_permohonan_sertifikat;

        $this->upload->do_upload('surat_benih');
        $surat_benih = $this->upload->data();
        $data_surat_benih = $surat_benih;

        $this->upload->do_upload('gambar');
        $gbr_kegiatan = $this->upload->data();
        $data_gbr_kegiatan = $gbr_kegiatan;

        $path['link'] = "upload/kelengkapan_sertifikasi/";
        $Kode = $this->input->post('kode_rekomendasi');
        $Kode_iup = $this->input->post('kode_iup');

        $nama_pemohon = $this->input->post('nama_pemohon');
        $kode_rekomendasi = $this->input->post('kode_rekomendasi');
        $Komoditas = $this->input->post('jenis_komoditas');

        $data = array(
            'kode' => $kode2,
            'id_user' => $this->input->post('id_user'),
            'kode_iup' => $this->input->post('kode_iup'),
            'nama_surat_pemohon' => $data_surat_permohonan_sertifikat['file_name'],
            'surat_permohonan_sertifikat' => $path['link'] . '' . $data_surat_permohonan_sertifikat['file_name'],
            'nama_surat_benih' => $data_surat_benih['file_name'],
            'surat_benih' => $path['link'] . '' . $data_surat_benih['file_name'],
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'alamat_pemohon' => $this->input->post('alamat_pemohon'),
            'nama_usaha' => $this->input->post('nama_usaha'),
            'alamat_usaha' => $this->input->post('alamat_usaha'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'jenis_usaha' => $this->input->post('jenis_usaha'),
            'jenis_komoditas' => $this->input->post('jenis_komoditas'),
            'status' => 'Pengajuan',
            'created_date' => $date,
            'updated_date' => $date,
            'tanggal' => $date2
        );

        $data2 = array(
            'kode' => $kode2,
            'kode_iup' => $this->input->post('kode_iup'),
            'id_user' => $this->input->post('id_user'),
            'status' => 'Pengajuan',
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan proses pengajuan Sertifikasi dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur dengan Kode IUP<b> ' . $Kode_iup . '</b>',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $latitude = $this->input->post('latitude');
        $jenis_komoditas = $this->input->post('jenis_komoditas');
        $jenis_usaha = $this->input->post('jenis_usaha');

        $data3 = array();
        foreach ($latitude AS $key => $val) {
            $data3[] = array(
                "kode_sertifikasi" => $kode2,
                "latitude" => $_POST['latitude'][$key],
                "longitude" => $_POST['longitude'][$key],
                "gambar" => $path['link'] . '' . $data_gbr_kegiatan['file_name'],
                "jenis_komoditas" => $jenis_komoditas,
                "jenis_usaha" => $jenis_usaha,
                "ket" => $_POST['ket'][$key],
                "created_date" => $date
            );
        }

        $results = $this->db->insert('tbl_sertifikasi', $data);
        $results = $this->db->insert('tbl_history', $data2);
        $results = $this->db->insert_batch('tbl_koordinat', $data3);

        if ($results) {
            $result = array(
                'status' => 'true',
                'pesan' => 'Data berhasil di ajukan !'
            );
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'Maaf data gagal di ajukan !'
            );
        }

        echo json_encode($result);
    }

    public function prosesSimpanLabel()
    {

        $errMessage = "";
        $date = date('Y-m-d');
        $date2 = date('Y-m-d');

        $nama_pemohon = $this->input->post('nama_pemohon');
        $Komoditas = $this->input->post('jenis_komoditas');
        $KodeSertifikasi = $this->input->post('kode_sertifikasi');
        $NoSertifikat = $this->input->post('no_sertifikat');

        $data = array(
            'kode_sertifikasi' => $KodeSertifikasi,
            'id_user' => $this->input->post('id_user'),
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'no_sertifikat' => $this->input->post('no_sertifikat'),
            'alamat_pemohon' => $this->input->post('alamat_pemohon'),
            'nama_usaha' => $this->input->post('nama_usaha'),
            'alamat_usaha' => $this->input->post('alamat_usaha'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'jenis_usaha' => $this->input->post('jenis_usaha'),
            'jenis_komoditas' => $this->input->post('jenis_komoditas'),
            'jumlah_label' => $this->input->post('jumlah_label'),
            'status' => 'Pending',
            'created_date' => $date,
            'updated_date' => $date,
        );

        $data2 = array(
            'kode' => $KodeSertifikasi,
            'id_user' => $this->input->post('id_user'),
            'status' => 'Pengajuan Pelabelan',
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan proses pengajuan Pelabelan dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $checkKode = $this->M_service->checkKode($data);
        if (!$checkKode) {
            $results = $this->db->insert(self::__tableName, $data);
            $results = $this->db->insert(self::__tableName2, $data2);

            if ($results) {
                $result = array(
                    'status' => 'true',
                    'pesan' => 'Data berhasil di ajukan !'
                );
            } else {
                $result = array(
                    'status' => 'false',
                    'pesan' => 'Maaf data gagal di ajukan !'
                );
            }
        } else {

            $result = array(
                'status' => 'false',
                'pesan' => " Maaf Pengajuan dengan Nomor Sertifikat " . $NoSertifikat . "  sudah ada di dalam sistem !! "
            );
        }

        echo json_encode($result);
    }

    public function selekHistory()
    {
        $id_user = $this->input->post('id_user');
        $res = $this->M_service->getHistory($id_user);
        if ($res) {
            $result = array(
                'status' => 'true',
                'data' => $res
            );
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'Data masih kosong, belum ada history pengajuan'
            );
        }
        echo json_encode($result);
    }

    public function getDataHasil($id)
    {
        $KodeRegist = $id;
        $data['brand'] = $this->M_service->getDataHasil($KodeRegist);
        $Result = $this->M_service->getDataHasil($KodeRegist);
        if ($Result > 0) {
            $this->load->view('v_hasil', $data);
        } else {
            echo "Oppsss Page Not Availble";
        }
    }

    function SelekKategory()
    {
        $KategoriUsaha = $this->M_service->selekKatUsaha();
        $KategoriKomoditas = $this->M_service->selekKatKomoditas();

        if ($KategoriUsaha) {
            $result = array(
                'status' => 'true',
                'jenis_usaha' => $KategoriUsaha,
                'komoditas' => $KategoriKomoditas
            );
        } else {
            $result = array(
                'status' => 'false',
            );
        }
        echo json_encode($result);
    }

    function SelekKode()
    {
        $id_user = $this->input->post('id_user');
        $KodeIup = $this->M_service->selekKodeIup($id_user);
        $KodeRegist = $this->M_service->selekKodeRegist($id_user);

        if ($KodeIup) {
            $result = array(
                'status' => 'true',
                'kode_iup' => $KodeIup,
                'kode_registrasi' => $KodeRegist
            );
        } else {
            $result = array(
                'status' => 'false',
            );
        }
        echo json_encode($result);
    }
    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    private function do_upload_surat_images()
    {
        $upath = './upload/berkas_kelengkapan/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['surat']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['surat']['name'] = $files['surat']['name'][$i];
            $_FILES['surat']['type'] = $files['surat']['type'][$i];
            $_FILES['surat']['tmp_name'] = $files['surat']['tmp_name'][$i];
            $_FILES['surat']['error'] = $files['surat']['error'][$i];
            $_FILES['surat']['size'] = $files['surat']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('surat');
        }
    }

    public function UpdateProfile()
    {

        $date = date('Y-m-d H:i:s');
        $where = trim($this->input->post('id_user'));

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "file_cv_" . $new_name;
        $config['upload_path'] = "./upload/cv/";
        $config['allowed_types'] = 'doc|docx';
        $config['max_size'] = '1048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $this->upload->do_upload('file_cv');
        $file_cv = $this->upload->data();
        $data_file_cv = $file_cv;

        $path['link'] = "upload/cv/";

        if ($this->upload->do_upload("file_cv")) {

            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'alamat' => $this->input->post('alamat'),
                'no_surat' => $this->input->post('no_surat'),
                'tgl_surat' => date('Y-m-d', strtotime($this->input->post('tgl_surat'))),
                'latitude' => $this->input->post('latitude'),
                'nama_file_cv' => $data_file_cv['file_name'],
                'file_cv' => $path['link'] . '' . $data_file_cv['file_name'],
                'longitude' => $this->input->post('longitude'),
                'password' => base64_encode($this->input->post("password")),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'telp' => $this->input->post('telp'),
                'updated_date' => $date
            );


            $results = $this->db->update(self::__tableName, $data, array('id_user' => $where));
            $this->do_upload_surat_images();

            if ($results) {
                $result = array(
                    'status' => 'true',
                    'pesan' => 'Data berhasil di update !'
                );
            } else {
                $result = array(
                    'status' => 'false',
                    'pesan' => 'Maaf data gagal di update !'
                );
            }
        } else {

            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'alamat' => $this->input->post('alamat'),
                'no_surat' => $this->input->post('no_surat'),
                'tgl_surat' => date('Y-m-d', strtotime($this->input->post('tgl_surat'))),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'password' => base64_encode($this->input->post("password")),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'telp' => $this->input->post('telp'),
                'updated_date' => $date
            );

            $results = $this->db->update(self::__tableName, $data, array('id_user' => $where));
            $this->do_upload_surat_images();

            if ($results) {
                $result = array(
                    'status' => 'true',
                    'pesan' => 'Data berhasil di update !'
                );
            } else {
                $result = array(
                    'status' => 'false',
                    'pesan' => 'Maaf data gagal di update !'
                );
            }
        }

        echo json_encode($result);
    }
}
