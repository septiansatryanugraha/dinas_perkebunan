<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        * { margin: 0; padding: 0; font-size: 100%; font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; line-height: 1.65; }
        img { max-width: 100%; margin: 0 auto; display: block; }
        body, #body-wrap { width: 100% !important; margin-left: -10px;  height: 100%; background: #f8f8f8; }
        a { color: #71bc37; text-decoration: none; }
        a:hover { text-decoration: underline; }
        #text-center { text-align: center; }
        #text-right { text-align: right; }
        #text-left { text-align: left; }
        #button { display: inline-block; color: white; background: #e31f52; border: solid #e31f52; border-width: 10px 20px 8px; font-weight: bold; border-radius: 4px; }
        #button:hover { text-decoration: none; }
        h1, h2, h3, h4, h5, h6 { margin-bottom: 20px; line-height: 1.25; }
        h1 { font-size: 32px; }
        h2 { font-size: 28px; }
        h3 { font-size: 24px; }
        h4 { font-size: 20px; }
        h5 { font-size: 16px; }
        p, ul, ol { font-size: 16px; font-family: "Bookman Old Style"; font-weight: normal; margin-bottom: 20px; }
        #container { display: block !important; clear: both !important; margin: 0 auto !important; max-width: 700px !important; }
        #container table { width: 100% !important; border-collapse: collapse;  }
        #container #masthead { padding: 20px 0; background: #13e503; color: white; }
        #container #masthead h1 { margin: 0 auto !important; max-width: 90%; text-transform: uppercase; }
        #container #masthead h3 { margin-top: 10px; max-width: 90%; text-transform: uppercase; }
        #container #content { background: white; padding: 30px 35px; }
        #container #content.footer { background: none; }
        #container #content.footer p { margin-bottom: 0; color: #888; text-align: center; font-size: 14px; }
        #container #content.footer a { color: #888; text-decoration: none; font-weight: bold; }
        #container #content.footer a:hover { text-decoration: underline; }
        td {
            padding: 10px 10px 10px 20px;
            margin-left: -100px;
            text-align: left;
            font-family: "Bookman Old Style";
        }
    </style>
</head>
<body>
    <?php
    $ResMutke = $brand->mutke;
    if ($ResMutke == 't') {
        $mutke = "Mutasi Keluar";
    } else {
        $mutke = "";
    }

    ?>

    <table id="body-wrap">
        <tr>
            <td id="container">
                <!-- Message start -->
                <table>
                    <tr>
                        <td align="center" colspan="3" id="masthead">
                            <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/logo.png" >
                        </td>
                    </tr>
                    <tr id="content" >
                        <td colspan="3"><center><p>Bersama ini diberitahukan dengan hormat bahwa ada pengajuan dengan data informasi sebagai berikut :</p></center>
            </td>
        </tr>
        <tr id="content">
            <td>Nama Pemohon
            </td><td width="10px">:</td> <td> <?php echo $brand->no_uji ?><?php echo $brand->nama_pemohon ?></td>
        </tr>
        <tr id="content">
            <td>Alamat Pemohon</td>
            <td width="10px">:</td> <td><?php echo $brand->alamat_pemohon ?>
            </td>
        </tr>
        <tr id="content">
            <td>Nama Usaha</td>
            <td width="10px">:</td> <td> <?php echo $brand->nama_usaha ?>
            </td>
        </tr>
        <tr id="content">
            <td>Alamat Usaha</td>
            <td width="10px">:</td> <td> <?php echo $brand->alamat_usaha ?>
            </td>
        </tr>
        <tr id="content">
            <td>No Telp</td>
            <td width="10px">:</td> <td> <?php echo $brand->no_telp ?>
            </td>
        </tr>
        <tr id="content">
            <td>Email</td>
            <td width="10px">:</td> <td> <?php echo $brand->email ?>
            </td>
        </tr>
        <tr id="content">
            <td>Jenis Usaha</td>
            <td width="10px">:</td> <td> <?php echo $brand->jenis_usaha ?>
            </td>
        </tr>
        <tr id="content">
            <td>Jenis Sertifikat</td>
            <td width="10px">:</td> <td> <?php echo $brand->jenis_sertifikat ?>
            </td>
        </tr>
        <tr id="content">
            <td>Jenis Komoditas</td>
            <td width="10px">:</td> <td> <?php echo $brand->jenis_komoditas ?>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" id="masthead">
        <center><h3>Dinas Perkebunan Provinsi Jawa Timur</h3></center>
    </td>
</tr>
</table>
</td>
</tr>
</table>
</body>