<?php

class M_service extends CI_Model
{

    public function login($data)
    {

        $this->db->select('*');
        $this->db->from('tbl_login');
        $this->db->where('email', $data['email']);
        $this->db->where('password', $data['password']);

        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function selekKatUsaha()
    {
        $this->db->from('tbl_usaha');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekKomoditas()
    {
        $sql = "SELECT DISTINCT nama FROM tbl_komoditi";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKodeIup($id)
    {
        $sql = "SELECT * FROM tbl_user WHERE id_user = '{$id}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKodeRegist($id)
    {
        $sql = "SELECT * FROM tbl_sertifikasi WHERE id_user = '{$id}' and status= 'Selesai Approve' ";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function getSertifikasi($id_user)
    {
        $sql = "SELECT * FROM tbl_sertifikasi WHERE id_user='$id_user' order by id_sertifikasi desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getHistory($id_user)
    {
        $sql = "SELECT * FROM tbl_history WHERE id_user='$id_user' order by id_history desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDataHasil($KodeRegist)
    {
        $sql = " SELECT * FROM tbl_sertifikasi WHERE kode='$KodeRegist' order by id_sertifikasi desc";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function kode()
    {
        $this->db->select('RIGHT(tbl_sertifikasi.kode,2) as kode', FALSE);
        $this->db->order_by('kode', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tbl_sertifikasi');  //cek dulu apakah ada sudah ada kode di tabel.    
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia    
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $tahun = date('Y');
        $bulan = date('m');
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodetampil = "SRF" . $tahun . $bulan . $batas;  //format kode
        return $kodetampil;
    }

    public function selek_empoye($nama)
    {
        $sql = "SELECT gambar FROM tbl_login WHERE nama='$nama'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function checkKode($data)
    {
        $this->db->select('kode_sertifikasi');
        $this->db->from('tbl_label');
        $this->db->where('kode_sertifikasi', $data['kode_sertifikasi']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkUsername($data)
    {
        $this->db->select('email');
        $this->db->from('tbl_user');
        $this->db->where('email', $data['email']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}

?>