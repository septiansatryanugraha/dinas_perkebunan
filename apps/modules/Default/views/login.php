<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Masuk | Dinas Perkebunan</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/tambahan/style-login.css">

        <link rel="icon" href="<?php echo base_url() ?>/assets/tambahan/gambar/logo-icon.png">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <?php echo $script_captcha; // javascript recaptcha ?>
        <style type="text/css">
            .login-page { background: url(<?php echo base_url(); ?>assets/tambahan/gambar/login-admin.jpg);background-position: center;background-repeat: no-repeat; background-size: cover; }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo" style="margin-top: -15px;">
                <img src="<?php echo base_url(); ?>assets/tambahan/gambar/logo.png"> 
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">
                    Login Admin
                </p>
                <form action="<?php echo base_url('Default/Auth/login'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Username" name="username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type='password' class="form-control" id="password" name="password" placeholder="password" class="span12">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <label>
                        <input type="checkbox" class="form-ok"> <font style='color: black; font-size: 13px;'> <b>Show password</b></font><br>
                    </label>
                    <?php echo $captcha ?>
                    <br>
                    <div class="row">
                        <div class="col-xs-offset-8 col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.login-box-body -->
            <?php
            echo show_err_msg($this->session->flashdata('error_msg'));

            ?>
        </div>
        <script>
            $(document).ready(function () {
                $('.form-ok').click(function () {
                    if ($(this).is(':checked')) {
                        $('#password').attr('type', 'text');
                    } else {
                        $('#password').attr('type', 'password');
                    }
                });
            });
        </script>
        <!-- /.login-box -->
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>