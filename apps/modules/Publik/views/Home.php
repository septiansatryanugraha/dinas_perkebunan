<style type="text/css">
    #mapid {
        height: 550px;
    }
    #btn_loading {
        display: none;
    }
</style>
<br><br><br>
<div class="container">
    <br><br><br>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-search" aria-hidden="true"></i>&nbsp; Pencarian</h3>
                </div>
                <div class="panel-body">
                    <form class="ui large form" id="slct_komoditi" method="POST">
                        <div>
                            <div class="form-group">
                                <label>Komoditas</label>
                                <select name="komoditas" id="komoditas" class="ui dropdown">
                                    <option value="">-- Jenis Komoditas --</option>
                                    <?php foreach ($komoditas as $data) { ?>
                                        <option value="<?php echo $data->jenis_komoditas; ?>">
                                            <?php echo $data->jenis_komoditas; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id='btn_loading'>
                                <button type='button' class='btn btn-success' disabled><i class='fa fa-spinner fa-spin'></i> &nbsp;Tunggu..</button> 
                            </div>
                            <div id="buka">
                                <button class="btn btn-success" type="submit" name="button"><i class="fa fa-search" aria-hidden="true"></i>&nbsp; Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; Peta Lokasi</h3>
                </div>
                <div class="panel-body">
                    <div id="mapid"></div><br>
                    <div class="col-md-12" style="background-color:beige; padding:7px;">
                        <div class="col-md-4">
                            <h5 style="text-align:center"><b>Jenis Komoditi: </b><span id="lg_komoditi">Semua </span></h5>
                        </div>
                        <div class="col-md-4">
                            <h5 style="text-align:center"><b>Total Pengajuan: </b><span class="count" id="total"></span> <b>Sertifikasi</b></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Prepare random data
    var mymap;
    var iconMap;
    var markerGroup;
    $(document).ready(function () {
        // iconMap=L.icon({
        // iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/03/Bigcity_red.png'
        // })
        mymap = L.map('mapid', {attributionControl: false}).setView([-6.988043, 112.9860763], 7);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            id: 'mapbox.streets'
        }).addTo(mymap);
        markerGroup = L.layerGroup().addTo(mymap);
        showMapJatim();

        $("#btn_loading").hide();

    });

    function showMapJatim() {
        mymap.setView([-6.988043, 112.9860763], 8);
        $.ajax({
            url: '<?php echo base_url('Publik/Web/showMapJatim'); ?>',
            type: "POST",
            // data: {komoditas: komoditas},
            success: function (res) {
                var data = JSON.parse(res);
                console.log(res);
                markerGroup.clearLayers();
                $.each(data, function (i, val) {
                    $("#total").text(val.total);
                    var marker = L.marker([val.lat, val.lng], {icon2: iconMap}).addTo(markerGroup);
                    marker.bindPopup("<div class='row'><div class='col-md-4'><img src='assets/tes.png'></div><div class='col-md-8'><b> Nama Pemohon : " + val.nama_pemohon + "<br> Jenis Komoditas : " + val.jenis_komoditas + "</b><br><b> Jenis Usaha : " + val.jenis_usaha + "<br> Nama Usaha : " + val.nama_usaha + "<br> No Telp : " + val.no_telp + "<br> Tanggal Sertifikasi : " + val.created_date + "<br> Keterangan : " + val.ket + "</b></div></div>")
                });
            }
        });
    }
    $('#slct_komoditi').submit(function (e) {
        var error = 0;
        var message = "";
        e.preventDefault();
        var data = $(this).serialize();

        if (error == 0) {
            var komoditas = $("#komoditas").val();
            var komoditas = komoditas.trim();
            if (komoditas.length == 0) {
                error++;
                message = "Silahkan pilih komoditas.";
            }
        }
        if (error == 0) {
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('search'); ?>',
                type: "POST",
                data: data,
                success: function (res) {
                    var data = JSON.parse(res);
                    console.log(res);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    markerGroup.clearLayers();
                    $.each(data, function (i, val) {
                        $("#total").text(val.total);
                        var marker = L.marker([val.lat, val.lng], {icon2: iconMap}).addTo(markerGroup);
                        marker.bindPopup("<b> Nama Pemohon : " + val.nama_pemohon + "<br> Jenis Komoditas : " + val.jenis_komoditas + "</b><br><b> Jenis Usaha : " + val.jenis_usaha + "<br> Nama Usaha : " + val.nama_usaha + "<br> No Telp : " + val.no_telp + "<br> Tanggal Sertifikasi : " + val.created_date + "<br> Keterangan : " + val.ket + "</b>")
                    });
                }
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>