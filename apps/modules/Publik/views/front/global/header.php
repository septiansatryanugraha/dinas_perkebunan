<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><?php echo $title . " - Dinas Perkebunan"; ?></title>
        <link rel="icon" href="<?php echo base_url() ?>/assets/publik/img/images/logo-icon.png">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/matrix-style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/apaja.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/dataTables.semanticui.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/semantic.css">
        <script src="<?php echo base_url(); ?>assets/code/highmaps.js"></script>
        <script src="<?php echo base_url(); ?>assets/code/modules/data.js"></script>
        <script src="<?php echo base_url(); ?>assets/code/modules/drilldown.js"></script>
        <script src="<?php echo base_url(); ?>assets/code/modules/exporting.js"></script>
        <script src="<?php echo base_url(); ?>assets/code/modules/offline-exporting.js"></script>
        <?php if (isset($style)) echo $style; ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">
        <script src="<?php echo base_url(); ?>assets/toastr/toastr.js"></script>
        <!-- LEAFLET MAPS -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
              integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
              crossorigin="" />
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
                integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
        crossorigin=""></script>
    </head>
    <body>