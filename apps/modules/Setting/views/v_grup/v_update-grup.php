<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="loading2"></div>
    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <input type="hidden" name="grup_id" value="<?php echo $datagrup->grup_id; ?>">
            <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
            <div class="row">
                <div class="col-md-9">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User Grup</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama Grup</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="nama grup" name="nama_grup" aria-describedby="sizing-addon2" value="<?php echo $datagrup->nama_grup; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="deskripsi singkat" name="deskripsi" aria-describedby="sizing-addon2" value="<?php echo $datagrup->deskripsi; ?>">
                            </div>
                        </div>
                    </div>	
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo base_url('user-grup'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);

        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-grup'); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-update").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('user-grup'); ?>'", 500);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    //klik loading ajax
    $(document).ready(function () {
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='http://localhost/custom-admin/assets/tambahan/gambar/loader-ok.gif' height='18'> ");
            $("#loading2").modal('show');
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $("#loading2").modal('hide');
                }
            });
            return true;
        });
    });
</script>	