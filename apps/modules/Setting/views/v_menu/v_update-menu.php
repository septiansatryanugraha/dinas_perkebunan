<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <input type="hidden" name="id_menu" value="<?php echo $dataMenu->id_menu; ?>">
            <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
            <div class="row">
                <div class="col-md-9">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Menu</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama Menu</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="nama warna" name="nama_menu" aria-describedby="sizing-addon2" value="<?php echo $dataMenu->nama_menu; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Icon</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="icon menu" name="icon" aria-describedby="sizing-addon2" value="<?php echo $dataMenu->icon; ?>">
                                <p style='color: red; font-size: 14px;'> *example fa fa-home</p>
                            </div>	  
                            <div class="col-md-1">
                                <a href="<?php echo base_url() ?>icon.html" target="_blank" class="btn default btn-primary "><i class='fa fa-edit'></i> Icon List </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Link Menu</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="link menu" name="link" aria-describedby="sizing-addon2" value="<?php echo $dataMenu->link; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">kode Menu</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="kode menu" name="kode_menu" aria-describedby="sizing-addon2" value="<?php echo $dataMenu->kode_menu; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Menu</label>
                            <div class="col-sm-5">
                                <select name="parent" class="form-control select2" aria-describedby="sizing-addon2">
                                    <option value="0">Menu Utama</option>
                                    <option disabled="disabled">---------------------- SUB MENU FROM ------------------</option>
                                    <?php foreach ($dataMenu2 as $data) { ?>
                                        <option value="<?php echo $data->id_menu; ?>"<?php echo ($data->id_menu == $dataMenu->parent) ? "selected='selected'" : ""; ?>>
                                            <?php echo $data->nama_menu; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Urutan</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="urutan" name="urutan" aria-describedby="sizing-addon2" value="<?php echo $dataMenu->urutan; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Menu Available</label>
                            <div class="col-xs-3">
                                <?php $menu = explode(",", $dataMenu->menu_file); ?>
                                <input type='checkbox' name='menu_file[]' value="view" <?php
                                foreach ($menu as $value) {
                                    echo ('view' == $value) ? 'checked' : '';
                                }

                                ?> /> &nbsp;View<br>
                                <input type='checkbox' name='menu_file[]' value="add" <?php
                                foreach ($menu as $value) {
                                    echo ('add' == $value) ? 'checked' : '';
                                }

                                ?> /> &nbsp;Add<br>
                                <input type='checkbox' name='menu_file[]' value="edit" <?php
                                foreach ($menu as $value) {
                                    echo ('edit' == $value) ? 'checked' : '';
                                }

                                ?> /> &nbsp;Edit<br>
                                <input type='checkbox' name='menu_file[]' value="del" <?php
                                foreach ($menu as $value) {
                                    echo ('del' == $value) ? 'checked' : '';
                                }

                                ?> /> &nbsp;Delete<br>										
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo base_url('menu'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.row -->
        </form>
    </div>
</section>
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);

        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-menu'); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-update").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('menu'); ?>'", 500);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    // untuk select2 original
    $(function () {
        $(".select2").select2({});
    });
</script>	