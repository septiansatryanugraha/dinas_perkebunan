<?php $this->load->view('_heading/_headerContent') ?>
<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <input type="hidden" name="id" value="<?php echo $dataUser->id; ?>">
            <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
            <div class="row">
                <div class="col-md-7">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama User</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="nama" name="nama" aria-describedby="sizing-addon2" value="<?php echo $dataUser->nama; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email User</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="email" name="email" aria-describedby="sizing-addon2" autocomplete="false" value="<?php echo $dataUser->email; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-3">
                                <select name="status" class="form-control select2"  aria-describedby="sizing-addon2">
                                    <?php foreach ($dataStatus as $status) { ?>
                                        <option value="<?php echo $status->id_status; ?>" <?php echo ($status->id_status == $dataUser->status) ? "selected='selected'" : ""; ?>>
                                            <?php echo $status->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="username" name="username" aria-describedby="sizing-addon2" autocomplete="false" value="<?php echo $dataUser->username; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" id="password-field" placeholder="password"  name="password" aria-describedby="sizing-addon2" autocomplete="false">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <p style='color: red; font-size: 14px;'><b> *isi password jika mau di update</b></p>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Grup</label>
                            <div class="col-sm-4">
                                <select name="grup_id" class="form-control select2" aria-describedby="sizing-addon2">
                                    <?php foreach ($datagrup as $data) { ?>
                                        <option value="<?php echo $data->grup_id; ?>" <?php echo ($data->grup_id == $dataUser->grup_id) ? "selected='selected'" : ""; ?>>
                                            <?php echo $data->nama_grup; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo base_url('user'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.row -->
        </form>
    </div>
</section>
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);

        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-user'); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-update").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('user'); ?>'", 500);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select2").select2({});
    });

    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>	