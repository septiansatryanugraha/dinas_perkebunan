<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_grup');
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "grup";
        $data['judul'] = "grup";
        $data['dataWarna'] = $this->M_grup->get_datatables();
        $this->loadkonten('v_grup/v_home', $data);
    }

    public function add()
    {
        $data['userdata'] = $this->userdata;

        $data['page'] = "grup";
        $data['judul'] = "grup";
        $data['deskripsi'] = "Manage Data Kategori Halaman";

        $this->loadkonten('v_grup/v_tambah-grup', $data);
    }

    public function prosesTambah()
    {
        $this->db->trans_begin();
        try {
            if (isset($_POST["nama_grup"]) && !empty($_POST["nama_grup"])) {
                $data = array(
                    'nama_grup' => $this->input->post('nama_grup'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'created_by' => $this->input->post('created_by'),
                    'last_created_date' => date('Y-m-d H:i:s')
                );
                $result = $this->M_grup->simpan_data($data);
                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        if ($id > 1) {
            $where = array('grup_id' => $id);

            $data['datagrup'] = $this->M_grup->select_by_id($id);
            $data['userdata'] = $this->userdata;

            $data['page'] = "grup";
            $data['judul'] = "grup";
            $data['deskripsi'] = "Manage Data Kategori";
            $this->loadkonten('v_grup/v_update-grup', $data);
        } else {
            echo "<script>alert('Super Administrator cannot changed.'); window.location = '" . base_url('user-grup') . "';</script>";
        }
    }

    public function prosesUpdate()
    {
        $this->db->trans_begin();
        try {
            if (isset($_POST["nama_grup"]) && !empty($_POST["nama_grup"])) {
                if ($this->input->post('grup_id') > 1) {
                    $data = array(
                        'nama_grup' => $this->input->post('nama_grup'),
                        'deskripsi' => $this->input->post('deskripsi'),
                        'last_update_by' => $this->input->post('last_update_by'),
                        'last_update_date' => date('Y-m-d H:i:s')
                    );
                    $result = $this->M_grup->update($data, array('grup_id' => $this->input->post('grup_id')));
                    if ($result > 0) {
                        $this->db->trans_commit();
                        $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                    } else {
                        $this->db->trans_rollback();
                        $out = array('status' => false, 'pesan' => "GAGAL");
                    }
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $id = $_POST['grup_id'];
        if ($id > 1) {
            $result = $this->M_grup->hapus($id);

            if ($result > 0) {
                $out['status'] = 'berhasil';
            } else {
                $out['status'] = 'gagal';
            }
        } else {
            $out['status'] = 'gagal';
        }
    }
}
