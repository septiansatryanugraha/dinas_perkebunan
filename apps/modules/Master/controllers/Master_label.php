<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_label extends AUTH_Controller
{
    const __tableName = 'tbl_label';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_pengajuan';
    const __folder = 'v_label/';
    const __kode_menu = 'kode-label';
    const __title = 'Master Label ';
    const __model = 'M_label';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $list = $this->M_label->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $status = '<small class="label pull-center bg-red">Pending</small">';
            if ($brand->status == 'Approve Pelabelan') {
                $status = '<small class="label pull-center bg-green">Approve Pelabelan</small">';
            } elseif ($brand->status == 'Belum Approve') {
                $status = '<small class="label pull-center bg-blue">Belum Approve</small">';
            } elseif ($brand->status == 'Revisi') {
                $status = '<small class="label pull-center bg-red">Revisi Pelabelan</small">';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_sertifikasi;
            $row[] = $brand->no_sertifikat;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->alamat_pemohon;
            $row[] = $brand->no_telp;
            $row[] = $brand->jenis_komoditas;
            $row[] = $status;
            //add html for action

            $buttonEdit = '';
            $buttonPrint = '';

            if ($brand->status == 'Pending' || $brand->status == 'Belum Approve' || $brand->status == 'Revisi') {
                $buttonEdit = anchor('edit-label/' . $brand->id_pengajuan, '<span tooltip="Edit Data"><span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
            } elseif ($brand->status == 'Approve Pelabelan') {
                $buttonPrint = "<a href='" . site_url('cetak-sertifikat/' . $brand->id_pengajuan) . "' target='__blank'><button class='btn btn-sm btn-primary'><span tooltip='Print Data'><i class='fa fa-print'></i></button></a>";
            }

            $row[] = $buttonEdit . ' ' . $buttonPrint;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);

            $data['brand'] = $this->M_label->selectById($id);
            $data['status'] = $this->M_label->selectStatus();

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $status_pengajuan = $this->input->post('status');

        $this->db->trans_begin();
        try {
            $data = array(
                'status' => $status_pengajuan,
                'updated_by' => $username,
                'updated_date' => $date,
            );
            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

            if ($status_pengajuan == 'Approve Pelabelan') {
                $keterangan = '<b>' . $username . '</b> Telah Menyatakan <b>' . $status_pengajuan . ' </b>kepada pengajuan user <b>' . $nama_pemohon . '</b> dan proses pelabelan segera di lakukan oleh dinas perkebunan provinsi jawa timur';
            } else {
                $keterangan = '<b>' . $username . '</b> Telah Menyatakan <b>' . $status_pengajuan . ' </b>kepada pengajuan user <b>' . $nama_pemohon . '</b> karena ada beberapa verifikasi data yang belum valid silahkan di cek di catatan pengajuan';
            }

            $data2 = array(
                'kode' => $this->input->post('kode'),
                'id_user' => $this->input->post('id_user'),
                'status' => $status_pengajuan,
                'keterangan' => $keterangan,
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => "GAGAL");
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post('id_pengajuan');
        $this->db->delete(self::__tableName, array('kode' => $token));
        $this->db->delete(self::__tableName2, array('kode' => $token));
        echo "{}";
    }
}
