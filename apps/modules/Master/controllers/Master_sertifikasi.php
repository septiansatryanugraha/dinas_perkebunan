<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_sertifikasi extends AUTH_Controller
{
    const __tableName = 'tbl_sertifikasi';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_sertifikasi';
    const __tableIdname = 'name';
    const __folder = 'v_sertifikasi/';
    const __kode_menu = 'kode_sertifikasi';
    const __title = 'Master Sertifikat ';
    const __model = 'M_sertifikasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_sertifikasi->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $idUser = $brand->id_user;
            $dataUser = $this->M_sertifikasi->select_user($idUser);
            $namaUser = $dataUser->nama_lengkap;

            // $Kode_ser =   $brand->kode_rekomendasi;
            // $dataKode = $this->M_sertifikasi->select_kode($Kode_ser);
            // $Kode = $dataKode->kode_rekomendasi;

            $status = '<small class="label pull-center bg-blue">Belum Approve</small">';
            if ($brand->status == 'Survey') {
                $status = '<small class="label pull-center bg-green">Survey</small">';
            } elseif ($brand->status == 'Selesai Survey') {
                $status = '<small class="label pull-center bg-red">Selesai Survey</small">';
            } elseif ($brand->status == 'Revisi') {
                $status = '<small class="label pull-center bg-red">Revisi User</small">';
            } elseif ($brand->status == 'Selesai Approve') {
                $status = '<small class="label pull-center bg-red">Approve Sertifikat</small">';
            }

            if ($brand->tanggal_survey == NULL) {
                $tanggal_survey = 'Tanggal Belum Dijadwalkan';
            } else {
                $tanggal_survey = date('d-m-Y', strtotime($brand->tanggal_survey));
            }

            if ($brand->qrcode == '') {
                $Qrcode = '<img src="' . base_url() . '/assets/tambahan/gambar/tidak-ada.png" width="120" /></center>';
            } else {

                $Qrcode = '<span tooltip="Klik Qrcode"><a href="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=' . $brand->qrcode . '"" target="blank"><img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=' . $brand->qrcode . '" /></a></span>';
            }


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $Qrcode;
            $row[] = $brand->kode;
            $row[] = $namaUser;
            $row[] = $brand->nama_usaha;
            $row[] = $brand->kode_iup;
            $row[] = $tanggal_survey;
            $row[] = $status;
            //add html for action

            $buttonEdit = '';
            $buttonQr = '';
            if ($brand->status == 'Selesai Approve') {
                
            } else {
                if ($accessEdit->menuview > 0) {
                    $buttonEdit = anchor('edit-sertifikasi/' . $brand->id_sertifikasi, '<span tooltip="Edit Data"><span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
                }
            }

            if ($brand->status == 'Selesai Survey') {
                if ($accessEdit->menuview > 0) {
                    $buttonQr = anchor('edit-qr/' . $brand->id_sertifikasi, '<span tooltip="Edit No Sertifikat "><span class="fa fa-qrcode"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
                }
            } elseif ($brand->status == 'Selesai Approve') {
                
            }


            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<small class="label pull-center bg-green">No Action</small">';
                // $buttonDel = '<button class="btn btn-sm btn-danger hapus-sts" data-id=' . "'" . $brand->kode . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . ' ' . $buttonQr . ' ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {
            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_sertifikasi->selectById($id);
            $product = $this->M_sertifikasi->selectById($id);
            $tukar = $product->kode;
            $data['loop'] = $this->M_sertifikasi->selekTes($tukar);
            $data['jenis_usaha'] = $this->M_sertifikasi->selekKategori();
            $data['jenis_komoditas'] = $this->M_sertifikasi->selekKomoditas();
            $data['jenis_sertifikat'] = $this->M_sertifikasi->selekSertifikat();
            $data['status'] = $this->M_sertifikasi->selectStatus();


            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $this->db->trans_begin();
        try {
            $new_name = time() . $_FILES["userfiles"]['name'];
            $nmfile = "file_sts_" . $new_name;
            $config['upload_path'] = "./upload/sts/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '2048'; //maksimum besar file 5M
            $config['file_name'] = $nmfile;
            // $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            $status_pengajuan = $this->input->post('status');
            $nama_pemohon = $this->input->post('nama_pemohon');
            $tanggal_survey = date('d-m-Y', strtotime($this->input->post('tanggal_survey')));

            if ($status_pengajuan == 'Belum Approve') {

                $data = array(
                    'status' => $this->input->post('status'),
                    'catatan' => $this->input->post('catatan'),
                    'jenis_sertifikat' => $this->input->post('jenis_sertifikat'),
                    'updated_by' => $username,
                    'updated_date' => $date,
                    'tanggal' => $date2,
                );

                $data2 = array(
                    'kode' => $this->input->post('kode'),
                    'kode_iup' => $this->input->post('kode_iup'),
                    'id_user' => $this->input->post('id_user'),
                    'status' => $this->input->post('status'),
                    'keterangan' => '<b>' . $username . '</b> Telah Menyatakan <b>' . $status_pengajuan . ' </b>kepada pengajuan user <b>' . $nama_pemohon . '</b> karena ada beberapa berkas atau file dokumen yang belum lengkap, harap di baca di catatan pengajuan ',
                    'created_by' => 'System',
                    'created_date' => $date,
                    'tanggal' => $date2,
                );

                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
                $result = $this->db->insert(self::__tableName2, $data2);

                if ($this->db->trans_status() === FALSE) {
                    $out['status'] = 'gagal';
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out['status'] = 'berhasil';
                } else {
                    $this->db->trans_rollback();
                    $out['status'] = 'gagal';
                }
            } else {
                if ($this->upload->do_upload("file_sts")) {
                    $data_file = $this->upload->data();
                    $path['link'] = "upload/sts/";

                    $data = array(
                        'status' => $this->input->post('status'),
                        'jenis_sertifikat' => $this->input->post('jenis_sertifikat'),
                        'file_sts' => $path['link'] . '' . $data_file['file_name'],
                        'nama_sts' => $data_file['file_name'],
                        'updated_by' => $username,
                        'updated_date' => $date,
                        'tanggal' => $date2,
                    );

                    $data2 = array(
                        'kode' => $this->input->post('kode'),
                        'kode_iup' => $this->input->post('kode_iup'),
                        'id_user' => $this->input->post('id_user'),
                        'status' => $this->input->post('status'),
                        'keterangan' => '<b>' . $username . '</b> Telah Menyatakan <b>' . $status_pengajuan . ' </b>kepada usaha yang dimiliki user <b>' . $nama_pemohon . '</b> dan wajib melakukan pembayaran sejumlah Nominal yang tertera di surat tagihan retribusi dan melakukan upload bukti Transfer di Sistem PRIMA PSBP ',
                        'created_by' => 'System',
                        'created_date' => $date,
                        'tanggal' => $date2,
                    );

                    $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
                    $result = $this->db->insert(self::__tableName2, $data2);

                    if ($this->db->trans_status() === FALSE) {
                        $out = array('status' => false, 'pesan' => "GAGAL");
                    }

                    if ($result > 0) {
                        $this->db->trans_commit();
                        $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                    } else {
                        $this->db->trans_rollback();
                        $out = array('status' => false, 'pesan' => "GAGAL");
                    }
                } else {
                    $data = array(
                        'status' => $this->input->post('status'),
                        'jenis_sertifikat' => $this->input->post('jenis_sertifikat'),
                        'tanggal_survey' => date('Y-m-d', strtotime($this->input->post('tanggal_survey'))),
                        'updated_by' => $username,
                        'updated_date' => $date,
                        'tanggal' => $date2,
                    );

                    $data2 = array(
                        'kode' => $this->input->post('kode'),
                        'kode_iup' => $this->input->post('kode_iup'),
                        'id_user' => $this->input->post('id_user'),
                        'status' => $this->input->post('status'),
                        'keterangan' => '<b>' . $username . '</b> Sedang menjadwalkan proses <b>' . $status_pengajuan . ' </b>kepada user <b>' . $nama_pemohon . '</b> pada tanggal <b>' . $tanggal_survey . '</b>',
                        'created_by' => 'System',
                        'created_date' => $date,
                        'tanggal' => $date2,
                    );

                    $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
                    $result = $this->db->insert(self::__tableName2, $data2);

                    if ($this->db->trans_status() === FALSE) {
                        $out = array('status' => false, 'pesan' => "GAGAL");
                    }

                    if ($result > 0) {
                        $this->db->trans_commit();
                        $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                    } else {
                        $this->db->trans_rollback();
                        $out = array('status' => false, 'pesan' => "GAGAL");
                    }
                }
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function Editqr($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {
            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_sertifikasi->selectById($id);
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update_qr', $data);
        }
    }

    public function prosesUpdateqr()
    {
        $where = trim($this->input->post(self::__tableId));

        $KodePengajuan = $this->input->post('kode');

        $this->db->trans_begin();
        try {
            $qrcode = 'http://' . $_SERVER['SERVER_NAME'] . '/dinas_perkebunan/qrcode-results/' . $KodePengajuan;

            $data = array(
                'qrcode' => $qrcode,
                'no_sertifikat' => $this->input->post('no_sertifikat'),
            );
            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post('kode');
        $this->db->delete(self::__tableName, array('kode' => $token));
        $this->db->delete(self::__tableName2, array('kode' => $token));
        echo "{}";
    }
}
