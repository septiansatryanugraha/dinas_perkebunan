<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_sts extends AUTH_Controller
{
    const __tableName = 'tbl_sertifikasi';
    const __tableName2 = 'tbl_history';
    const __tableName3 = 'tbl_koordinat';
    const __tableId = 'id_sertifikasi';
    const __tableIdname = 'name';
    const __folder = 'v_sts/';
    const __kode_menu = 'kode-sts';
    const __title = 'Master Retribusi ';
    const __model = 'M_sertifikasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_sertifikasi->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $idUser = $brand->id_user;
            $dataUser = $this->M_sertifikasi->select_user($idUser);
            $namaUser = $dataUser->nama_lengkap;

            // $Kode_ser =   $brand->kode_rekomendasi;
            // $dataKode = $this->M_sertifikasi->select_kode($Kode_ser);
            // $Kode = $dataKode->kode_rekomendasi;

            $status = '<small class="label pull-center bg-blue">Belum Approve</small">';
            if ($brand->status == 'Survey') {
                $status = '<small class="label pull-center bg-green">Survey</small">';
            } elseif ($brand->status == 'Selesai Survey') {
                $status = '<small class="label pull-center bg-red">Selesai Survey</small">';
            } elseif ($brand->status == 'Selesai Approve') {
                $status = '<small class="label pull-center bg-red">Approve Sertifikat</small">';
            }

            if ($brand->tanggal_survey == NULL) {
                $tanggal_survey = 'Tanggal Belum Dijadwalkan';
            } else {
                $tanggal_survey = date('d-m-Y', strtotime($brand->tanggal_survey));
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->nama_usaha;
            $row[] = $brand->kode_iup;
            $row[] = $tanggal_survey;
            $row[] = $status;
            //add html for action

            $buttonEdit = '';
            if ($brand->status == 'Selesai Approve') {
                
            } else {


                if ($accessEdit->menuview > 0) {
                    $buttonEdit = anchor('edit-sts/' . $brand->id_sertifikasi, '<span tooltip="Edit Data"><span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
                }
            }



            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<small class="label pull-center bg-green">No Action</small">';
                // $buttonDel = '<button class="btn btn-sm btn-danger hapus-sts" data-id=' . "'" . $brand->kode . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);

            $data['brand'] = $this->M_sertifikasi->selectById($id);
            $data['jenis_usaha'] = $this->M_sertifikasi->selekKategori();
            $data['jenis_komoditas'] = $this->M_sertifikasi->selekKomoditas();
            $data['status'] = $this->M_sertifikasi->selectStatus2();

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $where2 = trim($this->input->post('kode'));
        $status_pengajuan = $this->input->post('status');
        $nama_pemohon = $this->input->post('nama_pemohon');

        $this->db->trans_begin();
        try {
            $data = array(
                'status' => $this->input->post('status'),
                'updated_by' => $username,
                'updated_date' => $date,
                'tanggal' => $date2,
            );

            $data2 = array(
                'kode' => $this->input->post('kode'),
                'id_user' => $this->input->post('id_user'),
                'status' => $this->input->post('status'),
                'keterangan' => '<b>' . $username . '</b> Telah Menyatakan <b>' . $status_pengajuan . ' </b>serta Pengiriman Dokumen ( kurang lebih ) 1 minggu setelah proses Approve ',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $data3 = array(
                'created_date' => $date,
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);
            $result = $this->db->insert(self::__tableName3, $data3, array('kode_sertifikasi' => $where2));

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => "GAGAL");
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post('kode');
        $this->db->delete(self::__tableName, array('kode' => $token));
        $this->db->delete(self::__tableName2, array('kode' => $token));
        echo "{}";
    }
}
