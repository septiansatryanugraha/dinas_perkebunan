<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_opd extends AUTH_Controller
{
    const __tableName = 'tbl_user';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_user';
    const __tableIdname = 'name';
    const __folder = 'v_opd/';
    const __kode_menu = 'master-opd';
    const __title = 'Master User ';
    const __model = 'Mdl_opd';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->Mdl_opd->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $status = '<small class="label pull-center bg-yellow">Non aktif</small>';
            if ($brand->status == 'Aktif') {
                $status = '<small class="label pull-center bg-green">Aktif</small>';
            }

            $statusBerkas = '<small class="label pull-center bg-red">Belum Approve</small>';
            if ($brand->status_berkas == 'Approve') {
                $statusBerkas = '<small class="label pull-center bg-green">Approve</small>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->email;
            $row[] = $brand->nama_lengkap;
            $row[] = $brand->alamat;
            $row[] = '<b>Status user : </b> ' . $status . '<br> <b>Status Berkas : </b>' . $statusBerkas . '';
            //add html for action
            $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-master-opd/' . $brand->id_user, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-master-opd" data-id=' . "'" . $brand->nama . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {
            $where = array(self::__tableId => $id);
            $data['brand'] = $this->Mdl_opd->selectById($id);
            $data['datastatus'] = $this->Mdl_opd->selek_status();
            $data['statusBerkas'] = $this->Mdl_opd->statusBerkas();
            $path = $this->Mdl_opd->selectById($id);
            $path->folder;
            $res = $path->folder;

            $directory = 'upload/berkas_kelengkapan/' . $res;
            $data["resFold"] = glob($directory . "/*");

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function prosesUpdate()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $where2 = trim($this->input->post('nama'));

        $statusBerkas = $this->input->post('status_berkas');
        $nama_pemohon = $this->input->post('nama_lengkap');

        $this->db->trans_begin();
        try {
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'alamat' => $this->input->post('alamat'),
                'nama' => $this->input->post('nama'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'status' => $this->input->post('status'),
                'status_berkas' => $statusBerkas,
                // 'email'			=> $this->input->post('_email'),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'telp' => $this->input->post('telp'),
                'password' => base64_encode($this->input->post("password")),
                'ip_address' => $this->input->ip_address(),
                'user_agent' => $this->input->user_agent(),
                'created_by' => $username,
                'updated_date' => $date
            );
            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

            if ($statusBerkas == 'Approve') {
                $keterangan = '<b>' . $username . '</b> Telah Menyatakan <b>' . $statusBerkas . ' </b>Berkas kepada user <b>' . $nama_pemohon . '</b> silahkan mulai melakukan pengajuan.';
            } else {
                $keterangan = '<b>' . $username . '</b> Telah Menyatakan <b>' . $statusBerkas . ' </b>Berkas kepada user <b>' . $nama_pemohon . '</b> karena ada berapa berkas yang belum valid silahkan dicek di catatan profil.';
            }

            $data2 = array(
                'id_user' => $this->input->post('id_user'),
                'status' => $statusBerkas,
                'keterangan' => $keterangan,
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post('nama');
        $this->db->delete(self::__tableName, array('nama' => $token));
        $this->db->delete(self::__tableName2, array('nama' => $token));
        echo "{}";
    }
}
