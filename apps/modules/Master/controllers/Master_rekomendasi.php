<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_rekomendasi extends AUTH_Controller
{
    const __tableName = 'tbl_form_rekomendasi';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_rekomendasi';
    const __tableIdname = 'name';
    const __folder = 'v_rekomendasi/';
    const __kode_menu = 'master-rekomendasi';
    const __kode_menu2 = 'histori-dokumen';
    const __title = 'Master Rekomendasi ';
    const __title2 = 'Histori Pengajuan  ';
    const __model = 'M_rekomendasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function history_dokumen()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title2;
        $data['judul'] = self::__title2;

        $this->loadkonten('' . self::__folder . 'history', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_rekomendasi->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $idUser = $brand->id_user;
            $dataUser = $this->M_rekomendasi->select_user($idUser);
            $namaUser = $dataUser->nama_lengkap;

            $status = '<small class="label pull-center bg-blue">Belum Direkomendasi</small>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<small class="label pull-center bg-green">Telah Direkomendasi</small>';
            }

            if ($brand->kode_rekomendasi == NULL) {
                $kode_rekomendasi = '<small class="label pull-center bg-blue">Belum Mendapatkan Kode Rekomendasi</small>';
            } else {
                $kode_rekomendasi = $brand->kode_rekomendasi;
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kode_rekomendasi;
            $row[] = $namaUser;
            $row[] = $brand->nama_usaha;
            $row[] = $brand->email;
            $row[] = $brand->no_telp;
            $row[] = $status;
            //add html for action

            $buttonEdit = '';
            if ($brand->status == 'Rekomendasi') {
                
            } else {
                $buttonEdit = '';
                if ($accessEdit->menuview > 0) {
                    $buttonEdit = anchor('edit-rekomendasi/' . $brand->id_rekomendasi, ' <span tooltip="Edit Data"><span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
                }
            }

            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<small class="label pull-center bg-green">No Action</small">';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function tes()
    {
        $res = '1574389162';
        $directory = 'upload/berkas_kelengkapan/' . $res;
        foreach (glob($directory . "/*") as $file) {
            $kalimat = $file;
            $string = substr($kalimat, 37);
            echo "$string";
            echo"<pre>";
        }
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $where = array(self::__tableId => $id);

            $data['brand'] = $this->M_rekomendasi->selectById($id);
            $data['jenis_usaha'] = $this->M_rekomendasi->selekKategori();
            $data['status'] = $this->M_rekomendasi->selectStatus();

            $path = $this->M_rekomendasi->selectById($id);
            $path->folder;
            $res = $path->folder;

            $directory = 'upload/berkas_kelengkapan/' . $res;
            $data["images"] = glob($directory . "/*");

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function prosesUpdate()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $this->db->trans_begin();
        try {
            $new_name = time() . $_FILES["userfiles"]['name'];
            $nmfile = "file_rekom_" . $new_name;
            $config['upload_path'] = "./upload/berkas_kelengkapan/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '2048'; //maksimum besar file 5M
            $config['file_name'] = $nmfile;
            // $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);
            $this->db->trans_begin();

            $status_pengajuan = $this->input->post('status');
            $kode_rekomendasi = $this->input->post('kode_rekomendasi');
            $kode_iup = $this->input->post('kode_iup');

            $data = array(
                'kode_rekomendasi' => $this->input->post('kode_rekomendasi'),
                'status' => $this->input->post('status'),
                'updated_by' => $username,
                'updated_date' => $date,
                'tanggal' => $date2,
            );

            if ($this->upload->do_upload("surat_rekom")) {
                $data_file = $this->upload->data();
                $path['link'] = "upload/berkas_kelengkapan/";

                $data = array_merge($data, array(
                    'nama_file_rekom' => $data_file['file_name'],
                    'surat_rekom' => $path['link'] . '' . $data_file['file_name'],
                ));
                $keterangan = '<b>' . $username . '</b> Sedang melakukan memberikan Kode Rekomendasi <b>' . $kode_rekomendasi . '</b>';
            } else {
                $data = array_merge($data, array(
                    'kode_iup' => $this->input->post('kode_iup'),
                ));
                $keterangan = '<b>' . $username . '</b> Sedang melakukan <b>' . $status_pengajuan . ' </b>terhadap proses pengajuan rekomendasi dengan memberikan Kode IUP <b>' . $kode_iup . '</b>';
            }

            $data2 = array(
                'kode' => $this->input->post('kode'),
                'kode_rekomendasi' => $this->input->post('kode_rekomendasi'),
                'id_user' => $this->input->post('id_user'),
                'status' => $this->input->post('status'),
                'keterangan' => $keterangan,
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );
            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => "GAGAL");
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post('kode');

        $files = $this->db->get_where(self::__tableName, array('kode' => $token));

        $path = $this->M_rekomendasi->selectById2($token);
        $path->folder;
        $res = $path->folder;

        $path = 'upload/berkas_kelengkapan/' . $res;
        delete_files($path, true, false, 1);

        if ($files->num_rows() > 0) {
            $hasil = $files->row();
            $judul = $hasil->surat_rekomendasi;
            //hapus unlink file
            if (file_exists($file = $judul)) {
                unlink($file);
            }
        }
        if ($files->num_rows() > 0) {
            $hasil = $files->row();
            $judul = $hasil->surat_kerjasama;
            //hapus unlink file
            if (file_exists($file = $judul)) {
                unlink($file);
            }
        }
        if ($files->num_rows() > 0) {
            $hasil = $files->row();
            $judul = $hasil->surat_patuh;
            //hapus unlink file
            if (file_exists($file = $judul)) {
                unlink($file);
            }
        }
        if ($files->num_rows() > 0) {
            $hasil = $files->row();
            $judul = $hasil->surat_rekom;
            //hapus unlink file
            if (file_exists($file = $judul)) {
                unlink($file);
            }
        }

        $this->db->delete(self::__tableName, array('kode' => $token));
        $this->db->delete(self::__tableName2, array('kode' => $token));
        echo "{}";
    }
}
