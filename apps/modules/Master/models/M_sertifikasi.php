<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sertifikasi extends CI_Model
{
    const __tableName = '  tbl_sertifikasi';
    const __tableId = 'id_sertifikasi';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
    const __tableName3 = 'tbl_history_dokumen';
    const __tableId3 = 'id_history';
    const __tableName4 = 'tbl_form_rekomendasi';
    const __tableId4 = 'id_rekomendasi';
    const __tableName5 = 'tbl_koordinat';
    const __tableId5 = 'kode_sertifikasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE 1=1";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
        }

        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_sertifikasi DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selekKategori()
    {
        $this->db->from('tbl_usaha');
        $data = $this->db->get();

        return $data->result();
    }

    public function selekKomoditas()
    {
        $sql = "SELECT DISTINCT nama FROM tbl_komoditi";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selekSertifikat()
    {
        $sql = "SELECT DISTINCT nama FROM tbl_sertifikat";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selekTes($kode_sertifikasi)
    {
        $this->db->from('tbl_koordinat');
        $this->db->where('kode_sertifikasi', $kode_sertifikasi);
        $data = $this->db->get();

        return $data->result();
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Survey','Selesai Survey','Belum Approve')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectStatus2()
    {
        $sql = " select * from status_grup WHERE nama in ('Selesai Approve')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select_user($id)
    {
        $sql = "SELECT * FROM " . self::__tableName2 . " WHERE " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select_kode($id)
    {
        $sql = "SELECT * FROM " . self::__tableName4 . " WHERE " . self::__tableId4 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";
        $this->db->query($sql);

        return $this->db->affected_rows();
    }
}
