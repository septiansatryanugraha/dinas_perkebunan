<?php $this->load->view('_heading/_headerContent') ?>
<style>
    /* The container */
    .container2 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    /* Hide the browser's default checkbox */
    .container2 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }
    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        border-radius: 3px;
        background-color: #d4d2d2;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }
    /* When the checkbox is checked, add a blue background */
    .container2 input:checked ~ .checkmark {
        background-color: #2196F3;
    }
    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }
    /* Show the checkmark when checked */
    .container2 input:checked ~ .checkmark:after {
        display: block;
    }
    /* Style the checkmark/indicator */
    .container2 .checkmark:after {
        left: 7px;
        top: 4px;
        width: 5px;
        height: 11px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<section class="content">
    <div class="loading2"></div>
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $brand->id_user; ?>">
                        <input type="hidden" class="form-control" name="id_rekomendasi" value="<?php echo $brand->id_rekomendasi; ?>">
                        <input type="hidden" class="form-control" name="kode" value="<?php echo $brand->kode; ?>">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Rekomendasi</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Rekomendasi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="kode_rekomendasi" placeholder="Kode Rekomendasi" id="kode_rekomendasi" value="<?php echo $brand->kode_rekomendasi; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Pemohon</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="nama_pemohon" placeholder="Nama Pemohon" id="nama_pemohon" value="<?php echo $brand->nama_pemohon; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat Pemohon</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="alamat_pemohon" placeholder="Alamat Pemohon" id="alamat_pemohon" value="<?php echo $brand->alamat_pemohon; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Usaha</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" id="nama_usaha" value="<?php echo $brand->nama_usaha; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat Usaha</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="alamat_usaha" placeholder="Alamat Usaha" id="alamat_usaha" value="<?php echo $brand->alamat_usaha; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Telp</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="no_telp" placeholder="No Telp" id="no_telp" value="<?php echo $brand->no_telp; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="email" placeholder="Email" id="email" value="<?php echo $brand->email; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Usaha</label>
                                <div class="col-sm-7">
                                    <select name="parent" class="form-control selek-usaha" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_usaha as $data) { ?>
                                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $brand->jenis_usaha) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php if ($brand->surat_iup != NULL) { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Surat IUP</label>
                                    <div class="col-sm-7">
                                        <a href="<?php echo base_url() . $brand->surat_iup; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_iup; ?></font></b></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Kode IUP</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="kode_iup" placeholder="Kode IUP" id="kode_iup" value="<?php echo $brand->kode_iup; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($brand->tipe == "pengajuan permohonan rekomendasi") { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Surat Rekomendasi</label>
                                    <div class="col-sm-7">
                                        <a href="<?php echo base_url() . $brand->surat_rekomendasi; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_rekomendasi; ?></font></b></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Surat Kerjasama</label>
                                    <div class="col-sm-7">
                                        <a href="<?php echo base_url() . $brand->surat_kerjasama; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_kerjasama; ?></font></b></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Surat Patuh</label>
                                    <div class="col-sm-7">
                                        <a href="<?php echo base_url() . $brand->surat_patuh; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_patuh; ?></font></b></a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Surat Rekomendasi2</label>
                                    <div class="col-sm-7">
                                        <a href="<?php echo base_url() . $brand->surat_rekom; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-archive-o" aria-hidden="true"></i> <?php echo $brand->nama_file_rekom; ?></font></b></a>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-3">
                                    <select name="status" class="form-control selek-status" id="status_dokumen">
                                        <?php foreach ($status as $data) { ?>
                                            <option></option>
                                            <option value="<?php echo $data->nama ?>"<?php echo ($data->nama == $brand->status) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="menu-show">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">File Rekomendasi</label>
                                    <div class="col-sm-5">
                                        <input type="file"  name="surat_rekom" id="surat_rekom"  onchange="return valid_surat4()"/>
                                        <?php if ($brand->surat_rekom != NULL) { ?>
                                            <a href="<?php echo base_url() . $brand->surat_rekom; ?>"><b><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo $brand->nama_file_rekom; ?></font></b></a>
                                        <?php } ?>
                                        <p style='color: red; font-size: 14px;'> * maksimal size 1 MB, file berformat pdf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            <a class="klik ajaxify" href="<?php echo site_url('master-rekomendasi'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php if ($brand->folder != NULL) { ?>
            <div class="col-md-4">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">File Berkas Pengajuan</h3>
                    </div>
                    <div class="box-body box-profile">
                        <form method="post" action="<?php echo base_url(); ?>Master/Master_rekomendasi/download" enctype="multipart/form-data">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th width="20px"><label class="container2"><input type="checkbox" id="check-all"><span class="checkmark"></span></label></th>
                                    <th>File</th>
                                </tr>
                                <?php
                                foreach ($images as $image) {
                                    $kalimat = $image;
                                    $string = substr($kalimat, 37);
                                    echo '
    <tr>
    <td><label class="container2"><input type="checkbox" name="images[]" class="check-item" value="' . $kalimat . '"><span class="checkmark"></span></label></th></td>
    <td><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;' . $string . '</font></td>
    </tr>';
                                }

                                ?>
                                <br>
                            </table>
                            <br/>
                            <div class="box-footer">
                                <div class="pencet">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var kode_rekomendasi = $("#kode_rekomendasi").val();
            var kode_rekomendasi = kode_rekomendasi.trim();
            if (kode_rekomendasi.length == 0) {
                error++;
                message = "Kode Rekomendasi wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('update-rekomendasi'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-update").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?= base_url('master-rekomendasi'); ?>'", 500);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-usaha").select2({
            placeholder: " -- Pilih usaha -- "
        });
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $('.menu-show').hide();
        $('#status_dokumen').change(function () {
            if ($('#status_dokumen').val() == 'Approve') {
                $('.menu-show').fadeIn("slow");
            } else {
                $('.menu-show').fadeOut("slow");
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('.pencet').hide();
        $("#check-all").click(function () {
            if ($(this).is(":checked")) {
                $(".check-item").prop("checked", true);
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
                $(".check-item").prop("checked", false);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('.pencet').hide();
        $(".check-item").click(function () {
            if ($(this).is(":checked")) {
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
            }
        });
    });
</script>