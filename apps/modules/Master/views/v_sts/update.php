<?php
$this->load->view('_heading/_headerContent');

$Kode_ser = $brand->kode_rekomendasi;
$dataKode = $this->M_sertifikasi->select_kode($Kode_ser);
$Kode = $dataKode->kode_rekomendasi;

?>
<style type="text/css">
    .bkti-tf {
        margin-left: 20px;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $brand->id_user; ?>">
                        <input type="hidden" name="kode" value="<?php echo $brand->kode; ?>">
                        <input type="hidden" name="nama_pemohon" value="<?php echo $brand->nama_pemohon; ?>">
                        <input type="hidden" class="form-control" name="id_sertifikasi" value="<?php echo $brand->id_sertifikasi; ?>">
                        <input type="hidden" name="kode" value="<?php echo $brand->kode; ?>">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Pemohon</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_pemohon" placeholder="Nama Pemohon" id="nama_pemohon" value="<?php echo $brand->nama_pemohon; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Alamat Pemohon</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_pemohon" placeholder="Alamat Pemohon" id="alamat_pemohon" value="<?php echo $brand->alamat_pemohon; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Usaha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" id="nama_usaha" value="<?php echo $brand->nama_usaha; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Alamat Usaha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_usaha" placeholder="Alamat Usaha" id="alamat_usaha" value="<?php echo $brand->alamat_usaha; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">No Telp</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_telp" placeholder="No Telp" id="no_telp" value="<?php echo $brand->no_telp; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" placeholder="Email" id="email" value="<?php echo $brand->email; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jenis Usaha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" placeholder="Email" id="email" value="<?php echo $brand->jenis_usaha; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jenis Sertifikat</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="jenis_sertifikat" placeholder="Jenis Sertifikat" id="jenis_sertifikat" value="<?php echo $brand->jenis_sertifikat; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jenis Komoditas</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="jenis_komoditas" placeholder="Jenis Komoditas" id="jenis_sertifikat" value="<?php echo $brand->jenis_komoditas; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-3">
                                    <select name="status" class="form-control selek-status" id="status">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?php echo $data->nama ?>"<?php echo ($data->nama == $brand->status) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="inputFoto" class="col-sm-2 control-label">Bukti Transfer</label>
                                <?php if ($brand->bukti_pembayaran == NULL) { ?>
                                    <div class="bkti-tf">
                                        <a href="<?php echo base_url(); ?>/assets/tambahan/gambar/tidak-ada.png" target="_blank"><img class="img-thumbnail" src="<?php echo base_url(); ?>/assets/tambahan/gambar/tidak-ada.png" width="200px" /></a>
                                    </div>
                                <?php } else { ?>
                                    <a href="<?php echo base_url(); ?>/<?php echo $brand->bukti_pembayaran; ?>" target="_blank"><img class="img-thumbnail" src='../<?php echo $brand->bukti_pembayaran; ?>' width="150px"></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            <a class="klik ajaxify" href="<?php echo base_url('master-sts'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);

        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-sts'); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-update").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('master-sts'); ?>'", 500);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".selek-usaha").select2({
            placeholder: " -- Pilih usaha -- "
        });
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
</script>