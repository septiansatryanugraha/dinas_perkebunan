<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

</style>

<?php
$password = $brand->password;
$dec_pass = base64_decode($password);

?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="row">
        <div class="col-md-7">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">View Data User</h3>
                </div>
                <form class="form-horizontal" id="form-update" method="POST">
                    <input type="hidden"  name="id_user" value="<?php echo $brand->id_user; ?>">
                    <input type="hidden"  name="nama" value="<?php echo $brand->nama; ?>">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap / Nama User OPD" id="nama_lengkap" value="<?php echo $brand->nama_lengkap; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-7">
                                <textarea class="form-control" name="alamat"><?php echo $brand->alamat; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama Usaha</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" id="nama_usaha" value="<?php echo $brand->nama_usaha; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Alamat Usaha</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="alamat_usaha" placeholder="Nama Usaha" id="alamat_usaha" value="<?php echo $brand->alamat_usaha; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Telp</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="telp" placeholder="Telp" id="telp" value="<?php echo $brand->telp; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-3">
                                <select name="status" class="form-control selek-status" aria-describedby="sizing-addon2">
                                    <?php foreach ($datastatus as $status) { ?>
                                        <option value="<?php echo $status->nama; ?>" <?php echo ($status->nama == $brand->status) ? "selected='selected'" : ""; ?>>
                                            <?php echo $status->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status Berkas</label>
                            <div class="col-sm-4">
                                <select name="status_berkas" id="status" class="form-control selek-status" aria-describedby="sizing-addon2">
                                    <?php foreach ($statusBerkas as $status) { ?>
                                        <option value="<?php echo $status->nama; ?>"<?php echo ($status->nama == $brand->status_berkas) ? "selected='selected'" : ""; ?>>
                                            <?php echo $status->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="menu-show"> -->
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Catatan</label>
                            <div class="col-sm-5">
                                <textarea class="form-control" name="catatan"><?php echo $brand->catatan; ?></textarea>
                            </div>
                        </div>
                        <!--    </div> -->
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Latitude / Longitude</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Latitude" name="latitude" value="<?php echo $brand->latitude; ?>">
                                <br><font color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Longitude" name="longitude" value="<?php echo $brand->longitude; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">File CV</label>
                            <div class="col-sm-6">
                                <a href="<?php echo base_url() . $brand->file_cv; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_file_cv; ?></font></b></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="email" aria-describedby="sizing-addon2" value="<?php echo $brand->email; ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" value="<?php echo $dec_pass; ?>"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                        </div>      
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo base_url('master-opd'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </form>
            </div>
        </div>
        <?php if ($brand->folder != "") { ?>
            <div class="col-md-5">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">File Berkas Kelengkapan</h3>
                    </div>
                    <div class="box-body box-profile">
                        <form method="post" action="<?php echo base_url(); ?>Master/Master_opd/download" enctype="multipart/form-data">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th width="20px"><label class="container2">
                                            <input type="checkbox" id="check-all"><span class="checkmark"></span></label></th>
                                    <th>File</th>
                                </tr>
                                <?php
                                foreach ($resFold as $image) {
                                    $kalimat = $image;
                                    $path = base_url() . '' . $kalimat;
                                    $string = substr($kalimat, 37);
                                    echo '
                <tr>
                <td><label class="container2"><input type="checkbox" name="images[]" class="check-item" value="' . $kalimat . '"><span class="checkmark"></span></label></th></td>
                <td><a href="' . $path . '" target="_blank"><font class="font-data"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;' . $string . '</font></a></td>
                </tr>';
                                }

                                ?>
                                <br>
                            </table>
                            <br/>
                            <div class="box-footer">
                                <div class="pencet">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-md-5">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">File Berkas Kelengkapan</h3>
                    </div>
                    <div class="box-body box-profile">
                        <p>Belum ada kelengkapan berkas</p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }

    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var nama_lengkap = $("#nama_lengkap").val();
            var nama_lengkap = nama_lengkap.trim();
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama Lengkap wajib di isi.";
            }
        }
        if (error == 0) {
            var password = $("#password-field").val();
            var password = password.trim();
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            } else if (!cekpassword(password)) {
                error++;
                message = "Password tidak boleh ada spasi !! ";
            }
        }
        if (error == 0) {
            var nama_usaha = $("#nama_usaha").val();
            var nama_usaha = nama_usaha.trim();
            if (nama_usaha.length == 0) {
                error++;
                message = "Nama usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            var telp = $("#telp").val();
            var telp = telp.trim();
            if (telp.length == 0) {
                error++;
                message = "Telp usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_usaha = $("#alamat_usaha").val();
            var alamat_usaha = alamat_usaha.trim();
            if (alamat_usaha.length == 0) {
                error++;
                message = "Alamat usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('update-master-opd'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-update").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?php echo base_url("master-opd"); ?>'", 450);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });

    $(function () {
        $('.menu-show').hide();
        $('#status').change(function () {
            if ($('#status').val() == 'Belum Approve') {
                $('.menu-show').fadeIn("slow");
            } else {
                $('.menu-show').fadeOut("slow");
            }
        });
    });

</script>
<script type="text/javascript">
    $(function () {
        $('.pencet').hide();
        $("#check-all").click(function () {
            if ($(this).is(":checked")) {
                $(".check-item").prop("checked", true);
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
                $(".check-item").prop("checked", false);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('.pencet').hide();
        $(".check-item").click(function () {
            if ($(this).is(":checked")) {
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
            }
        });
    });
</script>