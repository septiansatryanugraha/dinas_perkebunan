<?php $this->load->view('_heading/_headerContent') ?>
<style>
    .after-add-more {
        margin-bottom:10px;
        width:495px;
        margin-left: 200px;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" enctype="multipart/form-data" id="form-update" method="POST">
                        <input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $brand->id_user; ?>">
                        <input type="hidden" class="form-control" name="id_sertifikasi" value="<?php echo $brand->id_sertifikasi; ?>">
                        <input type="hidden" name="kode" value="<?php echo $brand->kode; ?>">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Sertifikasi</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">No Sertifikat </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_sertifikat" id="no_sertifikat" value="<?php echo $brand->no_sertifikat; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            <a class="klik ajaxify" href="<?php echo base_url('master-sertifikasi'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var no_sertifikat = $("#no_sertifikat").val();
            var no_sertifikat = no_sertifikat.trim();
            if (no_sertifikat.length == 0) {
                error++;
                message = "No Sertifikat wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('update-qr'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-update").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?= base_url('master-sertifikasi'); ?>'", 500);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: true,
            format: 'dd-mm-yyyy'
        })
        $(".selek-usaha").select2({
            placeholder: " -- Jenis Sertifikasi -- "
        });
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $('.menu-show').hide();
        $('.menu-show2').hide();
        $('#status').change(function () {
            if ($('#status').val() == 'Survey') {
                $('.menu-show2').fadeOut("slow");
                $('.menu-show').fadeIn("slow");
            } else if ($('#status').val() == 'Selesai Survey') {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeIn("slow");
            } else {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeOut("slow");
            }
        });
    });

    function valid_sts() {
        var fileInput = document.getElementById("file_sts").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("file_sts").value = '';
                return false;
            }
            var ukuran = document.getElementById("file_sts");
            if (ukuran.files[0].size > 1007200) { // validasi ukuran size file
                swal("Peringatan", "File harus maksimal 1MB", "warning");
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }
</script>