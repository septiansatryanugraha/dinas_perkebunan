<?php $this->load->view('_heading/_headerContent') ?>
<style>
    .after-add-more {
        margin-bottom:10px;
        width:495px;
        margin-left: 200px;
    }
    #slider {
        margin-left: -140px; 
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $brand->id_user; ?>">
                        <input type="hidden" class="form-control" name="id_sertifikasi" value="<?php echo $brand->id_sertifikasi; ?>">
                        <input type="hidden" name="kode" value="<?php echo $brand->kode; ?>">
                        <input type="hidden" name="kode_iup" value="<?php echo $brand->kode_iup; ?>">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Sertifikasi</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Pemohon</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_pemohon" placeholder="Nama Pemohon" id="nama_pemohon" value="<?php echo $brand->nama_pemohon; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Alamat Pemohon</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_pemohon" placeholder="Alamat Pemohon" id="alamat_pemohon" value="<?php echo $brand->alamat_pemohon; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Usaha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" id="nama_usaha" value="<?php echo $brand->nama_usaha; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Alamat Usaha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_usaha" placeholder="Alamat Usaha" id="alamat_usaha" value="<?php echo $brand->alamat_usaha; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">No Telp</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_telp" placeholder="No Telp" id="no_telp" value="<?php echo $brand->no_telp; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" placeholder="Email" id="email" value="<?php echo $brand->email; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Surat Permohonan Sertifikat</label>
                                <div class="col-sm-7">
                                    <a href="<?php echo base_url() . $brand->surat_permohonan_sertifikat ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_pemohon; ?></font></b></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Surat Asal Usul Benih</label>
                                <div class="col-sm-7">
                                    <a href="<?php echo base_url() . $brand->surat_benih ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_surat_benih; ?></font></b></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Usaha</label>
                                <div class="col-sm-4">
                                    <select name="jenis_usaha" class="form-control selek-usaha" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_usaha as $data) { ?>
                                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $brand->jenis_usaha) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Komoditas</label>
                                <div class="col-sm-2">
                                    <select name="jenis_komoditas" class="form-control selek-usaha" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_komoditas as $data) { ?>
                                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $brand->jenis_komoditas) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Sertifikat</label>
                                <div class="col-sm-2">
                                    <select name="jenis_sertifikat" class="form-control selek-usaha" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_sertifikat as $data) { ?>
                                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $brand->jenis_sertifikat) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Koordinat / Keterangan</label>
                                <?php foreach ($loop as $data) { ?>
                                    <div class="input-group control-group after-add-more">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php echo '<input type="text" class="form-control" value=' . $data->latitude . ' disabled><br>'; ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?php echo '<input type="text" class="form-control" value=' . $data->longitude . ' disabled><br>'; ?>
                                            </div>

                                            <div class="col-md-4">
                                                <?php echo '<textarea class "form-control" disabled>' . $data->ket . '</textarea>'; ?>
                                            </div>
                                        </div>    
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Foto Lokasi</label>
                                <?php foreach ($loop as $data) { ?>
                                    <div class="input-group ">
                                        <div id="slider">
                                            <img class="img-thumbnail" src='../<?php echo $data->gambar; ?>' width="150px;">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-2">
                                    <select name="status" class="form-control selek-status" id="status">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?php echo $data->nama ?>"<?php echo ($data->nama == $brand->status) ? "selected='selected'" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="menu-show">
                                <?php if ($brand->tanggal_survey == NULL) { ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Survey</label>
                                        <div class="col-sm-2">
                                            <input type="text" name="tanggal_survey" id="tanggal" class="form-control datepicker" value="<?php echo date('01-m-Y'); ?>">
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Survey</label>
                                        <div class="col-sm-2">
                                            <input type="text" name="tanggal_survey" id="tanggal" class="form-control datepicker" value="<?php echo date('d-m-Y', strtotime($brand->tanggal_survey)); ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="menu-show2">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">STS</label>
                                    <div class="col-sm-5">
                                        <input type="file" name="file_sts" id="file_sts" onchange="return valid_sts()"/>
                                        <?php if ($brand->nama_sts != NULL) { ?>
                                            <a href="<?php echo base_url() . $brand->file_sts; ?>"><b><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo $brand->nama_sts; ?></font></b></a>
                                        <?php } ?>
                                        <p style='color: red; font-size: 14px;'> * maksimal size 1 MB, file berformat pdf</p>
                                    </div>
                                </div>
                            </div>
                            <div class="menu-show3">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Catatan</label>
                                    <div class="col-sm-5">
                                        <textarea class="form-control" name="catatan"><?php echo $brand->catatan; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            <a class="klik ajaxify" href="<?php echo base_url('master-sertifikasi'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var tanggal = $("#tanggal").val();
            var tanggal = tanggal.trim();
            if (tanggal.length == 0) {
                error++;
                message = "Tanggal Survey wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('update-master-sertifikasi'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-update").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?= base_url('master-sertifikasi'); ?>'", 500);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: true,
            format: 'dd-mm-yyyy'
        })
        $(".selek-usaha").select2({
            placeholder: " -- Jenis Sertifikasi -- "
        });
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $('.menu-show').hide();
        $('.menu-show2').hide();
        $('.menu-show3').hide();

        $('#status').change(function () {
            if ($('#status').val() == 'Survey') {
                $('.menu-show2').fadeOut("slow");
                $('.menu-show').fadeIn("slow");
                $('.menu-show3').fadeOut("slow");
            } else if ($('#status').val() == 'Selesai Survey') {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeIn("slow");
                $('.menu-show3').fadeOut("slow");
            } else if ($('#status').val() == 'Belum Approve') {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeOut("slow");
                $('.menu-show3').fadeIn("slow");
            } else {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeOut("slow");
            }
        });
    });

    function valid_sts() {
        var fileInput = document.getElementById("file_sts").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("file_sts").value = '';
                return false;
            }
            var ukuran = document.getElementById("file_sts");
            if (ukuran.files[0].size > 1007200) { // validasi ukuran size file
                swal("Peringatan", "File harus maksimal 1MB", "warning");
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }
</script>