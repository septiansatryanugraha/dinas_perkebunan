<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-4" style="margin-left:0px; margin-bottom:10px;">
                <div class="btn-group">
                    <button id="search-button" name="search-button" type="button" class="btn grey"><i class="fa fa-search"></i> Advanced Search</button>
                </div>
            </div>
            <br><br><br>
            <div class="search-form" style="display: none;margin-left: 20px">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Awal:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Akhir:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" >
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Filter</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode Pengajuan</th>
                            <th>No Sertifikat</th>
                            <th>Nama Pemohon</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Jenis Komoditas</th>
                            <th>Status</th>
                            <th style="width:125px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    // untuk datepicker
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
                sProcessing: "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>"
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('ajax-master-label') ?>",
                "type": "POST",
                data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-label", function () {
        var id_pengajuan = $(this).attr("data-id");
        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: true,
            html: true
        }, function () {
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('delete-master-label'); ?>",
                data: "id_pengajuan=" + id_pengajuan,
                success: function (data) {
                    $("tr[data-id='" + id_pengajuan + "']").fadeOut("fast", function () {
                        $(this).remove();
                    });
                    hapus_berhasil();
                    reload_table();
                }
            });
        });
    });
</script>