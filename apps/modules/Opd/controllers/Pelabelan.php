<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelabelan extends MX_Controller
{
    const __tableName = 'tbl_label';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_pengajuan';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_label');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function ambil_data()
    {

        $modul = $this->input->post('modul');
        $id = $this->input->post('id_sertifikasi');

        if ($modul == "nama_pemohon") {
            echo $this->Mdl_label->selek_data1($id);
        } else if ($modul == "alamat_pemohon") {
            echo $this->Mdl_label->selek_data2($id);
        } else if ($modul == "nama_usaha") {
            echo $this->Mdl_label->selek_data3($id);
        } else if ($modul == "alamat_usaha") {
            echo $this->Mdl_label->selek_data4($id);
        } else if ($modul == "no_telp") {
            echo $this->Mdl_label->selek_data5($id);
        } else if ($modul == "email") {
            echo $this->Mdl_label->selek_data6($id);
        }

        if ($modul == "kode_sertifikasi") {
            echo $this->Mdl_label->selek_data7($id);
        } else if ($modul == "jenis_usaha") {
            echo $this->Mdl_label->selek_data8($id);
        } else if ($modul == "jenis_komoditas") {
            echo $this->Mdl_label->selek_data9($id);
        }

        if ($modul == "no_sertifikat") {
            echo $this->Mdl_label->selek_data10($id);
        }
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {

            $data = array(
                'title' => 'Data Pelabelan',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );


            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/home_label', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function ajax_list()
    {
        $id_user = $this->session->userdata('id');
        $list = $this->Mdl_label->get_where_custom('id_user', $id_user);

        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            $status = '<a class="ui red label">Pending</a>';
            if ($brand->status == 'Approve Pelabelan') {
                $status = '<a class="ui green label">Approve Pelabelan</a>';
            } elseif ($brand->status == 'Belum Approve') {
                $status = '<a class="ui blue label">Belum Approve</a>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_sertifikasi;
            $row[] = $brand->no_sertifikat;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->alamat_pemohon;
            $row[] = $status;
            //add html for action

            if ($brand->status == 'Approve Pelabelan') {
                $buttonEdit = '<a class="ui green label">Approve Pelabelan dan Proses Cetak Pelabelan</a>';
            } elseif ($brand->status == 'Belum Approve') {
                $buttonEdit = '<span tooltip=" Edit Pengajauan"><div class="ui icon buttons">' . anchor('edit-pengajuan-label/' . $brand->id_pengajuan, ' <i class="fa fa-edit"></i>', ' class="positive ui button"');
            }

            $row[] = $buttonEdit;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add_pengajuan()
    {
        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Tambah Permohonan Form Pelabelan',
                'kat_sertifikat' => $this->Mdl_label->selekKategori(),
                'kat_usaha' => $this->Mdl_label->selekKategori2(),
                'sessid' => $this->session->userdata('id'),
                'dinas' => $this->session->userdata('dinas'),
                'komoditas' => $this->Mdl_label->selekKomoditas(),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
            $data['selekKode'] = $this->Mdl_label->selekKodePengajuan($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/tambah_label', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function prosesAdd()
    {
        $errMessage = "";
        $date = date('Y-m-d');
        $date2 = date('Y-m-d');

        $nama_pemohon = $this->input->post('nama_pemohon');
        $Komoditas = $this->input->post('jenis_komoditas');
        $KodeSertifikasi = $this->input->post('kode_sertifikasi');
        $NoSertifikat = $this->input->post('no_sertifikat');

        $data = array(
            'kode_sertifikasi' => $KodeSertifikasi,
            'id_user' => $this->input->post('id_user'),
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'no_sertifikat' => $this->input->post('no_sertifikat'),
            'alamat_pemohon' => $this->input->post('alamat_pemohon'),
            'nama_usaha' => $this->input->post('nama_usaha'),
            'alamat_usaha' => $this->input->post('alamat_usaha'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'jenis_usaha' => $this->input->post('jenis_usaha'),
            'jenis_komoditas' => $this->input->post('jenis_komoditas'),
            'jumlah_label' => $this->input->post('jumlah_label'),
            'status' => 'Pending',
            'created_date' => $date,
            'updated_date' => $date,
        );

        $data2 = array(
            'kode' => $KodeSertifikasi,
            'id_user' => $this->input->post('id_user'),
            'status' => 'Pengajuan Pelabelan',
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan proses pengajuan Pelabelan dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $checkKode = $this->Mdl_label->checkKode($data);
        if (!$checkKode) {
            $result = $this->db->insert(self::__tableName, $data);
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $errMessage = "Maaf gagal menyimpan data !!";
                $out['status'] = $errMessage;
            }
        } else {

            $errMessage = " Maaf Pengajuan dengan Nomor Sertifikat " . $NoSertifikat . "  sudah ada di dalam sistem !! ";
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Permohonan Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $data['pengajuan'] = $this->Mdl_label->selectById($id);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data_sertifikasi', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function Edit_pengajuan($id)
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Permohonan Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
            $data['pengajuan'] = $this->Mdl_label->selectById($id);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/edit_data_label', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function prosesUpdateLabel()
    {

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $kode = $this->input->post('kode');
        $Komoditas = $this->input->post('jenis_komoditas');
        $nama_pemohon = $this->input->post('nama_pemohon');

        $this->db->trans_begin();

        $data = array(
            'jumlah_label' => $this->input->post('jumlah_label'),
            'status' => 'Revisi',
        );

        $data2 = array(
            'kode' => $kode,
            'id_user' => $this->input->post('id_user'),
            'status' => 'Revisi',
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan revisi dan proses pengajuan pelabelan dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
        $result = $this->db->insert(self::__tableName2, $data2);

        if ($this->db->trans_status() === FALSE) {
            $out['status'] = 'gagal';
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $errMessage = "Maaf gagal menyimpan data !!";
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesUpdate()
    {

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "bukti_tf_" . $new_name;
        $config['upload_path'] = "./upload/transfer/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG';
        $config['max_size'] = '2048'; //maksimum besar file 5M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();


        if ($this->upload->do_upload("bukti_pembayaran")) {

            $data_file = $this->upload->data();
            $path['link'] = "upload/transfer/";


            $nama_pemohon = $this->input->post('nama_pemohon');
            $data = array(
                'bukti_pembayaran' => $path['link'] . '' . $data_file['file_name'],
                // 'updated_by'         => $username,
                'updated_date' => $date,
                'tanggal' => $date2,
            );

            $data2 = array(
                'kode' => $this->input->post('kode'),
                'id_user' => $this->input->post('id_user'),
                'status' => 'Upload Bukti Transfer',
                'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sudah melakukan pembayaran dan telah melakukan proses upload bukti transfer STS',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $out['status'] = 'gagal';
            }
        }


        echo json_encode($out);
    }
}
