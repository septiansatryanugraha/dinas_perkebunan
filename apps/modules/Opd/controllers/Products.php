<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller
{
    const __tableName = 'tbl_form_rekomendasi';
    const __tableId = 'id_rekomendasi';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_products');
        $this->load->model('Mdl_registrasi');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    public function add_dokumen()
    {
        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Tambah Form Permohonan Rekomendasi',
                'category' => $this->mdl_category->get('category', 'ASC'),
                'kat_usaha' => $this->Mdl_products->selekKategori2(),
                'file' => $this->Mdl_products->selek_file(),
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/tambah', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function ajax_list()
    {
        $id_user = $this->session->userdata('id');
        $list = $this->Mdl_products->get_where_custom('id_user', $id_user);

        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            $status = '<a class="ui blue label">Belum Direkomendasi</a>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<a class="ui red label">Telah Direkomendasi</a>';
            }

            if ($brand->kode_rekomendasi == NULL) {
                $kode_rekomendasi = '<a class="ui red label">Belum Mendapatkan Kode Rekomendasi</a>';
            } else {
                $kode_rekomendasi = $brand->kode_rekomendasi;
            }

            if ($brand->kode_iup == NULL) {
                $kode_iup = '<a class="ui red label">Belum Ada Kode IUP</a>';
            } else {
                $kode_iup = $brand->kode_iup;
            }

            if ($brand->surat_rekom == NULL) {
                $file_rekom = '<a class="ui red label">Belum ada file</a>';
            } elseif ($brand->tipe == 'memiliki surat rekomendasi') {
                $file_rekom = '<a class="ui blue label">sudah memiliki file rekomendasi</a>';
            } else {
                $file_rekom = 'Silahkan download file dibawah ini <a href="' . $brand->surat_rekom . '" target="_blank"> &nbsp; ' . $brand->nama_file_rekom . '</a>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kode_rekomendasi;
            $row[] = $brand->nama_pemohon;
            $row[] = $kode_iup;
            $row[] = $file_rekom;
            $row[] = $status;
            //add html for action

            if ($brand->kode_rekomendasi == NULL) {
                $buttonEdit = '<a class="ui green label">Belum Bisa Upload IUP</a>';
            } elseif ($brand->status == 'Rekomendasi') {
                $buttonEdit = '<a class="ui green label">Telah Upload IUP</a>';
            } else {

                $buttonEdit = '<span tooltip="Upload IUP"><div class="ui icon buttons">' . anchor('upload-iup/' . $brand->id_rekomendasi, ' <i class="fa fa-upload"></i>', ' class="positive ui button"');
            }



            // $buttonDel = '<button class="negative ui positive button hapus-employe" data-id=' . "'" . $brand->id_products . "'" . '><i class="remove circle icon"></button>';

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_history()
    {
        $id_user = $this->session->userdata('id');
        $list = $this->Mdl_products->get_where_history('id_user', $id_user);


        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            if ($brand->kode == NULL) {
                $kodeRegistrasi = '<a class="ui red label">Belum Mendapatkan Kode Registrasi</a>';
            } else {
                $kodeRegistrasi = $brand->kode;
            }

            $no++;
            $row = array();
            // $row[] = $no;
            $row[] = $kodeRegistrasi;
            $row[] = '<a class="ui blue label"> Status ' . $brand->status . '</a><br>' . $brand->keterangan . '<br><b>' . date('d-m-Y H:i:s', strtotime($brand->created_date)) . '</b>';
            //add html for action
            // $buttonEdit ='<div class="ui icon buttons">'.anchor('edit-employe/' . $brand->id_dokumen, ' <i class="edit icon"></i> ', ' class="positive ui button"');
            // $buttonDel = '<button class="negative ui positive button hapus-employe" data-id=' . "'" . $brand->id_products . "'" . '><i class="remove circle icon"></button>';

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function prosesAdd()
    {

        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');
        $kode = $this->Mdl_products->kode();

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "file_" . $new_name;
        $config['upload_path'] = "./upload/berkas_kelengkapan/";
        $config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png|jpeg';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();


        $this->upload->do_upload('surat_rekomendasi');
        $surat_rekomendasi = $this->upload->data();

        $this->upload->do_upload('surat_kerjasama');
        $surat_kerjasama = $this->upload->data();

        $this->upload->do_upload('surat_patuh');
        $surat_patuh = $this->upload->data();

        $data_surat_rekomendasi = $surat_rekomendasi;
        $data_surat_kerjasama = $surat_kerjasama;
        $data_surat_patuh = $surat_patuh;


        $path['link'] = "upload/berkas_kelengkapan/";

        $nama_pemohon = $this->input->post('nama_pemohon');

        $data = array(
            'kode' => $kode,
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'alamat_pemohon' => $this->input->post('alamat_pemohon'),
            'nama_usaha' => $this->input->post('nama_usaha'),
            'alamat_usaha' => $this->input->post('alamat_usaha'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'jenis_usaha' => $this->input->post('jenis_usaha'),
            'folder' => $this->input->post('folder'),
            'id_user' => $this->input->post('id_user'),
            'nama_surat_rekomendasi' => $data_surat_rekomendasi['file_name'],
            'surat_rekomendasi' => $path['link'] . '' . $data_surat_rekomendasi['file_name'],
            'nama_surat_kerjasama' => $data_surat_rekomendasi['file_name'],
            'surat_kerjasama' => $path['link'] . '' . $data_surat_kerjasama['file_name'],
            'nama_surat_patuh' => $data_surat_rekomendasi['file_name'],
            'surat_patuh' => $path['link'] . '' . $data_surat_patuh['file_name'],
            'tipe' => 'pengajuan permohonan sertifikasi',
            'created_date' => $date,
            'updated_date' => $date,
            'tanggal' => $date2,
        );

        $data2 = array(
            'kode' => $kode,
            'id_user' => $this->input->post('id_user'),
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan proses pengajuan sertifikasi ke Dinas Perkebunan Jawa Timur',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $result = $this->db->insert('tbl_form_rekomendasi', $data);
        $result = $this->db->insert('tbl_history', $data2);
        $this->do_upload_others_images();

        if ($this->db->trans_status() === FALSE) {
            $out['status'] = 'gagal';
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = 'gagal';
        }

        echo json_encode($out);
    }
    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    private function do_upload_others_images()
    {
        $upath = './upload/berkas_kelengkapan/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['others']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['others']['name'] = $files['others']['name'][$i];
            $_FILES['others']['type'] = $files['others']['type'][$i];
            $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
            $_FILES['others']['error'] = $files['others']['error'][$i];
            $_FILES['others']['size'] = $files['others']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('others');
        }
    }

    public function prosesUpdate()
    {

        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "file_iup_" . $new_name;
        $config['upload_path'] = "./upload/file_iup/";
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $this->upload->do_upload('surat_iup');
        $surat_iup = $this->upload->data();
        $data_surat_iup = $surat_iup;

        $path['link'] = "upload/file_iup/";

        $data = array(
            'nama_surat_iup' => $data_surat_iup['file_name'],
            'surat_iup' => $path['link'] . '' . $data_surat_iup['file_name'],
        );

        $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

        if ($this->db->trans_status() == FALSE) {
            $out['status'] = 'gagal';
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = 'gagal';
        }

        echo json_encode($out);
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {

            $data = array(
                'title' => 'Dokumen Rekomendasi',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );


            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/home', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    function history()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {

            $data = array(
                'title' => 'Proses Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );


            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/proses', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function Edit($id)
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Upload File IUP',
                'sessid' => $this->session->userdata('id'),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'kat_usaha' => $this->Mdl_products->selekKategori2(),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $data['pengajuan'] = $this->Mdl_products->selectById($id);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function Add_baru()
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Permohonan Rekomendasi',
                'sessid' => $this->session->userdata('id'),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'kat_usaha' => $this->Mdl_products->selekKategori2(),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }
}
