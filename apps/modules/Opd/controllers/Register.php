<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_registrasi');
    }

    public function index()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = array(
                'title' => 'Daftar akun - Dinas Perkebunan',
                'style' => '<style type="text/css">body {background-color: #DADADA;}.ui.two.column.centered.grid {margin-top: 1px;}</style>'
            );
            $this->load->view('front/global/header', $data);
            $this->load->view('register');
            $this->load->view('front/global/footer', $data);
        }
    }

    function submit()
    {
        $this->form_validation->set_rules('_nama_lengkap', 'Nama Lengkap', 'required|min_length[5]|xss_clean');
        $this->form_validation->set_rules('_email', 'Email', 'required|min_length[5]|valid_email|is_unique[tbl_user.email]|xss_clean');
        $this->form_validation->set_rules('_password', 'Password', 'required|min_length[5]|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $data = array(
                'title' => 'Daftar akun - Prima PSBP Dinas Perkebunan Provinsi Jawa Timur',
                'style' => '<style type="text/css">body {background-color: #DADADA;}.ui.two.column.centered.grid {margin-top: 1px;}</style>'
            );

            $this->load->view('front/global/header', $data);
            $this->load->view('register');
            $this->load->view('front/global/footer', $data);
        } else {

            $date = date('Y-m-d H:i:s');
            $data = array(
                'nama_lengkap' => $this->input->post('_nama_lengkap'),
                'nama' => $this->input->post('_nama_lengkap'),
                'email' => $this->input->post('_email'),
                'folder' => $this->input->post('folder'),
                'password' => base64_encode($this->input->post("_password")),
                'status' => 'Non aktif',
                'status_berkas' => 'Belum Approve',
                'ip_address' => $this->input->ip_address(),
                'user_agent' => $this->input->user_agent(),
                'created_by' => 'System',
                'created_date' => $date,
                'updated_date' => $date
            );


            $this->db->insert('tbl_user', $data);
            $this->do_upload_others_images();
            $this->session->set_flashdata('success', '<div class="ui success message"><i class="close icon"></i><div class="header">Selamat Pendaftaran akun berhasil, Silahkan login !!</div></div>');
            redirect('daftar-akun');
        }
    }

    private function do_upload_others_images()
    {
        $upath = './upload/berkas_kelengkapan/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }
    }
}
