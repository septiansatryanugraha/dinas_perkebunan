<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_products');
        $this->load->model('Mdl_web');
        $this->load->model('Mdl_registrasi');
    }

    public function history()
    {
        $idUser = $this->session->userdata('id');

        $view = $this->input->post('view');
        if ($view != '') {
            $update_status = $this->Mdl_web->updateStatusHistory($idUser);
        }
        $selek_notif = $this->Mdl_web->selectNotifHistory($idUser);

        if (!empty($selek_notif)) {
            foreach ($selek_notif as $data) {
                $dataPhoto = base_url() . 'upload/user/no-image.jpg';

                $output .= '<div class = "sec new">
            			<a href="' . base_url() . 'data-sertifikasi">  
            			<div class = "profCont">
            			<img class = "profile" src="' . $dataPhoto . '">
            			</div>
            			<div class = "txt">' . $data->keterangan . '</div>
              			<div class = "txt sub">' . date('d-m-Y', strtotime($data->tanggal)) . '</div></a></div>';
            }
        } else {
            $output .= '<div class = "nothing"> <i class="fas fa-child stick"></i> <div class = "cent">Maaf belum ada pengajuan</div></div>';
        }

        $count = $this->Mdl_web->totalCountHistory($idUser);
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function index()
    {

        $data = array(
            'query' => $this->Mdl_web->get('id_category'),
            'sessid' => $this->session->userdata('id'),
            'nama' => $this->session->userdata('nama_lengkap'),
            'title' => "Dashboard",
            'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
        );

        $id_user = $this->session->userdata('id');
        $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);


        $this->load->view('front/global/header', $data);
        $this->load->view('front/global/nav_menu', $data);
        $this->load->view('back/user/sidebar_dashboard');
        $this->load->view('dashboard', $data);
        $this->load->view('front/global/footer', $data);
    }
}
