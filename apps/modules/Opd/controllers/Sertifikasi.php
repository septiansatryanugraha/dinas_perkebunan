<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikasi extends MX_Controller
{
    const __tableName = 'tbl_sertifikasi';
    const __tableName2 = 'tbl_history';
    const __tableName3 = 'tbl_koordinat';
    const __tableId = 'id_sertifikasi';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_sertifikasi');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function ambil_data()
    {

        $modul = $this->input->post('modul');
        $id = $this->input->post('id_user');

        if ($modul == "nama_pemohon") {
            echo $this->Mdl_sertifikasi->selek_data1($id);
        } else if ($modul == "alamat_pemohon") {
            echo $this->Mdl_sertifikasi->selek_data2($id);
        } else if ($modul == "nama_usaha") {
            echo $this->Mdl_sertifikasi->selek_data3($id);
        } else if ($modul == "alamat_usaha") {
            echo $this->Mdl_sertifikasi->selek_data4($id);
        } else if ($modul == "no_telp") {
            echo $this->Mdl_sertifikasi->selek_data5($id);
        } else if ($modul == "email") {
            echo $this->Mdl_sertifikasi->selek_data6($id);
        }

        if ($modul == "kd_iup") {
            echo $this->Mdl_sertifikasi->selek_data7($id);
        }
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {

            $data = array(
                'title' => 'Data Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );


            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/home_sertifikasi', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function ajax_list()
    {
        $id_user = $this->session->userdata('id');
        $list = $this->Mdl_sertifikasi->get_where_custom('id_user', $id_user);

        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            // $Kode_ser =   $brand->kode_rekomendasi;
            //          $dataKode = $this->Mdl_sertifikasi->select_kode($Kode_ser);
            //          $Kode = $dataKode->kode_rekomendasi;

            $status = '<a class="ui blue label">Belum Approve</a>';
            if ($brand->status == 'Survey') {
                $status = '<a class="ui red label">Survey</a>';
            } elseif ($brand->status == 'Selesai Survey') {
                $status = '<a class="ui green label">Selesai Survey</a>';
            } elseif ($brand->status == 'Selesai Approve') {
                $status = '<a class="ui green label">Approve Sertifikat</a>';
            } elseif ($brand->status == 'Revisi') {
                $status = '<a class="ui blue label">Revisi User</a>';
            }

            if ($brand->tanggal_survey == NULL) {
                $tanggal_survey = '<a class="ui blue label">Belum Dijadwalkan</a>';
            } else {
                $tanggal_survey = date('d-m-Y', strtotime($brand->tanggal_survey));
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_iup;
            $row[] = $brand->nama_pemohon;
            $row[] = $tanggal_survey;
            $row[] = $status;
            //add html for action

            if ($brand->status == 'Selesai Survey') {
                $buttonEdit = '<span tooltip=" Upload Pembayaran"><div class="ui icon buttons">' . anchor('view-sertifikasi/' . $brand->id_sertifikasi, ' <i class="fa fa-upload"></i>', ' class="positive ui button"');
            } elseif ($brand->status == 'Selesai Approve') {
                $buttonEdit = '<a class="ui green label">Dokumen Segera Dikirim</a>';
            } elseif ($brand->status == 'Belum Approve' || $brand->status == 'Revisi') {
                $buttonEdit = '<span tooltip=" Edit Pengajauan"><div class="ui icon buttons">' . anchor('edit-pengajuan-sertifikasi/' . $brand->id_sertifikasi, ' <i class="fa fa-edit"></i>', ' class="positive ui button"');
            } else {
                $buttonEdit = '<a class="ui green label">Belum di Proses</a>';
            }

            // $buttonDel = '<button class="negative ui positive button hapus-employe" data-id=' . "'" . $brand->id_products . "'" . '><i class="remove circle icon"></button>';

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add_dokumen()
    {
        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Tambah Permohonan Form Sertifikasi',
                'kat_sertifikat' => $this->Mdl_sertifikasi->selekKategori(),
                'kat_usaha' => $this->Mdl_sertifikasi->selekKategori2(),
                'sessid' => $this->session->userdata('id'),
                'dinas' => $this->session->userdata('dinas'),
                'komoditas' => $this->Mdl_sertifikasi->selekKomoditas(),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
            $data['selekKode'] = $this->Mdl_sertifikasi->selekKategori3($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/tambah_sertifikasi', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function prosesAdd()
    {

        $errMessage = "";
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');
        $kode2 = $this->Mdl_sertifikasi->kode();

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "berkas_kelengkapan_" . $new_name;
        $config['upload_path'] = "./upload/kelengkapan_sertifikasi/";
        $config['allowed_types'] = 'pdf|PDF|jpg|JPG|png|PNG|jpeg|JPEG';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $this->upload->do_upload('surat_permohonan_sertifikat');
        $surat_permohonan_sertifikat = $this->upload->data();
        $data_surat_permohonan_sertifikat = $surat_permohonan_sertifikat;

        $this->upload->do_upload('surat_benih');
        $surat_benih = $this->upload->data();
        $data_surat_benih = $surat_benih;

        $this->upload->do_upload('gambar');
        $gbr_kegiatan = $this->upload->data();
        $data_gbr_kegiatan = $gbr_kegiatan;

        $path['link'] = "upload/kelengkapan_sertifikasi/";
        $Kode = $this->input->post('kode_rekomendasi');
        $Kode_iup = $this->input->post('kode_iup');

        $nama_pemohon = $this->input->post('nama_pemohon');
        $kode_rekomendasi = $this->input->post('kode_rekomendasi');
        $Komoditas = $this->input->post('jenis_komoditas');

        $data = array(
            'kode' => $kode2,
            'id_user' => $this->input->post('id_user'),
            'kode_iup' => $this->input->post('kode_iup'),
            'nama_surat_pemohon' => $data_surat_permohonan_sertifikat['file_name'],
            'surat_permohonan_sertifikat' => $path['link'] . '' . $data_surat_permohonan_sertifikat['file_name'],
            'nama_surat_benih' => $data_surat_benih['file_name'],
            'surat_benih' => $path['link'] . '' . $data_surat_benih['file_name'],
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'alamat_pemohon' => $this->input->post('alamat_pemohon'),
            'nama_usaha' => $this->input->post('nama_usaha'),
            'alamat_usaha' => $this->input->post('alamat_usaha'),
            'no_telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'jenis_usaha' => $this->input->post('jenis_usaha'),
            'jenis_komoditas' => $this->input->post('jenis_komoditas'),
            'status' => 'Pengajuan',
            'created_date' => $date,
            'updated_date' => $date,
            'tanggal' => $date2
        );

        $data2 = array(
            'kode' => $kode2,
            'kode_iup' => $this->input->post('kode_iup'),
            'id_user' => $this->input->post('id_user'),
            'status' => 'Pengajuan',
            'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan proses pengajuan Sertifikasi dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur dengan Kode IUP<b> ' . $Kode_iup . '</b>',
            'created_by' => 'System',
            'created_date' => $date,
            'tanggal' => $date2,
        );

        $latitude = $this->input->post('latitude');
        $jenis_komoditas = $this->input->post('jenis_komoditas');
        $jenis_usaha = $this->input->post('jenis_usaha');

        $data3 = array();
        foreach ($latitude AS $key => $val) {
            $data3[] = array(
                "kode_sertifikasi" => $kode2,
                "latitude" => $_POST['latitude'][$key],
                "longitude" => $_POST['longitude'][$key],
                "gambar" => $path['link'] . '' . $data_gbr_kegiatan['file_name'],
                "jenis_komoditas" => $jenis_komoditas,
                "jenis_usaha" => $jenis_usaha,
                "ket" => $_POST['ket'][$key],
                "created_date" => $date
            );
        }

        $result = $this->db->insert(self::__tableName, $data);
        $result = $this->db->insert(self::__tableName2, $data2);
        $result = $this->db->insert_batch(self::__tableName3, $data3);

        if ($this->db->trans_status() === FALSE) {
            $out['status'] = 'gagal';
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $errMessage = "Maaf gagal menyimpan data !!";
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Permohonan Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $data['pengajuan'] = $this->Mdl_sertifikasi->selectById($id);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data_sertifikasi', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function Edit_pengajuan($id)
    {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');

            $data = array(
                'title' => 'Form Permohonan Sertifikasi',
                'sessid' => $this->session->userdata('id'),
                'kat_usaha' => $this->Mdl_sertifikasi->selekKategori2(),
                'komoditas' => $this->Mdl_sertifikasi->selekKomoditas(),
                'category' => $this->mdl_category->get('category', 'ASC'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
            $data['pengajuan'] = $this->Mdl_sertifikasi->selectById($id);
            $product = $this->Mdl_sertifikasi->selectById($id);
            $tukar = $product->kode;

            $data['loop'] = $this->Mdl_sertifikasi->selekTes($tukar);


            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/edit_data_sertifikasi', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function prosesUpdateSertifikasi()
    {

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $kode = $this->input->post('kode');
        $Komoditas = $this->input->post('jenis_komoditas');

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "berkas_kelengkapan_" . $new_name;
        $config['upload_path'] = "./upload/kelengkapan_sertifikasi/";
        $config['allowed_types'] = 'pdf|PDF|jpg|JPG|png|PNG|jpeg|JPEG';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $path['link'] = "upload/kelengkapan_sertifikasi/";

        if ($this->upload->do_upload('surat_permohonan_sertifikat')) {
            $idPengajuan = trim($this->input->post(self::__tableId));
            $data_kode = array('id_sertifikasi' => $idPengajuan);
            $file = $this->db->get_where('tbl_sertifikasi', $data_kode);
            if ($file->num_rows() > 0) {
                $pros = $file->row();
                $name = $pros->surat_permohonan_sertifikat;

                if (file_exists($lok = FCPATH . $name)) {
                    unlink($lok);
                }
            }

            $image_data = $this->upload->data();
            $data = array(
                'nama_surat_pemohon' => $image_data['file_name'],
                'surat_permohonan_sertifikat' => $path['link'] . '' . $image_data['file_name'],
                'nama_pemohon' => $this->input->post('nama_pemohon'),
                'alamat_pemohon' => $this->input->post('alamat_pemohon'),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'no_telp' => $this->input->post('no_telp'),
                'jenis_usaha' => $this->input->post('jenis_usaha'),
                'jenis_komoditas' => $this->input->post('jenis_komoditas'),
                'status' => 'Revisi',
            );

            $data2 = array(
                'kode' => $kode,
                'kode_iup' => $this->input->post('kode_iup'),
                'id_user' => $this->input->post('id_user'),
                'status' => 'Revisi',
                'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan revisi dan proses pengajuan ulang Sertifikasi dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $data3 = array(
                "latitude" => $this->input->post('latitude'),
                "longitude" => $this->input->post('longitude'),
                "jenis_komoditas" => $this->input->post('jenis_komoditas'),
                "jenis_usaha" => $this->input->post('jenis_usaha'),
                "ket" => $this->input->post('ket'),
                "created_date" => $date
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);
            $result = $this->db->update(self::__tableName3, $data3, array('kode_sertifikasi' => $kode));

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $errMessage = "Maaf gagal menyimpan data !!";
                $out['status'] = $errMessage;
            }
        } elseif ($this->upload->do_upload('surat_benih')) {

            $idPengajuan = trim($this->input->post(self::__tableId));
            $data_kode = array('id_sertifikasi' => $idPengajuan);
            $file = $this->db->get_where('tbl_sertifikasi', $data_kode);
            if ($file->num_rows() > 0) {
                $pros = $file->row();
                $name = $pros->surat_benih;

                if (file_exists($lok = FCPATH . $name)) {
                    unlink($lok);
                }
            }

            $image_data2 = $this->upload->data();
            $data = array(
                'nama_surat_benih' => $image_data2['file_name'],
                'surat_benih' => $path['link'] . '' . $image_data2['file_name'],
                'nama_pemohon' => $this->input->post('nama_pemohon'),
                'alamat_pemohon' => $this->input->post('alamat_pemohon'),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'no_telp' => $this->input->post('no_telp'),
                'jenis_usaha' => $this->input->post('jenis_usaha'),
                'jenis_komoditas' => $this->input->post('jenis_komoditas'),
                'status' => 'Revisi',
            );

            $data2 = array(
                'kode' => $kode,
                'kode_iup' => $this->input->post('kode_iup'),
                'id_user' => $this->input->post('id_user'),
                'status' => 'Revisi',
                'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan revisi dan proses pengajuan ulang Sertifikasi dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $data3 = array(
                "latitude" => $this->input->post('latitude'),
                "longitude" => $this->input->post('longitude'),
                "jenis_komoditas" => $this->input->post('jenis_komoditas'),
                "jenis_usaha" => $this->input->post('jenis_usaha'),
                "ket" => $this->input->post('ket'),
                "created_date" => $date
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);
            $result = $this->db->update(self::__tableName3, $data3, array('kode_sertifikasi' => $kode));

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $errMessage = "Maaf gagal menyimpan data !!";
                $out['status'] = $errMessage;
            }
        } else {

            $data = array(
                'nama_pemohon' => $this->input->post('nama_pemohon'),
                'alamat_pemohon' => $this->input->post('alamat_pemohon'),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'no_telp' => $this->input->post('no_telp'),
                'jenis_usaha' => $this->input->post('jenis_usaha'),
                'jenis_komoditas' => $this->input->post('jenis_komoditas'),
                'status' => 'Revisi',
            );

            $data2 = array(
                'kode' => $kode,
                'kode_iup' => $this->input->post('kode_iup'),
                'id_user' => $this->input->post('id_user'),
                'status' => 'Revisi',
                'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sedang melakukan revisi dan proses pengajuan ulang Sertifikasi dengan jenis Komoditas <b>' . $Komoditas . '</b> ke Dinas Perkebunan Jawa Timur',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $data3 = array(
                "latitude" => $this->input->post('latitude'),
                "longitude" => $this->input->post('longitude'),
                "jenis_komoditas" => $this->input->post('jenis_komoditas'),
                "jenis_usaha" => $this->input->post('jenis_usaha'),
                "ket" => $this->input->post('ket'),
                "created_date" => $date
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);
            $result = $this->db->update(self::__tableName3, $data3, array('kode_sertifikasi' => $kode));

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $errMessage = "Maaf gagal menyimpan data !!";
                $out['status'] = $errMessage;
            }
        }
        echo json_encode($out);
    }

    public function prosesUpdate()
    {

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "bukti_tf_" . $new_name;
        $config['upload_path'] = "./upload/transfer/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG';
        $config['max_size'] = '2048'; //maksimum besar file 5M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();


        if ($this->upload->do_upload("bukti_pembayaran")) {

            $data_file = $this->upload->data();
            $path['link'] = "upload/transfer/";


            $nama_pemohon = $this->input->post('nama_pemohon');
            $data = array(
                'bukti_pembayaran' => $path['link'] . '' . $data_file['file_name'],
                // 'updated_by'         => $username,
                'updated_date' => $date,
                'tanggal' => $date2,
            );

            $data2 = array(
                'kode' => $this->input->post('kode'),
                'id_user' => $this->input->post('id_user'),
                'status' => 'Upload Bukti Transfer',
                'keterangan' => 'user <b>' . $nama_pemohon . '</b> Sudah melakukan pembayaran dan telah melakukan proses upload bukti transfer STS',
                'created_by' => 'System',
                'created_date' => $date,
                'tanggal' => $date2,
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $result = $this->db->insert(self::__tableName2, $data2);

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $out['status'] = 'gagal';
            }
        }


        echo json_encode($out);
    }
}
