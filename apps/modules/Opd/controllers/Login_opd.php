<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_opd extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_login');
    }

    public function index()
    {
        if ($this->lib->login() != "") {
            redirect('Login_opd/logout');
        } else {
            $this->form_validation->set_rules('_email', 'Email', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('_password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_error_delimiters('<p style="color:red;">', '</p>');
            $this->form_validation->set_rules('g-recaptcha-response', 'Recaptcha', 'trim|required');

            $recaptcha = $this->input->post('g-recaptcha-response');
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if ($this->form_validation->run() == TRUE || !isset($response['success']) || $response['success'] == true) {
                $email = $this->input->post('_email');
                $password = base64_encode($this->input->post('_password'));
                $check = $this->mdl_login->login(array('email' => $email), array('password' => $password));
                if ($check == TRUE) {
                    foreach ($check as $user) {
                        if ($user->status == "Non aktif") {
                            $data = array(
                                'error' => 'Maaf, Akun anda belum aktif!',
                                'title' => 'Login',
                                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                                'script_captcha' => $this->recaptcha->getScriptTag(),
                                'style' => '<style type="text/css">body {background-color: #DADADA;}body > .grid {height: 100%;}.image {margin-top: -100px;}.column {max-width: 450px;}</style>'
                            );
                            $this->load->view('front/global/header', $data);
                            $this->load->view('login', $data);
                            $this->load->view('front/global/footer', $data);
                        } else {
                            $this->session->set_userdata(array(
                                'email' => $user->email,
                                'id' => $user->id_user,
                                'nama_lengkap' => $user->nama_lengkap,
                                'alamat' => $user->alamat,
                                'latitude' => $user->latitude,
                                'longitude' => $user->longitude,
                                'status' => $user->status,
                                'status_berkas' => $user->status_berkas,
                                'nama' => $user->nama,
                                'password' => $user->password
                            ));
                            redirect('beranda');
                        }
                    }
                } else {
                    $data = array(
                        'error' => 'Maaf, Email atau Password salah!',
                        'title' => 'Login',
                        'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                        'script_captcha' => $this->recaptcha->getScriptTag(),
                        'style' => '<style type="text/css">body {background-color: #DADADA;}body > .grid {height: 100%;}.image {margin-top: -100px;}.column {max-width: 450px;}</style>'
                    );
                    $this->load->view('front/global/header', $data);
                    $this->load->view('login', $data);
                    $this->load->view('front/global/footer', $data);
                }
            } elseif (isset($recaptcha)) {
                $data = array(
                    'error' => 'Captcha Kosong !!',
                    'title' => 'Login',
                    'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                    'script_captcha' => $this->recaptcha->getScriptTag(),
                    'style' => '<style type="text/css">body {background-color: #DADADA;}body > .grid {height: 100%;}.image {margin-top: -100px;}.column {max-width: 450px;}</style>'
                );
                $this->load->view('front/global/header', $data);
                $this->load->view('login', $data);
                $this->load->view('front/global/footer', $data);
            } else {
                $data = array(
                    'title' => 'Login',
                    'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                    'script_captcha' => $this->recaptcha->getScriptTag(),
                    'style' => '<style type="text/css">body {background-color: #DADADA;}body > .grid {height: 100%;}.image {margin-top: -100px;}.column {max-width: 450px;}</style>'
                );
                $this->load->view('front/global/header', $data);
                $this->load->view('login', $data);
                $this->load->view('front/global/footer', $data);
            }
        }
    }

    function loggedin()
    {
        if ($this->lib->login() != "") {
            $me = $this->lib->record();
            foreach ($me as $user) {
                $data['email'] = $user->email;
                $data['id'] = $user->id_user;
                $data['nama'] = $user->nama_lengkap;
                $data['username'] = $user->username;
                $data['ip'] = $user->ip_address;
            }
            $this->load->view('loggedin', $data);
        } else {
            redirect();
        }
    }

    function logout()
    {
        if ($this->lib->login() != "") {
            $this->lib->logout();
            redirect('login-pengajuan');
        } else {
            redirect('Opd/Dashboard');
        }
    }
}
