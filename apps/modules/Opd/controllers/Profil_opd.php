<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_opd extends MX_Controller
{
    const __tableName = 'tbl_user';
    const __tableId = 'id_user';
    const __tableIdname = 'name';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_registrasi');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    public function profil()
    {
        if ($this->lib->login() != "") {
            $data = array(
                'title' => 'Profil User',
                'file' => $this->Mdl_registrasi->selek_file(),
                'sessid' => $this->session->userdata('id'),
                'nama_lengkap' => $this->session->userdata('nama_lengkap'),
                'email' => $this->session->userdata('email'),
                'nama' => $this->session->userdata('nama'),
                'password' => $this->session->userdata('password'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/profil', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login upload dokumen pengajuan.</p>');
            redirect('login');
        }
    }

    public function prosesUpdate()
    {
        $date = date('Y-m-d H:i:s');
        $where = trim($this->input->post('id_user'));
        $where2 = trim($this->input->post('nama'));

        $new_name = time() . $_FILES["userfiles"]['name'];
        $nmfile = "file_cv_" . $new_name;
        $config['upload_path'] = "./upload/cv/";
        $config['allowed_types'] = 'doc|docx';
        $config['max_size'] = '1048'; //maksimum besar file 2M
        $config['file_name'] = $nmfile;
        // $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->db->trans_begin();

        $this->upload->do_upload('file_cv');
        $file_cv = $this->upload->data();
        $data_file_cv = $file_cv;

        $path['link'] = "upload/cv/";

        if ($this->upload->do_upload("file_cv")) {
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'alamat' => $this->input->post('alamat'),
                'no_surat' => $this->input->post('no_surat'),
                'tgl_surat' => date('Y-m-d', strtotime($this->input->post('tgl_surat'))),
                'latitude' => $this->input->post('latitude'),
                'nama_file_cv' => $data_file_cv['file_name'],
                'file_cv' => $path['link'] . '' . $data_file_cv['file_name'],
                'longitude' => $this->input->post('longitude'),
                'password' => base64_encode($this->input->post("password")),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'telp' => $this->input->post('telp'),
                'updated_date' => $date
            );

            $result = $this->db->update(self::__tableName, $data, array('id_user' => $where));
            $this->do_upload_surat_images();

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $out['status'] = 'gagal';
            }
        } else {
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'alamat' => $this->input->post('alamat'),
                'no_surat' => $this->input->post('no_surat'),
                'tgl_surat' => date('Y-m-d', strtotime($this->input->post('tgl_surat'))),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'password' => base64_encode($this->input->post("password")),
                'nama_usaha' => $this->input->post('nama_usaha'),
                'alamat_usaha' => $this->input->post('alamat_usaha'),
                'telp' => $this->input->post('telp'),
                'updated_date' => $date
            );

            $data2 = array(
                'nama' => $this->input->post('nama'),
                'password' => base64_encode($this->input->post("password")),
            );

            $result = $this->db->update(self::__tableName, $data, array('id_user' => $where));
            $this->do_upload_surat_images();

            if ($this->db->trans_status() === FALSE) {
                $out['status'] = 'gagal';
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out['status'] = 'berhasil';
            } else {
                $this->db->trans_rollback();
                $out['status'] = 'gagal';
            }
        }

        echo json_encode($out);
    }
    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    private function do_upload_surat_images()
    {
        $upath = './upload/berkas_kelengkapan/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['surat']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['surat']['name'] = $files['surat']['name'][$i];
            $_FILES['surat']['type'] = $files['surat']['type'][$i];
            $_FILES['surat']['tmp_name'] = $files['surat']['tmp_name'][$i];
            $_FILES['surat']['error'] = $files['surat']['error'][$i];
            $_FILES['surat']['size'] = $files['surat']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('surat');
        }
    }
}
