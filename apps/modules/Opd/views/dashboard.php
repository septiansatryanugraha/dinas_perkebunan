<style>
    #graph {
        height: 330px;
        margin: 0 auto
    } 
    .bg_button{ background:#ff0007;}
    .bg_button2{ background:#c29e11;}
    .bg_button3{ background:#07bc02;}
    .site-stats2 {
        margin: 0;
        padding: 0; text-align:center;
        list-style: none;
    }
    .site-stats2 li {
        cursor: pointer; display:inline-block;
        margin: 0 10px 20px; text-align:center; width:105%;
        padding:20px 0; color:#fff;
        position: relative;
        box-shadow: 5px 5px 5px #aaaaaa;
    }
    .site-stats2 li i{ font-size:50px; clear:both}
    .site-stats2 li:hover { background:#ff4200;
    }
    .site-stats2 li i {
        vertical-align: baseline;
    }
    .site-stats2 li strong {
        font-weight: bold;
        font-size: 20px; width:100%; float:left;
        margin-left:0px;
    }
    .site-stats2 li small {
        margin-left:0px;
        font-size: 22px;
        padding-top: 10px;
        width:100%; float:left;
    }
    .site-stats3 {
        margin: 0;
        padding: 0; text-align:center;
        list-style: none;
    }
    .site-stats3 li {
        cursor: pointer; display:inline-block;
        margin: 0 10px 20px; text-align:center; width:105%;
        padding:20px 0; color:#fff;
        position: relative;
        box-shadow: 5px 5px 5px #aaaaaa;
    }
    .site-stats3 li i{ font-size:50px; clear:both}
    .site-stats3 li:hover { background:#0456d2;
    }
    .site-stats3 li i {
        vertical-align: baseline;
    }
    .site-stats3 li strong {
        font-weight: bold;
        font-size: 20px; width:100%; float:left;
        margin-left:0px;
    }
    .site-stats3 li small {
        margin-left:0px;
        font-size: 22px;
        padding-top: 10px;
        width:100%; float:left;
    }
    .site-stats4 {
        margin: 0;
        padding: 0; text-align:center;
        list-style: none;
    }
    .site-stats4 li {
        cursor: pointer; display:inline-block;
        margin: 0 10px 20px; text-align:center; width:105%;
        padding:20px 0; color:#fff;
        position: relative;
        box-shadow: 5px 5px 5px #aaaaaa;
    }
    .site-stats4 li i{ font-size:50px; clear:both}
    .site-stats4 li:hover { background:#ffd117;
    }
    .site-stats4 li i {
        vertical-align: baseline;
    }
    .site-stats4 li strong {
        font-weight: bold;
        font-size: 20px; width:100%; float:left;
        margin-left:0px;
    }
    .site-stats4 li small {
        margin-left:0px;
        font-size: 22px;
        padding-top: 10px;
        width:100%; float:left;
    }
    .site-stats5 {
        margin: 0;
        padding: 0; text-align:center;
        list-style: none;
    }
    .site-stats5 li {
        cursor: pointer; display:inline-block;
        margin: 0 10px 20px; text-align:center; width:105%;
        padding:20px 0; color:#fff;
        position: relative;
        box-shadow: 5px 5px 5px #aaaaaa;
    }
    .site-stats5 li i{ font-size:50px; clear:both}
    .site-stats5 li:hover { background:#05ab01;
    }
    .site-stats5 li i {
        vertical-align: baseline;
    }
    .site-stats5 li strong {
        font-weight: bold;
        font-size: 20px; width:100%; float:left;
        margin-left:0px;
    }
    .site-stats5 li small {
        margin-left:0px;
        font-size: 22px;
        padding-top: 10px;
        width:100%; float:left;
    }
</style>

<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <div class="ui grid container">
        <div class="four wide column"><ul class="site-stats2 column">
                <a href="<?php echo base_url() . 'profil-user.html' ?>"><li class="bg_button"><i class="fa fa-user"></i> <small></small> <small>Profile</small></li></a>
            </ul></div>
        <div class="four wide column"><ul class="site-stats4 column">
                <?php if ($this->session->userdata('status_berkas') == 'Approve'): ?><a href="<?php echo base_url() . 'add-sertifikasi.html' ?>"><?php endif; ?><li class="bg_button2"><i class="fa fa-file-text"></i> <small></small> <small>Sertifikasi</small></li></a></ul></div>

        <div class="four wide column"><ul class="site-stats3 column">
                <?php if ($this->session->userdata('status_berkas') == 'Approve'): ?> <a href="<?php echo base_url() . 'data-label.html' ?>"><?php endif; ?><li class="bg_lh"><i class="fa fa-bookmark"></i> <small></small> <small>Label</small></li></a></ul>
        </div>
        <div class="four wide column"><ul class="site-stats5 column">
                <?php if ($this->session->userdata('status_berkas') == 'Approve'): ?><a href="<?php echo base_url() . 'data-history.html' ?>"><?php endif; ?><li class="bg_button3"><i class="fa fa-undo"></i> <small></small> <small>Proses</small></li></a></ul></div>
    </div>
    <div class="five wide column ">
        <div class="alur"></div>
    </div>
</div>
</div>
</div>
</div>
<!-- highchart -->
<script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
<script>
    jQuery(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'graph',
                type: 'column',
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Grafik Data',
                x: -20
            },
            xAxis: {
                categories: <?php echo json_encode($tanggal); ?>
            },
            yAxis: {
                title: {
                    text: 'Jumlah Data Pengajuan'
                }
            },
            series: [{
                    name: 'Data Pengajuan ',
                    data: <?php echo json_encode($data1); ?>
                },
            ]
        });
    });
</script>
<script>
    // Animasi angka bergerak dashboard
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
</script>