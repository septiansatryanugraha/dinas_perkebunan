<div class="ui black inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="" target="_blank">&copy; Dinas Perkebunan Provinsi Jawa Timur <?php echo date("Y"); ?></a>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/jquery-3.3.1.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/semantic.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/main.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/dataTables.semanticui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.min.js"></script>
</body>
</html>