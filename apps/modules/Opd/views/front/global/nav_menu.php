<style type="text/css">
    a{
        text-decoration: none;
        color: black;
    }
    a:visited{
        color: black;
    }
    .box::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
        border-radius: 5px
    }
    .box::-webkit-scrollbar
    {
        width: 10px;
        background-color: #F5F5F5;
        border-radius: 5px
    }
    .box::-webkit-scrollbar-thumb
    {
        background-color: black;
        border: 2px solid black;
        border-radius: 5px
    }
    header{
        -moz-box-shadow: 10px 10px 23px 0px rgba(0,0,0,0.1);
        box-shadow: 10px 10px 23px 0px rgba(0,0,0,0.1);
        height: 110px;
        vertical-align: middle;
    }
    h1{
        float: left;
        padding: 10px 30px
    }
    .icons{
        display: inline;
        float: right;
        margin-top: -30px;
    }
    .notification{
        padding-top: 30px;
        position: relative;
        display: inline-block;
    }
    .number{
        height: 22px;
        width:  22px;
        background-color: #d63031;
        border-radius: 20px;
        color: white;
        text-align: center;
        position: absolute;
        top: 23px;
        left: 60px;
        padding: 3px;
        border-style: solid;
        border-width: 2px;
    }
    .number:empty {
        display: none;
    }
    .notBtn{
        transition: 0.5s;
        cursor: pointer
    }
    .fas{
        font-size: 24pt;
        color: black;
        margin-right: 30px;
        margin-left: 40px;
    }
    .box{
        width: 400px;
        height: 0px;
        border-radius: 10px;
        transition: 0.5s;
        position: absolute;
        overflow-y: scroll;
        padding: 0px;
        left: -300px;
        margin-top: 5px;
        background-color: #F4F4F4;
        -webkit-box-shadow: 10px 10px 23px 0px rgba(0,0,0,0.2);
        -moz-box-shadow: 10px 10px 23px 0px rgba(0,0,0,0.1);
        box-shadow: 10px 10px 23px 0px rgba(0,0,0,0.1);
        cursor: context-menu;
    }
    .fas:hover {
        color: #d63031;
    }
    .notBtn:hover > .box{
        height: 60vh
    }
    .content{
        padding: 20px;
        color: black;
        vertical-align: middle;
        text-align: left;
    }
    .gry{
        background-color: #F4F4F4;
    }
    .top{
        color: black;
        padding: 10px
    }
    .display{
        position: relative;
    }
    .cont{
        position: absolute;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: #F4F4F4;
    }
    .cont:empty{
        display: none;
    }
    .stick{
        text-align: center;  
        display: block;
        font-size: 50pt;
        padding-top: 70px;
        padding-left: 80px
    }
    .stick:hover{
        color: black;
    }
    .cent{
        text-align: center;
        display: block;
    }
    .sec{
        padding: 25px 10px;
        background-color: #F4F4F4;
        transition: 0.5s;
    }
    .profCont{
        padding-left: 15px;
    }
    .profile{
        -webkit-clip-path: circle(50% at 50% 50%);
        clip-path: circle(50% at 50% 50%);
        width: 75px;
        float: left;
    }
    .txt{
        vertical-align: top;
        font-size: 13px;
        line-height: 17px;
        padding: 10px 10px 0px 115px;
    }
    .sub{
        font-size: 1rem;
        color: grey;
    }
    .new{
        border-style: none none solid none;
        border-color: red;
    }
    .sec:hover{
        background-color: #BFBFBF;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0/css/all.css">
<div class="ui fixed stackable menu">
    <div href="#" class="header item">
        <img class="image" width="400px" src="<?php echo base_url(); ?>assets/publik/img/images/logo-dinas.png">
    </div>
    <div class="right menu">
        <?php if ($sessid == ""): ?>
            <div class="item">
                <div class="content">
                    <a href="<?php echo base_url(); ?>" class="button">Login</a>
                </div>
            </div>
        <?php else: ?>
            <div class="item">
                <div class = "icons">
                    <div class = "notification">
                        <a href = "#" class="hokya notBtn">
                            <div class = "number count"></div>
                            <i class="fas fa-bell"></i>
                            <div class = "box">
                                <div class = "display">
                                    <div class = "cont menu2">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        function load_unseen_notification(view = '') {
            $.ajax({
                url: "<?= base_url('dashboard-history'); ?>",
                method: "POST",
                data: {view: view},
                dataType: "json",
                success: function (data) {
                    $('.menu2').html(data.notification);
                    if (data.unseen_notification > 0) {
                        $('.count').html(data.unseen_notification);
                    }
                    console.log(data.unseen_notification);
                }
            });
        }
        load_unseen_notification();
        $(document).on('click', '.hokya', function () {
            $('.count').html('');
            load_unseen_notification('yes');
        });
        setInterval(function () {
            load_unseen_notification();
        }, 1000);
    });
</script>