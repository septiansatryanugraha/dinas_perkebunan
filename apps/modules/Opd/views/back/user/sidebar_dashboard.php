<div class="ui vertical segment">
    <div class="ui stackable grid container">
        <div class="row">
            <div class="four wide column except">
                <div class="ui vertical pointing menu">
                    <a href="<?php echo base_url(); ?>beranda.html" class="item">
                        <i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp; Dashboard
                    </a>
                    <?php if ($this->session->userdata('status_berkas') == 'Approve'): ?>
                        <a href="<?php echo base_url(); ?>data-sertifikasi.html" class="item">
                            <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp; Sertifikasi
                        </a>
                        <a href="<?php echo base_url(); ?>data-label.html" class="item">
                            <i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp;&nbsp; Label
                        </a>
                        <a href="<?php echo base_url(); ?>data-history.html" class="item">
                            <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp; History Pengajuan
                        </a>
                    <?php endif; ?>
                    <a href="<?php echo base_url(); ?>profil-user.html" class="item">
                        <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; Profil
                    </a>
                    <a href="<?php echo base_url(); ?>Opd/Login_opd/logout" class="item">
                            <i class="fa fa-lock" aria-hidden="true"></i>&nbsp;&nbsp; Keluar
                        </a>
                </div>
            </div>