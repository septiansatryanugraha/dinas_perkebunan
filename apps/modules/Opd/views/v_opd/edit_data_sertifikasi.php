<style>
    #loadingImg{
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-update" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="id_sertifikasi" value="<?php echo $pengajuan->id_sertifikasi; ?>">
        <input type="hidden" name="nama_pemohon" value="<?php echo $pengajuan->nama_pemohon; ?>">
        <input type="hidden" name="kode" value="<?php echo $pengajuan->kode; ?>">
        <input type="hidden" name="kode_iup" value="<?php echo $pengajuan->kode_iup; ?>">
        <div class="ui stacked segment">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Nama Pemohon" max-length="35" autocomplete="off" value="<?php echo $pengajuan->nama_pemohon; ?>">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <textarea name="alamat_pemohon" id="alamat_pemohon" placeholder="Alamat Pemohon" ><?php echo $pengajuan->alamat_pemohon; ?></textarea>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" name="nama_usaha" id="nama_usaha" placeholder="Nama Usaha" max-length="35" autocomplete="off" value="<?php echo $pengajuan->nama_usaha; ?>">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <textarea name="alamat_usaha" id="alamat_usaha" placeholder="Alamat Usaha"><?php echo $pengajuan->alamat_usaha; ?></textarea>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="No Telp" max-length="35" autocomplete="off" value="<?php echo $pengajuan->no_telp; ?>">
                </div>  
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Bentuk Usaha</label>
                    <select name="jenis_usaha" id="jenis_usaha" class="ui dropdown">
                        <option value="">-- Jenis usaha --</option>
                        <?php foreach ($kat_usaha as $data) { ?>
                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $pengajuan->jenis_usaha) ? "selected='selected'" : ""; ?>>
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Jenis Komoditas </label>
                    <select select type="hidden" name="jenis_komoditas" id="jenis_komoditas" class="ui dropdown">
                        <option value="">-- Jenis Komoditas --</option>
                        <?php foreach ($komoditas as $data) { ?>
                            <option value="<?php echo $data->nama; ?>"<?php echo ($data->nama == $pengajuan->jenis_komoditas) ? "selected='selected'" : ""; ?>>
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="field">
                <div class="seven wide column field">
                    <label>Jenis Sertifikasi</label>
                    <input type="text" value=" <?php echo $pengajuan->jenis_sertifikat; ?>" disabled>
                </div>  
            </div>
            <?php if ($pengajuan->catatan != '') { ?>
                <div class="field">
                    <div class="seven wide column field">
                        <label>Catatan</label>
                        <textarea disabled><?php echo $pengajuan->catatan; ?></textarea>
                    </div>  
                </div>
            <?php } ?>
            <?php foreach ($loop as $data) { ?>
                <div class="two fields">
                    <div class="five wide column field">
                        <label>Latitude</label>
                        <div class="ui left icon input">
                            <i class="map marker alternate icon"></i>
                            <input type="text" name="latitude" placeholder="Latitude" id="latitude" autocomplete="off" value=" <?php echo $data->latitude; ?>">
                        </div>
                    </div>
                    <div class="five wide column field">
                        <label>Longitude</label>
                        <div class="ui left icon input">
                            <i class="map marker alternate icon"></i>
                            <input type="text" name="longitude" placeholder="Longitude" id="longitude" autocomplete="off"  value=" <?php echo $data->longitude; ?>">
                        </div>
                    </div>
                    <div class="five wide column field">
                        <label>Keterangan</label>
                        <div class="ui left icon input">
                            <textarea name="ket" placeholder="Keterangan" id="keterangan" rows="2" autocomplete="off"><?php echo $data->ket; ?></textarea>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <font class="text-peta" color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
            <br><br>
            <?php foreach ($loop as $data) { ?>
                <div class="field">
                    <label>Gambar Lokasi</label>
                    <br>
                    <?php if ($data->gambar == "") { ?>
                        <div id="slider">
                            <img class="img-thumbnail" src="<?= base_url(); ?>/assets/tambahan/gambar/tidak-ada.png" alt="your image" />
                        </div><br>
                    <?php } else { ?>
                        <div id="slider">
                            <img class="img-thumbnail" src='../<?php echo $data->gambar; ?>' width="200px;">
                        </div>
                    <?php } ?>
                    <br>
                </div>
            <?php } ?>
            <div class="field">
                <label>Surat Permohonan Sertifikat</label>
                <a href="<?php echo base_url() . $pengajuan->surat_permohonan_sertifikat; ?>" target="blank"><b><font face="verdana" size="2" color="blue"><i class="nav-icon fa fa-file" aria-hidden="true"></i> 
                        <?php echo $pengajuan->nama_surat_pemohon; ?></font></b></a><br><br>
                <input type="file" name="surat_permohonan_sertifikat" id="surat_rekomendasi" onchange="return valid_surat1()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="field">
                <label>Surat Asal Usul Benih</label>
                <a href="<?php echo base_url() . $pengajuan->surat_benih; ?>" target="blank"><b><font face="verdana" size="2" color="blue"><i class="nav-icon fa fa-file" aria-hidden="true"></i> 
                        <?php echo $pengajuan->nama_surat_benih; ?></font></b></a><br><br>
                <input type="file" name="surat_benih" id="surat_benih" onchange="return valid_surat_benih()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Cek dan pastikan data anda benar sebelum di update dengan data terbaru  </p>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Update</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });
    $('#form-update').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('update-data-sertifikasi'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    $("#loadingImg").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('data-sertifikasi'); ?>'", 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>
<script type="text/javascript">
    //untuk live gambar ajax slider 
    function CheckGbr() {
        var fileInput = document.getElementById('gambar');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            swal("Peringatan", "Maaf masukan gambar dengan format .jpeg/.jpg/", "warning");
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '" width="200" height="auto"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
        // validasi ukuran size file
        var ukuran = document.getElementById("gambar");
        if (ukuran.files[0].size > 1007200) {
            swal("Peringatan", "File Maksimal 1 MB", "warning");
            ukuran.value = '';
//            setTimeout(location.reload.bind(location), 500);
            return false;
        }
    }

    function valid_surat1() {
        var fileInput = document.getElementById("surat_rekomendasi").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_rekomendasi").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_rekomendasi");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }

    function valid_surat_benih() {
        var fileInput = document.getElementById("surat_benih").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_benih").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_benih");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }
</script>