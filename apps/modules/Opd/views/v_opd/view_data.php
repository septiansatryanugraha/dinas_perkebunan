<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="id_rekomendasi" value="<?php echo $pengajuan->id_rekomendasi ?>">
        <div class="ui stacked segment">
            <div class="field">
                <label>File Ijin Usaha Perkebunan ( IUP )</label>
                <input type="file"  name="surat_iup" id="surat_iup"  onchange="return valid_surat4()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div><br>
            </div>
            <?php if ($pengajuan->surat_iup != NULL) { ?>
                <a href="<?php echo base_url() . $pengajuan->surat_iup; ?>"><b><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo $pengajuan->nama_surat_iup; ?></font></b></a>
            <?php } ?>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Catatan !! </div>
                    <li> Silahkan Upload File / Surat IUP Sebagai Syarat Pengajuan Sertifikasi.</li>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";

        if (error == 0) {
            var surat_iup = $("#surat_iup").val();
            var surat_iup = surat_iup.trim();
            if (surat_iup.length == 0) {
                error++;
                message = "File IUP wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('update-data-rekomendasi'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    upload_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>