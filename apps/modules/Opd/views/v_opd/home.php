<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <div class="ui info message">
        <div class="content">
            <div class="header">Penting !! </div>
            <p><li>Silahkan pilih button <b>Add Rekomendasi Baru</b> ( jika belum mempunyai surat rekomendasi ).</li>
            <!-- <li>Untuk yang sudah memiliki Surat Rekomendasi silahkan pilih <b>Add Rekomendasi Baru</b> kemudian upload bukti Surat Rekomendasi.</li>  --></p>
        </div>
    </div>
    <div class="ui form">
        <div class="fields">
            <div class="field">
                <a href="<?php echo base_url('add-rekomendasi'); ?>"><button class="ui primary button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Rekomendasi</button></a><br><br>
            </div>
            <!--  <div class="field">
              <a href="<?php echo base_url('add-rekomendasi-lama'); ?>"><button class="ui primary button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Rekomendasi</button></a><br><br>
             </div> -->
        </div>
    </div>
    <table id="example" class="ui celled table" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Rekomendasi</th>
                <th>Nama Pemohon</th>
                <th>Kode IUP</th>
                <th>File Rekomendasi</th>
                <th>Status</th>
                <th>Action</th>
            </tr>	
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //untuk load data table ajax	
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        //datatables
        table = $('#example').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
                sProcessing: "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>"
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('ajax-rekomendasi') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });

    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-menu", function () {
        var id_menu = $(this).attr("data-id");
        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: true,
            html: true
        }, function () {
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('Setting/Menu/hapus'); ?>",
                data: "id_menu=" + id_menu,
                success: function (data) {
                    $("tr[data-id='" + id_menu + "']").fadeOut("fast", function () {
                        $(this).remove();
                    });
                    hapus_berhasil();
                    reload_table();
                }
            });
        });
    });
</script>