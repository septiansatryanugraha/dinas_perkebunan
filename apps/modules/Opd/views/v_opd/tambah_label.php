<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    #slider {
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <div class="ui stacked segment">
            <div class="field ">
                <div class="nine wide column field">
                    <label>Kode Pengajuan Sertifikasi</label>
                    <input type="hidden" id="kode_sertifikasi" name="kode_sertifikasi">
                    <div class="ui fluid search selection dropdown">
                        <input type="hidden" id="id_pengajuan">
                        <i class="dropdown icon"></i>
                        <div class="default text">Pilih Kode Pengajuan</div>
                        <div class="menu">
                            <?php foreach ($selekKode as $data) { ?>
                                <div class="item" data-value="<?php echo $data->id_sertifikasi ?>">
                                    <?php echo $data->kode; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div id="loadingImg2">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                </div>
            </div>
            <input type="hidden" name="kode_rekomendasi" id="kode_rekomendasi" placeholder="Auto" max-length="35" autocomplete="off">
            <div class="field">
                <div class="nine wide column field">
                    <label>No Sertifikat</label>
                    <input type="text" name="no_sertifikat" id="no_sertifikat" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <input type="text" name="alamat_pemohon" id="alamat_pemohon" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" name="nama_usaha" id="nama_usaha" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <input type="text" name="alamat_usaha" id="alamat_usaha" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Email</label>
                    <input type="text" name="email" id="email" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Bentuk Usaha</label>
                    <input type="text" name="jenis_usaha" id="jenis_usaha" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Jenis Komoditas</label>
                    <input type="text" name="jenis_komoditas" id="jenis_komoditas" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="three wide column field">
                    <label>Jumlah label</label>
                    <input type="number" name="jumlah_label" id="jumlah_label" placeholder="Jumlah Label" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Pastikan Permohonan Rekomendasi sudah di approve oleh Admin, Cek ulang data anda sebelum mengajukan Permohonan Sertifikasi </p>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        if (error == 0) {
            var id_pengajuan = $("#id_pengajuan").val();
            var id_pengajuan = id_pengajuan.trim();
            if (id_pengajuan.length == 0) {
                error++;
                message = "Kode Pengajuan Sertifikasi wajib di isi.";
            }
        }
        if (error == 0) {
            var jumlah_label = $("#jumlah_label").val();
            var jumlah_label = jumlah_label.trim();
            if (jumlah_label.length == 0) {
                error++;
                message = "Jumlah label wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('save-label'); ?>',
                type: "post",
                data: new FormData(this),
                // data: data, 
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('data-label.html'); ?>'", 500);
                } else {
                    $("#loadingImg").hide();
                    swal("Peringatan", result.status, "warning");
                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

</script>
<script type="text/javascript">
    $('#no_sertifikat').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#nama_pemohon').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#alamat_pemohon').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#nama_usaha').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#alamat_usaha').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#no_telp').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#email').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#jenis_usaha').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#jenis_komoditas').bind('keypress', function (e) {
        e.preventDefault();
    })
    $(function () {
        $("#loadingImg2").hide();
        $("#loadingImg3").hide();
        $("#tampil").hide();
        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('get-data-label') ?>",
            cache: true,
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'no_sertifikat', id_sertifikasi: value},
                    success: function (respond) {
                        $("#no_sertifikat").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('no_sertifikat');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#no_sertifikat').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'nama_pemohon', id_sertifikasi: value},
                    success: function (respond) {
                        $("#nama_pemohon").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('nama_pemohon');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#nama_pemohon').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'alamat_pemohon', id_sertifikasi: value},
                    success: function (respond) {
                        $("#alamat_pemohon").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('alamat_pemohon');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#alamat_pemohon').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'nama_usaha', id_sertifikasi: value},
                    success: function (respond) {
                        $("#nama_usaha").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('nama_usaha');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#nama_usaha').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'alamat_usaha', id_sertifikasi: value},
                    success: function (respond) {
                        $("#alamat_usaha").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('alamat_usaha');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#alamat_usaha').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'no_telp', id_sertifikasi: value},
                    success: function (respond) {
                        $("#no_telp").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('no_telp');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#no_telp').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'email', id_sertifikasi: value},
                    success: function (respond) {
                        $("#email").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('email');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#email').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'jenis_usaha', id_sertifikasi: value},
                    success: function (respond) {
                        $("#jenis_usaha").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('jenis_usaha');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#jenis_usaha').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'jenis_komoditas', id_sertifikasi: value},
                    success: function (respond) {
                        $("#jenis_komoditas").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('jenis_komoditas');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#jenis_komoditas').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#id_pengajuan").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'kode_sertifikasi', id_sertifikasi: value},
                    success: function (respond) {
                        $("#kode_sertifikasi").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('kode_sertifikasi');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#kode_sertifikasi').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
    })
</script>