<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    #slider {
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="id_koordinat[]">
        <div class="ui stacked segment">
            <div class="field ">
                <div class="nine wide column field">
                    <label>Kode IUP</label>
                    <input type="hidden" id="kd_iup" name="kode_iup">
                    <div class="ui fluid search selection dropdown">
                        <input type="hidden" id="kode_iup">
                        <i class="dropdown icon"></i>
                        <div class="default text">Pilih IUP</div>
                        <div class="menu">
                            <?php foreach ($selekKode as $data) { ?>
                                <div class="item" data-value="<?php echo $data->id_user; ?>">
                                    <?php echo $data->kode_iup; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div id="loadingImg2">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                </div>
            </div>
            <input type="hidden" name="kode_rekomendasi" id="kode_rekomendasi" placeholder="Auto" max-length="35" autocomplete="off">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <input type="text" name="alamat_pemohon" id="alamat_pemohon" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" name="nama_usaha" id="nama_usaha" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <input type="text" name="alamat_usaha" id="alamat_usaha" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Email</label>
                    <input type="text" name="email" id="email" placeholder="Auto" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Bentuk Usaha</label>
                    <select name="jenis_usaha" id="jenis_usaha" class="ui dropdown">
                        <option value="">-- Jenis usaha --</option>
                        <?php foreach ($kat_usaha as $data) { ?>
                            <option value="<?php echo $data->nama; ?>">
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Jenis Komoditas </label>
                    <select select type="hidden" name="jenis_komoditas" id="jenis_komoditas" class="ui dropdown">
                        <option value="">-- Jenis Komoditas --</option>
                        <?php foreach ($komoditas as $data) { ?>
                            <option value="<?php echo $data->nama; ?>">
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="two fields">
                <div class="five wide column field">
                    <label>Latitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="latitude[]" placeholder="Latitude" id="latitude" autocomplete="off">
                    </div>
                </div>
                <div class="five wide column field">
                    <label>Longitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="longitude[]" placeholder="Longitude" id="longitude" autocomplete="off">
                    </div>
                </div>
                <div class="five wide column field">
                    <label>Keterangan</label>
                    <div class="ui left icon input">
                        <textarea name="ket[]" placeholder="Keterangan" id="keterangan" rows="2" autocomplete="off"></textarea>
                    </div>
                </div>
            </div>
            <font class="text-peta" color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
            <br><br>
            <div class="field">
                <label>Gambar Lokasi</label>
                <br>
                <div id="slider">
                    <img class="img-thumbnail" src="<?= base_url(); ?>/assets/tambahan/gambar/tidak-ada.png" alt="your image" />
                </div><br>
                <input type="file" name="gambar" id="gambar" onchange="return CheckGbr()"/>
                <div class="ui red left pointing label">Max: 900 kb. Ekstensi .jpg</div>
            </div>
            <div class="field">
                <label>Surat Permohonan Sertifikat</label>
                <input type="file" name="surat_permohonan_sertifikat" id="surat_rekomendasi" onchange="return valid_surat1()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="field">
                <label>Surat Asal Usul Benih</label>
                <input type="file" name="surat_benih" id="surat_benih" onchange="return valid_surat_benih()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Pastikan Permohonan Rekomendasi sudah di approve oleh Admin, Cek ulang data anda sebelum mengajukan Permohonan Sertifikasi </p>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";
        var data = $(this).serialize();

        if (error == 0) {
            var kode_iup = $("#kode_iup").val();
            var kode_iup = kode_iup.trim();
            if (kode_iup.length == 0) {
                error++;
                message = "Kode IUP wajib di isi.";
            }
        }
        if (error == 0) {
            var nama_pemohon = $("#nama_pemohon").val();
            var nama_pemohon = nama_pemohon.trim();
            if (nama_pemohon.length == 0) {
                error++;
                message = "Nama Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_pemohon = $("#alamat_pemohon").val();
            var alamat_pemohon = alamat_pemohon.trim();
            if (alamat_pemohon.length == 0) {
                error++;
                message = "Alamat Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var nama_usaha = $("#nama_usaha").val();
            var nama_usaha = nama_usaha.trim();
            if (nama_usaha.length == 0) {
                error++;
                message = " Nama Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_usaha = $("#alamat_usaha").val();
            var alamat_usaha = alamat_usaha.trim();
            if (alamat_usaha.length == 0) {
                error++;
                message = " Alamat Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var no_telp = $("#no_telp").val();
            var no_telp = no_telp.trim();
            if (no_telp.length == 0) {
                error++;
                message = " No telp wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = " Email harus diisi !!";
            } else if (!cekemail(email)) {
                error++;
                message = "Format Email tidak sesuai (example@gmail.com). ";
            }
        }
        if (error == 0) {
            var jenis_usaha = $("#jenis_usaha").val();
            var jenis_usaha = jenis_usaha.trim();
            if (jenis_usaha.length == 0) {
                error++;
                message = "Jenis Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var jenis_komoditas = $("#jenis_komoditas").val();
            var jenis_komoditas = jenis_komoditas.trim();
            if (jenis_komoditas.length == 0) {
                error++;
                message = "Jenis Komoditas wajib di isi.";
            }
        }
        if (error == 0) {
            var longitude = $("#longitude").val();
            var longitude = longitude.trim();
            if (longitude.length == 0) {
                error++;
                message = "Longitude wajib di isi.";
            }
        }
        if (error == 0) {
            var latitude = $("#latitude").val();
            var latitude = latitude.trim();
            if (latitude.length == 0) {
                error++;
                message = "Latitude wajib di isi.";
            }
        }
        if (error == 0) {
            var keterangan = $("#keterangan").val();
            var keterangan = keterangan.trim();
            if (latitude.length == 0) {
                error++;
                message = "Latitude wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_rekomendasi = $("#surat_rekomendasi").val();
            var surat_rekomendasi = surat_rekomendasi.trim();
            if (surat_rekomendasi.length == 0) {
                error++;
                message = "Surat Permohonan Sertifikat wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_benih = $("#surat_benih").val();
            var surat_benih = surat_benih.trim();
            if (surat_benih.length == 0) {
                error++;
                message = "Surat Asal Usul Benih wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('save-sertifikasi'); ?>',
                type: "post",
                data: new FormData(this),
                // data: data, 
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('data-sertifikasi.html'); ?>'", 500);
                } else {
                    $("#loadingImg").hide();
                    swal("Peringatan", result.status, "warning");
                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>
<script type="text/javascript">
    //untuk live gambar ajax slider 
    function CheckGbr() {
        var fileInput = document.getElementById('gambar');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            swal("Peringatan", "Maaf masukan gambar dengan format .jpeg/.jpg/", "warning");
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '" width="150" height="auto"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
        // validasi ukuran size file
        var ukuran = document.getElementById("gambar");
        if (ukuran.files[0].size > 1007200) {
            swal("Peringatan", "File Maksimal 1 MB", "warning");
            ukuran.value = '';
//            setTimeout(location.reload.bind(location), 500);
            return false;
        }
    }

    function valid_surat1() {
        var fileInput = document.getElementById("surat_rekomendasi").value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_rekomendasi").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_rekomendasi");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }

    function valid_surat_benih() {
        var fileInput = document.getElementById("surat_benih").value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_benih").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_benih");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }
</script>
<script type="text/javascript">
    $('#nama_pemohon').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#alamat_pemohon').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#nama_usaha').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#alamat_usaha').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#no_telp').bind('keypress', function (e) {
        e.preventDefault();
    })
    $('#email').bind('keypress', function (e) {
        e.preventDefault();
    })
    $(function () {
        $("#loadingImg2").hide();
        $("#loadingImg3").hide();
        $("#tampil").hide();
        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('get-data-sertifikasi') ?>",
            cache: true,
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'nama_pemohon', id_user: value},
                    success: function (respond) {
                        $("#nama_pemohon").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('nama_pemohon');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#nama_pemohon').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'alamat_pemohon', id_user: value},
                    success: function (respond) {
                        $("#alamat_pemohon").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('alamat_pemohon');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#alamat_pemohon').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'nama_usaha', id_user: value},
                    success: function (respond) {
                        $("#nama_usaha").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('nama_usaha');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#nama_usaha').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'alamat_usaha', id_user: value},
                    success: function (respond) {
                        $("#alamat_usaha").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('alamat_usaha');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#alamat_usaha').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'no_telp', id_user: value},
                    success: function (respond) {
                        $("#no_telp").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('no_telp');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#no_telp').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'email', id_user: value},
                    success: function (respond) {
                        $("#email").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
            var input = document.getElementById('email');
            input.onkeydown = function (event) {
                if (event.which == 8 || event.which == 46) {
                    event.preventDefault();
                }
            }
            $('#email').bind('keypress', function (e) {
                e.preventDefault();
            })
        });
        $("#kode_iup").change(function () {
            var value = $(this).val();
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg2").fadeIn();
                    },
                    data: {modul: 'kd_iup', id_user: value},
                    success: function (respond) {
                        $("#kd_iup").val(respond);
                        $("#loadingImg2").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });
    })
</script>   