<style>
    #loadingImg{
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
    .text-peta {
        margin-top: 23px;
    }
</style>
<?php
$dec_pass = base64_decode($user->password);

?>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="nama" value="<?= $nama; ?>">
        <input type="hidden" value="<?= isset($user->folder) ? $user->folder : date('YmdHis') ?>" name="folder">
        <div class="ui stacked segment">
            <div class="field">
                <label>Nama Lengkap</label>
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="nama_lengkap" placeholder="Nama Lengkap" id="nama_lengkap" max-length="35" autocomplete="off" value="<?php echo $user->nama_lengkap; ?>">
                </div>
            </div>
            <div class="field">
                <label>Email Pendaftar</label>
                <div class="ui left icon input">
                    <i class="envelope icon"></i>
                    <input type="text" name="email" placeholder="Masukan Email" autocomplete="off" value="<?php echo $user->email ?>" disabled>
                </div>
            </div>
            <div class="field">
                <label>Alamat Pendaftar</label>
                <div class="ui left icon input">
                    <i class="home icon"></i>
                    <input type="text" name="alamat" placeholder="Alamat Pendaftar" id="alamat" autocomplete="off" value="<?php echo $user->alamat ?>">
                </div>
            </div>
            <div class="two fields">
                <div class="seven wide column field">
                    <label>Latitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="latitude" placeholder="Latitude" id="latitude" autocomplete="off" value="<?php echo $user->latitude ?>">
                    </div>
                </div>
                <div class="seven wide column field">
                    <label>Longitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="longitude" placeholder="Latitude" id="longitude" autocomplete="off" value="<?php echo $user->longitude ?>">
                    </div>
                </div>
                <font class="text-peta" color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Password</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="password" name="password" id="password-field" placeholder="Password"  autocomplete="off" value="<?= $dec_pass; ?>">
                    </div>
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
                </div>
                <div class="field">
                    <label>Konfirmasi Password</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="password" name="password" id="password-field2"  placeholder="Konfirmasi Password"  autocomplete="off" value="<?= $dec_pass; ?>">
                    </div>
                    <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Nama Usaha</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="nama_usaha" id="nama_usaha" autocomplete="off" value="<?php echo $user->nama_usaha ?>">
                    </div>
                </div>
                <div class="field">
                    <label>Telp</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="telp" id="telp"  autocomplete="off" value="<?php echo $user->telp ?>">
                    </div>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Alamat Perusahaan</label>
                    <textarea name="alamat_usaha" id="alamat_usaha" autocomplete="off"><?php echo $user->alamat_usaha ?></textarea>
                </div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Info !! </div>
                    <p>Silahkan lengkapi data di bawah ini untuk bisa melakukan pengajuan </p>
                </div>
            </div>
            <?php if ($user->catatan != '') { ?>
                <div class="field">
                    <div class="nine wide column field">
                        <label>Catatan User</label>
                        <textarea disabled><?php echo $user->catatan; ?></textarea>
                    </div>  
                </div>
            <?php } ?>
            <div class="three fields">
                <div class="field">
                    <label>Kode IUP</label>
                    <input type="text" name="kode_iup" id="kode_iup" autocomplete="off" value="<?php echo $user->kode_iup ?>">
                </div>
                <div class="field">
                    <label>No Surat</label>
                    <input type="text" name="no_surat" id="no_surat"  autocomplete="off" value="<?php echo $user->no_surat ?>">
                </div>
                <div class="field">           
                    <label>Tanggal Surat</label>
                    <div class="ui calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <?php if ($user->tgl_surat == "") { ?>
                                <input type="text" id="tanggal" name="tgl_surat" value="<?php echo date('d-m-Y'); ?>">
                            <?php } else { ?>
                                <input type="text" id="tanggal" name="tgl_surat" value="<?php echo date('d-m-Y', strtotime($user->tgl_surat)); ?>">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>File Surat Permohonan Sertifikat</label>
                <input type="file"  name="surat[]" id="surat_permohonan"  onchange="return valid1()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div><br>
            </div>
            <div class="field">
                <label>Surat Asal Usul Benih</label>
                <input type="file"  name="surat[]" id="asal_usul"  onchange="return valid2()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div><br>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Update</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        if (error == 0) {
            var nama_lengkap = $("#nama_lengkap").val();
            var nama_lengkap = nama_lengkap.trim();
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama Lengkap Wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat = $("#alamat").val();
            var alamat = alamat.trim();
            if (alamat.length == 0) {
                error++;
                message = "Alamat User Wajib di isi.";
            }
        }
        if (error == 0) {
            var latitude = $("#latitude").val();
            var latitude = latitude.trim();
            if (latitude.length == 0) {
                error++;
                message = "Titik kordinat Latitude Wajib di isi.";
            }
        }
        if (error == 0) {
            var longitude = $("#longitude").val();
            var longitude = longitude.trim();
            if (longitude.length == 0) {
                error++;
                message = "Titik kordinat Longitude Wajib di isi.";
            }
        }
        if (error == 0) {
            var password_field = $("#password-field").val();
            var password_field = password_field.trim();
            if (password_field.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }
        }
        if (error == 0) {
            var password_field2 = $("#password-field2").val();
            var password_field2 = password_field2.trim();
            if (password_field2.length == 0) {
                error++;
                message = "Konfirmasi password wajib di isi.";
            }
        }
        if (error == 0) {
            var nama_usaha = $("#nama_usaha").val();
            var nama_usaha = nama_usaha.trim();
            if (nama_usaha.length == 0) {
                error++;
                message = "Nama usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            var telp = $("#telp").val();
            var telp = telp.trim();
            if (telp.length == 0) {
                error++;
                message = "Telp usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_usaha = $("#alamat_usaha").val();
            var alamat_usaha = alamat_usaha.trim();
            if (alamat_usaha.length == 0) {
                error++;
                message = "Alamat usaha Wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('update-profil-user'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    upload_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>
<script type="text/javascript">
    function valid1() {
        var fileInput = document.getElementById("surat_permohonan").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_rekomendasi").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_permohonan");
            if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
            {
                swal("Peringatan", "File harus maksimal 1MB", "warning");
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    function valid2() {
        var fileInput = document.getElementById("asal_usul").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_rekomendasi").value = '';
                return false;
            }
            var ukuran = document.getElementById("asal_usul");
            if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
            {
                swal("Peringatan", "File harus maksimal 1MB", "warning");
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }
</script>
<script type="text/javascript">
    $(function ()
    {
        $("#tanggal").datepicker({
            dateFormat: 'dd-mm-yy'
        })
    });
</script>
<script>
// untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>   