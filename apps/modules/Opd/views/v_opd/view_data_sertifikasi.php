<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="id_sertifikasi" value="<?php echo $pengajuan->id_sertifikasi; ?>">
        <input type="hidden" name="nama_pemohon" value="<?php echo $pengajuan->nama_pemohon; ?>">
        <input type="hidden" name="kode" value="<?php echo $pengajuan->kode; ?>">
        <div class="ui stacked segment">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->nama_pemohon; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->alamat_pemohon; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->nama_usaha; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->alamat_usaha; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->no_telp; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Email</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->email; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Jenis Usaha</label>
                    <div class="ui info message">
                        <div class="content">
                            <?php echo $pengajuan->jenis_usaha; ?>
                        </div>
                    </div>
                </div>  
            </div>
            <?php if ($pengajuan->nama_sts != NULL) { ?>
                <div class="eleven wide column field">
                    <div class="ui info message">
                        <div class="content">
                            <div class="header">Upload Biaya Retribusi</div><br>

                            <div class="field">
                                <label>Bukti STS</label>
                                <a href="<?php echo base_url() . $pengajuan->file_sts; ?>"><b><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo $pengajuan->nama_sts; ?></font></b></a><br>
                            </div>

                            <div class="field">
                                <label>Upload Bukti Pembayaran</label>
                                <input type="file" name="bukti_pembayaran" id="bukti_pembayaran"  onchange="return valid_bukti()"/>
                                <div class="ui red left pointing label">Max: 1MB. Ekstensi .jpg</div><br>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Upload</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";
        var data = $(this).serialize();

        if (error == 0) {
            var bukti_pembayaran = $("#bukti_pembayaran").val();
            var bukti_pembayaran = bukti_pembayaran.trim();
            if (bukti_pembayaran.length == 0) {
                error++;
                message = "Bukti Pembayaran wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('update2-data-sertifikasi'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    upload_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>
<script type="text/javascript">
    function valid_bukti() {
        var fileInput = document.getElementById("bukti_pembayaran").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.jpg|\.jpeg|\.png|\.JPEG|\.JPG|\.PNG)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .jpg", "warning");
                document.getElementById("bukti_pembayaran").value = '';
                return false;
            }
            var ukuran = document.getElementById("bukti_pembayaran");
            if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
            {
                swal("Peringatan", "File harus maksimal 1MB", "warning");
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }
</script>