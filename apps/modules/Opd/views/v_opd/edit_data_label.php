<style>
    #loadingImg{
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-update" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="id_pengajuan" value="<?php echo $pengajuan->id_pengajuan; ?>">
        <input type="hidden" name="kode" value="<?php echo $pengajuan->kode_sertifikasi; ?>">
        <input type="hidden" name="nama_pemohon" value="<?php echo $pengajuan->nama_pemohon; ?>">
        <input type="hidden" name="jenis_komoditas" value="<?php echo $pengajuan->jenis_komoditas; ?>">
        <div class="ui stacked segment">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Nama Pemohon" max-length="35" autocomplete="off" value="<?php echo $pengajuan->nama_pemohon; ?>" disabled>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <textarea name="alamat_pemohon" id="alamat_pemohon" placeholder="Alamat Pemohon"  disabled><?php echo $pengajuan->alamat_pemohon; ?></textarea>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" max-length="35" autocomplete="off" value="<?php echo $pengajuan->nama_usaha; ?>" disabled>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <textarea name="alamat_usaha" id="alamat_usaha" placeholder="Alamat Usaha" disabled><?php echo $pengajuan->alamat_usaha; ?></textarea>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="No Telp" max-length="35" autocomplete="off" value="<?php echo $pengajuan->no_telp; ?>" disabled>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Jenis Usaha</label>
                    <input type="text" max-length="35" autocomplete="off" value="<?php echo $pengajuan->jenis_usaha; ?>" disabled>
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Jenis Komoditas</label>
                    <input type="text" max-length="35" autocomplete="off" value="<?php echo $pengajuan->jenis_komoditas; ?>" disabled>
                </div>  
            </div>
            <?php if ($pengajuan->catatan != '') { ?>
                <div class="field">
                    <div class="nine wide column field">
                        <label>Catatan</label>
                        <textarea disabled><?php echo $pengajuan->catatan; ?></textarea>
                    </div>  
                </div>
            <?php } ?>
            <div class="field">
                <div class="three wide column field">
                    <label>Jumlah Pelabelan</label>
                    <input type="number" name="jumlah_label" max-length="35" autocomplete="off" value="<?php echo $pengajuan->jumlah_label; ?>">
                </div>  
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Cek dan pastikan data anda benar sebelum di update dengan data terbaru  </p>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Update</button>
        </div>
    </form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });
    $('#form-update').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('update-data-label'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    $("#loadingImg").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url('data-label'); ?>'", 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>
<script type="text/javascript">
    //untuk live gambar ajax slider 
    function CheckGbr() {
        var fileInput = document.getElementById('gambar');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            swal("Peringatan", "Maaf masukan gambar dengan format .jpeg/.jpg/", "warning");
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '" width="200" height="auto"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
        // validasi ukuran size file
        var ukuran = document.getElementById("gambar");
        if (ukuran.files[0].size > 1007200) {
            swal("Peringatan", "File Maksimal 1 MB", "warning");
            ukuran.value = '';
//            setTimeout(location.reload.bind(location), 500);
            return false;
        }
    }
    function valid_surat1() {
        var fileInput = document.getElementById("surat_rekomendasi").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_rekomendasi").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_rekomendasi");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }

    function valid_surat_benih() {
        var fileInput = document.getElementById("surat_benih").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                swal("Peringatan", "File harus format .pdf", "warning");
                document.getElementById("surat_benih").value = '';
                return false;
            }
            var ukuran = document.getElementById("surat_benih");
            if (ukuran.files[0].size > 1007200) {  // validasi ukuran size file
                {
                    swal("Peringatan", "File harus maksimal 1MB", "warning");
                    ukuran.value = '';
                    return false;
                }
                return true;
            }
        }
    }
</script>