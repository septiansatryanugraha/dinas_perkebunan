<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
        <div class="ui stacked segment">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Nama Pemohon" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <input type="text" name="alamat_pemohon" id="alamat_pemohon" placeholder="Alamat Pemohon" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" name="nama_usaha" id="nama_usaha" placeholder="Nama Usaha" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <input type="text" name="alamat_usaha" id="alamat_usaha" placeholder="Alamat Usaha" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="No Telp" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Email</label>
                    <input type="text" name="email" id="email" placeholder="Email" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Bentuk Usaha</label>
                    <select name="jenis_usaha" id="jenis_usaha" class="ui dropdown">
                        <option value="">-- Jenis Usaha --</option>
                        <?php foreach ($kat_usaha as $data) { ?>
                            <option value="<?php echo $data->nama; ?>">
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Kelengkapan Data</div>
                    <p>Kelengkapan yang harus di lampirkan</p>
                </div><br>
                <div class="field">
                    <label>Profil Usaha </label>
                    <input type="file" name="others[]" id="files" multiple onchange="return valid_file()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>(NIB) No Induk Berusaha </label>
                    <input type="file" name="others[]" id="files3" multiple onchange="return valid_file3()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>Surat Kuasa Pimpinan</label>
                    <input type="file" name="others[]" id="files2" multiple onchange="return valid_file2()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>Fotocopy KTP Pimpinan</label>
                    <input type="file" name="others[]" id="files4" multiple onchange="return valid_file4()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>Fotocopy NPWP</label>
                    <input type="file" name="others[]" id="files5" multiple onchange="return valid_file5()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>fotocopy Surat SIUP</label>
                    <input type="file" name="others[]" id="files6" multiple onchange="return valid_file6()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>Fotocopy Bukti Kepemilikan</label>
                    <input type="file" name="others[]" id="files7" multiple onchange="return valid_file7()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
                <div class="field">
                    <label>Surat Perjajnjian Kerjasama</label>
                    <input type="file" name="others[]" id="files8" multiple onchange="return valid_file8()"/>
                    <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
                </div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Download file permohonan di bawah ini dan upload di web sebagai syarat untuk pembuatan Surat Rekomendasi.</p>
                </div>
            </div>
            <?php foreach ($file as $data) { ?>
                <a href="<?php echo base_url() . $data->path; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $data->nama_file; ?></font></b></a><br>
            <?php } ?><br>
            <div class="field">
                <label>Surat Permohonan Rekomendasi</label>
                <input type="file" name="surat_rekomendasi" id="surat_rekomendasi" onchange="return valid_surat1()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="field">
                <label>Surat Perjanjian Kerjasama</label>
                <input type="file" name="surat_kerjasama" id="surat_kerjasama" onchange="return valid_surat2()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="field">
                <label>Surat Pernyataan Mematuhi Peraturan</label>
                <input type="file" name="surat_patuh" id="surat_patuh" onchange="return valid_surat3()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Lengkapi form data, pastikan file upload sesuai dengan ekstensi format yang di minta.</p>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        if (error == 0) {
            var nama_pemohon = $("#nama_pemohon").val();
            var nama_pemohon = nama_pemohon.trim();
            if (nama_pemohon.length == 0) {
                error++;
                message = "Nama Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_pemohon = $("#alamat_pemohon").val();
            var alamat_pemohon = alamat_pemohon.trim();
            if (alamat_pemohon.length == 0) {
                error++;
                message = "Alamat Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var nama_usaha = $("#nama_usaha").val();
            var nama_usaha = nama_usaha.trim();
            if (nama_usaha.length == 0) {
                error++;
                message = " Nama Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_usaha = $("#alamat_usaha").val();
            var alamat_usaha = alamat_usaha.trim();
            if (alamat_usaha.length == 0) {
                error++;
                message = " Alamat Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var no_telp = $("#no_telp").val();
            var no_telp = no_telp.trim();
            if (no_telp.length == 0) {
                error++;
                message = " No telp wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = " Email harus diisi !!";
            } else if (!cekemail(email)) {
                error++;
                message = "Format Email tidak sesuai (admin@gmail.com). ";
            }
        }
        if (error == 0) {
            var jenis_usaha = $("#jenis_usaha").val();
            var jenis_usaha = jenis_usaha.trim();
            if (jenis_usaha.length == 0) {
                error++;
                message = "Jenis Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var files = $("#files").val();
            var files = files.trim();
            if (files.length == 0) {
                error++;
                message = "Profil Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var files3 = $("#files3").val();
            var files3 = files3.trim();
            if (files3.length == 0) {
                error++;
                message = "No Induk Berusaha wajib di isi.";
            }
        }
        if (error == 0) {
            var files2 = $("#files2").val();
            var files2 = files2.trim();
            if (files2.length == 0) {
                error++;
                message = "Surat Kuasa Pimpinan wajib di isi.";
            }
        }
        if (error == 0) {
            var files4 = $("#files4").val();
            var files4 = files4.trim();
            if (files4.length == 0) {
                error++;
                message = "Fotocopy KTP Pimpinan wajib di isi.";
            }
        }
        if (error == 0) {
            var files5 = $("#files5").val();
            var files5 = files5.trim();
            if (files5.length == 0) {
                error++;
                message = "Fotocopy NPWP wajib di isi.";
            }
        }
        if (error == 0) {
            var files6 = $("#files6").val();
            var files6 = files6.trim();
            if (files6.length == 0) {
                error++;
                message = "fotocopy Surat SIUP wajib di isi.";
            }
        }
        if (error == 0) {
            var files7 = $("#files7").val();
            var files7 = files7.trim();
            if (files6.length == 0) {
                error++;
                message = "Fotocopy Bukti Kepemilikan wajib di isi.";
            }
        }
        if (error == 0) {
            var files8 = $("#files8").val();
            var files8 = files8.trim();
            if (files8.length == 0) {
                error++;
                message = "Surat Perjajnjian Kerjasama wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_rekomendasi = $("#surat_rekomendasi").val();
            var surat_rekomendasi = surat_rekomendasi.trim();
            if (surat_rekomendasi.length == 0) {
                error++;
                message = "Surat Rekomendasi wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_kerjasama = $("#surat_kerjasama").val();
            var surat_kerjasama = surat_kerjasama.trim();
            if (surat_kerjasama.length == 0) {
                error++;
                message = "Surat Kerjasama wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_patuh = $("#surat_patuh").val();
            var surat_patuh = surat_patuh.trim();
            if (surat_patuh.length == 0) {
                error++;
                message = "Surat Patuh wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('save-rekomendasi'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    upload_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>