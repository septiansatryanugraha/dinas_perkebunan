<style>
    #loadingImg {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <div class="ui stacked segment">
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Pemohon</label>
                    <input type="text" name="nama_pemohon" id="nama_pemohon" placeholder="Nama Pemohon" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Pemohon</label>
                    <input type="text" name="alamat_pemohon" id="alamat_pemohon" placeholder="Alamat Pemohon" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Usaha</label>
                    <input type="text" name="nama_usaha" id="nama_usaha" placeholder="Nama Usaha" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Alamat Usaha</label>
                    <input type="text" name="alamat_usaha" id="alamat_usaha" placeholder="Alamat Usaha" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>No Telp / HP</label>
                    <input type="text" name="no_telp" id="no_telp" placeholder="No Telp" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field">
                <div class="nine wide column field">
                    <label>Email</label>
                    <input type="text" name="email" id="email" placeholder="Email" max-length="35" autocomplete="off">
                </div>  
            </div>
            <div class="field ">
                <div class="eight wide column field">
                    <label>Bentuk Usaha</label>
                    <select name="jenis_usaha" id="jenis_usaha" class="ui dropdown">
                        <option value="">-- Jenis Usaha --</option>
                        <?php foreach ($kat_usaha as $data) { ?>
                            <option value="<?php echo $data->nama; ?>">
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="field">
                <label>Surat Rekomendasi</label>
                <input type="file"  name="surat_rekom" id="surat_rekom"  onchange="return valid_surat4()"/>
                <div class="ui red left pointing label">Max: 1MB. Ekstensi .pdf</div><br>
            </div>
            <div class="ui info message">
                <div class="content">
                    <div class="header">Catatan !! </div>
                    <li> Form berikut digunakan untuk user yang sudah mengajukan surat rekomendasi sebelumnya .</li>
                    <li> Silahkan ganti nama pemohon bila mana ada nama lain yang ingin mengajukan surat rekomendasi.</li>
                </div>
            </div>
            <div id="loadingImg">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>
            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";
        var data = $(this).serialize();

        if (error == 0) {
            var nama_pemohon = $("#nama_pemohon").val();
            var nama_pemohon = nama_pemohon.trim();
            if (nama_pemohon.length == 0) {
                error++;
                message = "Nama Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_pemohon = $("#alamat_pemohon").val();
            var alamat_pemohon = alamat_pemohon.trim();
            if (alamat_pemohon.length == 0) {
                error++;
                message = "Alamat Pemohon wajib di isi.";
            }
        }
        if (error == 0) {
            var nama_usaha = $("#nama_usaha").val();
            var nama_usaha = nama_usaha.trim();
            if (nama_usaha.length == 0) {
                error++;
                message = " Nama Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var alamat_usaha = $("#alamat_usaha").val();
            var alamat_usaha = alamat_usaha.trim();
            if (alamat_usaha.length == 0) {
                error++;
                message = " Alamat Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var no_telp = $("#no_telp").val();
            var no_telp = no_telp.trim();
            if (no_telp.length == 0) {
                error++;
                message = " No telp wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = " Email harus diisi !!";
            } else if (!cekemail(email)) {
                error++;
                message = "Format Email tidak sesuai (admin@gmail.com). ";
            }
        }
        if (error == 0) {
            var jenis_usaha = $("#jenis_usaha").val();
            var jenis_usaha = jenis_usaha.trim();
            if (jenis_usaha.length == 0) {
                error++;
                message = "Jenis Usaha wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_rekom = $("#surat_rekom").val();
            var surat_rekom = surat_rekom.trim();
            if (surat_rekom.length == 0) {
                error++;
                message = "File Surat Rekomendasi wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: '<?php echo base_url('save2-rekomendasi'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImg").hide();
                    upload_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $("#loadingImg").hide();
                    gagal();
                }
                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>