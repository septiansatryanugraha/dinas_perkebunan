<style>
    .field-icon {
        float: left;
        margin-left: 91%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }

</style>
<div class="ui two column centered grid">
    <div class="column">
        <center><h2 class="ui teal image header">
                <a href="http://localhost/dinas_perkebunan"><img class="image" src="<?php echo base_url(); ?>assets/publik/img/images/logo-dinas.png"></a>
            </h2></center><br>
        <?php
        echo $this->session->flashdata("success");
        echo validation_errors('<div class="ui error message"><i class="close icon"></i><div class="header">', '</div></div>');
        $attributes = array('class' => 'ui large form');
        echo form_open('proses-daftar', $attributes);

        ?>
        <div class="ui stacked segment">
            <div class="ui error message"></div>
            <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
            <div class="field">
                <label>Nama Lengkap</label>
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="_nama_lengkap" placeholder="Nama Lengkap" max-length="35" autocomplete="off">
                </div>
            </div>
            <div class="field">
                <label>Email Pendaftar</label>
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="_email" placeholder="Masukan Email" autocomplete="off">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Password</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="password" name="_password" id="password-field" placeholder="Password"  autocomplete="off">
                    </div>
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
                </div>
                <div class="field">
                    <label>Konfirmasi Password</label>
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="password" name="_password" id="password-field2"  placeholder="Konfirmasi Password"  autocomplete="off">
                    </div>
                    <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password">
                </div>
            </div>
            <div class="ui fluid large teal submit button">Daftar</div>
        </div>
        <?php echo form_close(); ?>
        <div class="ui info message">
            <div class="content">
                <div class="header">Informasi !! </div>
                <p>Silahkan daftarkan akun anda, setelah proses mendaftar selesai, untuk aktivasi akun silahkan hubungi Admin Center Web System Kami <b>(08121309912)</b></p>
            </div>
        </div>
        <div class="ui message">
            SUDAH MENDAFTAR? <a href="<?php echo base_url() . 'login-pengajuan.html' ?>">Silahkan login</a>
        </div>
    </div>
</div>
<br><br>
<script>
    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>	