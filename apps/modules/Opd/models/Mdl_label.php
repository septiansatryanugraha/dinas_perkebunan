<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_label extends CI_Model
{
    const __tableLabel = 'tbl_label';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_label";
        return $table;
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM tbl_label WHERE id_pengajuan = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function get_where_custom($col, $value)
    {
        $table = $this->get_table();
        $this->db->where($col, $value);
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    public function selekKodePengajuan($id)
    {
        $sql = "SELECT * FROM tbl_sertifikasi WHERE id_user = '{$id}' and status= 'Selesai Approve' ";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selek_data1($id)
    {

        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $kabupaten .= "$data[nama_pemohon]";
        }

        return $kabupaten;
    }

    public function selek_data2($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[alamat_pemohon]";
        }

        return $res;
    }

    public function selek_data3($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[nama_usaha]";
        }

        return $res;
    }

    public function selek_data4($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[alamat_usaha]";
        }

        return $res;
    }

    public function selek_data5($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[no_telp]";
        }

        return $res;
    }

    public function selek_data6($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[email]";
        }

        return $res;
    }

    public function selek_data7($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[kode]";
        }

        return $res;
    }

    public function selek_data8($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[jenis_usaha]";
        }

        return $res;
    }

    public function selek_data9($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[jenis_komoditas]";
        }

        return $res;
    }

    public function selek_data10($id)
    {
        $this->db->order_by('id_sertifikasi', 'ASC');
        $hasil = $this->db->get_where('tbl_sertifikasi', array('id_sertifikasi' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[no_sertifikat]";
        }

        return $res;
    }

    public function selekKategori()
    {
        $this->db->from('tbl_usaha');
        $data = $this->db->get();

        return $data->result();
    }

    public function selekKategori2()
    {
        $this->db->from('tbl_usaha');
        $data = $this->db->get();

        return $data->result();
    }

    public function selekKomoditas()
    {
        $sql = "SELECT DISTINCT nama FROM tbl_komoditi";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function checkKode($data)
    {
        $this->db->select('kode_sertifikasi');
        $this->db->from('tbl_label');
        $this->db->where('kode_sertifikasi', $data['kode_sertifikasi']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}
