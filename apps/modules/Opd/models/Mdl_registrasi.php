<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_registrasi extends CI_Model
{
    const __tableName = 'tbl_cat_opd';

    function __construct()
    {
        parent::__construct();
    }

    public function selekKategori()
    {
        $this->db->from(self::__tableName);
        $data = $this->db->get();
        return $data->result();
    }

    public function select_by_id($id)
    {
        $sql = "SELECT * FROM tbl_user WHERE id_user = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function selek_file()
    {
        $sql = " select * from tbl_file_form WHERE id_file in ('1')";
        $data = $this->db->query($sql);
        return $data->result();
    }
}
