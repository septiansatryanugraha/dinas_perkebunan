<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_web extends CI_Model
{
    const __tableName = 'tbl_user';
    const __tableId = 'id_user';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_category";
        return $table;
    }

    function get($order_by)
    {
        $table = $this->get_table();
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    function get_with_limit($limit, $offset, $order_by)
    {
        $table = $this->get_table();
        $this->db->limit($limit, $offset);
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id)
    {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom($col, $value)
    {
        $table = $this->get_table();
        $this->db->where($col, $value);
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data)
    {
        $table = $this->get_table();
        $this->db->insert($table, $data);
    }

    function _update($id, $data)
    {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }

    function updateStatusHistory($id)
    {
        $update_query = "UPDATE tbl_history SET status_baca=1 WHERE status_baca=0 AND id_user = '{$id}'";
        $this->db->query($update_query);
        return $this->db->affected_rows();
    }

    function selectNotifHistory($id)
    {
        $selek_query = "SELECT * FROM tbl_history WHERE id_user = '{$id}' ORDER BY id_history DESC LIMIT 30";
        $data = $this->db->query($selek_query);
        return $data->result();
    }

    public function totalCountHistory($id)
    {
        $data = $this->db
            ->where('id_user=', $id)
            ->where('status_baca=', 0)
            ->get('tbl_history');
        return $data->num_rows();
    }
}
