<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_products extends CI_Model
{
    const __tableusaha = 'tbl_usaha';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_form_rekomendasi";
        return $table;
    }

    function get_table2()
    {
        $table = "tbl_history";
        return $table;
    }

    public function selekKategori2()
    {
        $this->db->from(self::__tableusaha);
        $data = $this->db->get();
        return $data->result();
    }

    function selek_file()
    {
        $sql = " select * from tbl_file_form WHERE id_file in ('2','3','4')";
        $data = $this->db->query($sql);
        return $data->result();
    }

    function get($order_by)
    {
        $table = $this->get_table();
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM tbl_form_rekomendasi WHERE id_rekomendasi = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function get_where_custom($col, $value)
    {
        $table = $this->get_table();
        $this->db->where($col, $value);
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_history($col, $value)
    {
        $table = $this->get_table2();
        $this->db->where($col, $value);
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    function total_dokumen($column, $value)
    {
        $table = $this->get_table();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function total_histori($column, $value)
    {
        $table = $this->get_table2();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function get_total($id_user)
    {
        $query = $this->db->query("SELECT tanggal,COUNT(id_dokumen) AS id_dokumen FROM tbl_dokumen WHERE  id_user = '{$id_user}' and tanggal between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now()) GROUP BY tanggal");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function kode()
    {
        $this->db->select('RIGHT(tbl_form_rekomendasi.kode,2) as kode', FALSE);
        $this->db->order_by('kode', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tbl_form_rekomendasi');  //cek dulu apakah ada sudah ada kode di tabel.    
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia    
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $tahun = date('Y');
        $bulan = date('m');
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodetampil = "SRK" . $tahun . $bulan . $batas;  //format kode
        return $kodetampil;
    }
}
