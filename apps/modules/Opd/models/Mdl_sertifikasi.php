<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_sertifikasi extends CI_Model
{
    const __tablesertifikat = 'tbl_sertifikat';
    const __tableusaha = 'tbl_usaha';
    const __tableName4 = 'tbl_form_rekomendasi';
    const __tableId4 = 'id_rekomendasi';
    const __tbl_sertifikat = 'tbl_sertifikat';
    const __tableId5 = 'id_sertifikat';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_sertifikasi";
        return $table;
    }

    public function selekTes($kode_sertifikasi)
    {
        $this->db->from('tbl_koordinat');
        $this->db->where('kode_sertifikasi', $kode_sertifikasi);
        $data = $this->db->get();
        return $data->result();
    }

    public function select_kode($id)
    {
        $sql = "SELECT * FROM " . self::__tableName4 . " WHERE " . self::__tableId4 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function select_sertifikat($id)
    {
        $sql = "SELECT * FROM " . self::__tbl_sertifikat . " WHERE " . self::__tableId5 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM tbl_sertifikasi WHERE id_sertifikasi = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function get_where_custom($col, $value)
    {
        $table = $this->get_table();
        $this->db->where($col, $value);
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    public function selekKategori()
    {
        $this->db->from(self::__tablesertifikat);
        $data = $this->db->get();
        return $data->result();
    }

    public function selekKategori2()
    {
        $this->db->from(self::__tableusaha);
        $data = $this->db->get();
        return $data->result();
    }

    public function selekKomoditas()
    {
        $sql = "SELECT DISTINCT nama FROM tbl_komoditi";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKategori3($id)
    {
        $sql = "SELECT * FROM tbl_user WHERE id_user = '{$id}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selek_data1($id)
    {

        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $kabupaten .= "$data[nama_lengkap]";
        }

        return $kabupaten;
    }

    public function selek_data2($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[alamat]";
        }

        return $res;
    }

    public function selek_data3($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[nama_usaha]";
        }

        return $res;
    }

    public function selek_data4($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[alamat_usaha]";
        }

        return $res;
    }

    public function selek_data5($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[telp]";
        }

        return $res;
    }

    public function selek_data6($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[email]";
        }

        return $res;
    }

    public function selek_data7($id)
    {
        $this->db->order_by('id_user', 'ASC');
        $hasil = $this->db->get_where('tbl_user', array('id_user' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[kode_iup]";
        }

        return $res;
    }

    public function checkKode($data)
    {
        $this->db->select('kode_rekomendasi');
        $this->db->from('tbl_sertifikasi');
        $this->db->where('kode_rekomendasi', $data['kode_rekomendasi']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function kode()
    {
        $this->db->select('RIGHT(tbl_sertifikasi.kode,2) as kode', FALSE);
        $this->db->order_by('kode', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tbl_sertifikasi');  //cek dulu apakah ada sudah ada kode di tabel.    
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia    
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $tahun = date('Y');
        $bulan = date('m');
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodetampil = "SRF" . $tahun . $bulan . $batas;  //format kode
        return $kodetampil;
    }
}
