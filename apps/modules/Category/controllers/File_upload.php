<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_upload extends AUTH_Controller
{
    const __tableName = 'tbl_file_form';
    const __tableId = 'id_file';
    const __folder = 'v_upload/';
    const __kode_menu = 'file';
    const __title = 'Upload File ';
    const __model = 'M_file_upload';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_file_upload->get_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->nama_file;
            $row[] = $brand->created_by;

            //add html for action
            $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-file-pengajuan/' . $brand->id_file, ' <span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-file" data-id=' . "'" . $brand->id_file . "'" . '><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }

        /* ini harus ada boss */ else {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $this->db->trans_begin();
        try {
            $config['upload_path'] = "./upload/file_form/";
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '2048'; //maksimum besar file 5M
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload("path")) {
                $data_file = $this->upload->data();
                $path['link'] = "upload/file_form/";
                $data = array(
                    'nama_file' => $data_file['file_name'],
                    'path' => $path['link'] . '' . $data_file['file_name'],
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $result = $this->db->insert(self::__tableName, $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {

        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_file_upload->selectById($id);

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {
        $where = trim($this->input->post(self::__tableId));

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $this->db->trans_begin();
        try {
            $config['upload_path'] = "./upload/file_form/";
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '2048'; //maksimum besar file 5M
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload("path")) {
                $data_file = $this->upload->data();
                $path['link'] = "upload/file_form/";
                $data = array(
                    'nama_file' => $data_file['file_name'],
                    'path' => $path['link'] . '' . $data_file['file_name'],
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => "GAGAL");
                }
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => "GAGAL");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $ex->getMessage());
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $this->load->helper("file");
        $token = $this->input->post(self::__tableId);
        $gambar = $this->db->get_where(self::__tableName, array(self::__tableId => $token));
        if ($gambar->num_rows() > 0) {
            $hasil = $gambar->row();
            $judul = $hasil->path;
            //hapus unlink file pdf
            if (file_exists($file = $judul)) {
                unlink($file);
            }
            $this->db->delete(self::__tableName, array(self::__tableId => $token));
        }
        echo "{}";
    }
}
