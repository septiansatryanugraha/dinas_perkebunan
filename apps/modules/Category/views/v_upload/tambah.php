<?php $this->load->view('_heading/_headerContent') ?>
<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-8" id="newContain">
                <!-- form start -->
                <form class="form-horizontal" id="form-tambah" method="POST">
                    <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
                    <div class="box-body">
                        <div class="form-group">      
                            <label for="inputFoto" class="col-sm-2 control-label">File Form Pengajuan</label>
                            <div class="col-sm-7">
                                <input type="file" class="form-control" name="path" id="path">
                                <p style='color: red; font-size: 14px;'> *Maksimal File 2 MB</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        <a class="klik ajaxify" href="<?php echo base_url('file-pengajuan'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-tambah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var path = $("#path").val();
            var path = path.trim();
            if (path.length == 0) {
                error++;
                message = "File wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('save-file-pengajuan'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-tambah").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?php echo base_url("add-file-pengajuan"); ?>'", 450);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>