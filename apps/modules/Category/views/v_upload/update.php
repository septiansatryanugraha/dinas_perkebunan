<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <form id="form-update" class="form-horizontal" method="POST">
            <input type="hidden" name="id_file" value="<?php echo $brand->id_file; ?>">
            <input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
            <div class="row">
                <div class="col-md-9">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Data</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">      
                            <label for="inputFoto" class="col-sm-2 control-label">File Form Pengajuan</label>
                            <div class="col-sm-7">
                                <input type="file" class="form-control" name="path" id="path">
                                <p style='color: red; font-size: 14px;'> *Maksimal File 2 MB</p>
                                <a href="<?php echo base_url() . $brand->path; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_file; ?></font></b></a>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        <a class="klik ajaxify" href="<?php echo base_url('file-pengajuan'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";

        if (error == 0) {
            var path = $("#path").val();
            var path = path.trim();
            if (path.length == 0) {
                error++;
                message = "File wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?php echo base_url('update-file-pengajuan'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-update").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        save_berhasil();
                        setTimeout("window.location='<?php echo base_url("file-pengajuan"); ?>'", 450);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });
</script>