<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{

    public function total_label()
    {
        $data = $this->db->get('tbl_label');
        return $data->num_rows();
    }

    public function total_user()
    {
        $data = $this->db->get('tbl_user');
        return $data->num_rows();
    }

    public function total_sertifikasi()
    {
        $data = $this->db->get('tbl_sertifikasi');
        return $data->num_rows();
    }

    public function status_approve()
    {
        $sql = " SELECT *FROM tbl_sertifikasi where status='Selesai Approve' ORDER BY id_sertifikasi DESC limit 25";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function history()
    {
        $sql = " SELECT tbl_user.nama as nama, tbl_history.keterangan as keterangan, tbl_history.tanggal as tanggal, tbl_history.status as status FROM tbl_history
        LEFT JOIN tbl_user ON tbl_user.id_user = tbl_history.id_user
        ORDER BY id_history DESC limit 30";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function get_total_sertifikasi()
    {
        $query = $this->db->query("SELECT tanggal,COUNT(id_sertifikasi) AS id_sertifikasi FROM tbl_sertifikasi WHERE tanggal between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now()) GROUP BY tanggal");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
}
