<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('layouts/footer', $data);
    }

    public function index()
    {
        $data['label'] = $this->M_total->total_label();
        $data['user'] = $this->M_total->total_user();
        $data['sertifikasi'] = $this->M_total->total_sertifikasi();
        $data['graph'] = $this->M_total->get_total_sertifikasi();
        $data['status_approve'] = $this->M_total->status_approve();
        $data['history'] = $this->M_total->history();
        $data['userdata'] = $this->userdata;

        $data['page'] = "home";
        $data['judul'] = "Beranda";
        $this->loadkonten('home', $data);
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */