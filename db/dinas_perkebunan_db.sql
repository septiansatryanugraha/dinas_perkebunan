-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dinas_perkebunan_db.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grup_id` int(10) DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) unsigned zerofill DEFAULT NULL,
  `hidden` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `last_created_date` datetime DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `created_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `last_update_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `last_login_user` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `grup_id` (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.admin: ~5 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `grup_id`, `username`, `email`, `password`, `nama`, `foto`, `status`, `hidden`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`, `last_login_user`) VALUES
	(1, 1, 'superadmin', NULL, '40587bff0e72b6fdbba30c40c95e148a', 'Super Admin', '65ee4702469219563507d639e5445340.jpg', 00000000003, '1', '0000-00-00 00:00:00', '2019-09-19 09:41:40', '', 'Super Admin', '2022-03-15 02:48:14'),
	(2, 2, 'admin', 'disbun2019@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Admin Dinas Perkebunan', 'no-image.jpg', 00000000003, '0', '0000-00-00 00:00:00', '2019-09-19 09:41:40', '', 'Admin Dinas Perkebunan2', '2022-03-11 15:08:19'),
	(3, 3, 'admin_disbun2019', 'admindisbun1@gmail.com', '6ba2acd955247fc45ea254db1fd063b6', 'admin disbun', 'no-image.jpg', 00000000003, '0', '2018-04-14 12:42:35', '2019-09-19 09:54:38', '', 'Admin Dinas Perkebunan', '2019-12-04 12:13:01'),
	(4, 2, 'wahyu', 'wahyupunk10@gmail.com', '32c9e71e866ecdbc93e497482aa6779f', 'Wahyu Sugiarto', 'no-image.jpg', 00000000003, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '2022-03-07 18:24:19'),
	(5, 3, 'admin_disbun_sts2019', 'admindisbun2@gmail.com', 'a40870c2a1d601d782273e2038fec1e7', 'admin disbun 2', 'no-image.jpg', 00000000003, '0', '2019-09-19 09:51:08', '2019-09-19 09:55:09', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan', '2019-12-04 12:13:35');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.ci_sessions
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) CHARACTER SET utf8 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `timestamp` int(10) unsigned DEFAULT 0,
  `data` text CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.ci_sessions: ~0 rows (approximately)
DELETE FROM `ci_sessions`;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.grup
DROP TABLE IF EXISTS `grup`;
CREATE TABLE IF NOT EXISTS `grup` (
  `grup_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_grup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `deskripsi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `last_created_date` datetime DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `created_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `last_update_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`grup_id`),
  UNIQUE KEY `grup_id` (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.grup: ~4 rows (approximately)
DELETE FROM `grup`;
/*!40000 ALTER TABLE `grup` DISABLE KEYS */;
INSERT INTO `grup` (`grup_id`, `nama_grup`, `deskripsi`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`) VALUES
	(1, 'Super Admin', 'Full  Akses', NULL, NULL, NULL, NULL),
	(2, 'Admin', 'Akses Terbatas', '2022-03-13 08:17:21', '2022-03-13 08:17:21', 'superadmin', 'superadmin'),
	(3, 'Admin Bagian 1', 'Akses Terbatas', '2022-03-13 08:17:21', '2022-03-13 08:17:21', 'admin', 'admin'),
	(4, 'Admin Bagian 2', 'Akses Terbatas', '2022-03-13 08:17:21', '2022-03-13 08:17:21', 'admin', 'admin');
/*!40000 ALTER TABLE `grup` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.menu_akses
DROP TABLE IF EXISTS `menu_akses`;
CREATE TABLE IF NOT EXISTS `menu_akses` (
  `id_menuakses` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_menu` bigint(20) DEFAULT NULL,
  `grup_id` bigint(20) DEFAULT NULL,
  `view` double DEFAULT NULL,
  `add` double DEFAULT NULL,
  `edit` double DEFAULT NULL,
  `del` double DEFAULT NULL,
  PRIMARY KEY (`id_menuakses`),
  UNIQUE KEY `id_menuakses` (`id_menuakses`),
  KEY `id_menu` (`id_menu`),
  KEY `grup_id` (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.menu_akses: ~30 rows (approximately)
DELETE FROM `menu_akses`;
/*!40000 ALTER TABLE `menu_akses` DISABLE KEYS */;
INSERT INTO `menu_akses` (`id_menuakses`, `id_menu`, `grup_id`, `view`, `add`, `edit`, `del`) VALUES
	(1, 1, 2, 1, 0, 0, 0),
	(2, 2, 2, 1, 1, 1, 1),
	(3, 3, 2, 1, 1, 1, 1),
	(4, 4, 2, 1, 0, 1, 1),
	(5, 5, 2, 1, 0, 1, 1),
	(6, 6, 2, 1, 0, 1, 1),
	(7, 7, 2, 1, 0, 0, 0),
	(8, 8, 2, 1, 1, 1, 1),
	(9, 9, 2, 1, 1, 1, 1),
	(10, 10, 2, 1, 1, 1, 1),
	(11, 1, 3, 1, 0, 0, 0),
	(12, 2, 3, 1, 1, 1, 1),
	(13, 3, 3, 1, 1, 1, 1),
	(14, 4, 3, 1, 0, 1, 1),
	(15, 5, 3, 1, 0, 1, 1),
	(16, 6, 3, 1, 0, 1, 1),
	(17, 7, 3, 0, 0, 0, 0),
	(18, 8, 3, 0, 0, 0, 0),
	(19, 9, 3, 0, 0, 0, 0),
	(20, 10, 3, 0, 0, 0, 0),
	(21, 1, 4, 1, 0, 0, 0),
	(22, 2, 4, 1, 1, 1, 1),
	(23, 3, 4, 1, 1, 1, 1),
	(24, 4, 4, 1, 0, 1, 1),
	(25, 5, 4, 1, 0, 1, 1),
	(26, 6, 4, 1, 0, 1, 1),
	(27, 7, 4, 0, 0, 0, 0),
	(28, 8, 4, 0, 0, 0, 0),
	(29, 9, 4, 0, 0, 0, 0),
	(30, 10, 4, 0, 0, 0, 0);
/*!40000 ALTER TABLE `menu_akses` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.status_grup
DROP TABLE IF EXISTS `status_grup`;
CREATE TABLE IF NOT EXISTS `status_grup` (
  `id_status` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_status`),
  UNIQUE KEY `id_status` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.status_grup: ~15 rows (approximately)
DELETE FROM `status_grup`;
/*!40000 ALTER TABLE `status_grup` DISABLE KEYS */;
INSERT INTO `status_grup` (`id_status`, `nama`) VALUES
	(1, 'Unread'),
	(2, 'Read'),
	(3, 'Aktif'),
	(4, 'Belum Aktif'),
	(5, 'Hidden'),
	(6, 'Publish'),
	(11, 'Non aktif'),
	(12, 'Belum Approve'),
	(13, 'Approve'),
	(14, 'Survey'),
	(15, 'Selesai Survey'),
	(16, 'Selesai Approve'),
	(17, 'Belum Direkomendasi'),
	(18, 'Rekomendasi'),
	(19, 'Approve Pelabelan');
/*!40000 ALTER TABLE `status_grup` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_category
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id_category` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `id_category` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_category: ~2 rows (approximately)
DELETE FROM `tbl_category`;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` (`id_category`, `category`, `slug`) VALUES
	(1, 'Personal Care', 'personal-care'),
	(2, 'Handphone', 'handphone');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_file_form
DROP TABLE IF EXISTS `tbl_file_form`;
CREATE TABLE IF NOT EXISTS `tbl_file_form` (
  `id_file` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `path` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `created_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `updated_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_file`),
  UNIQUE KEY `id_file` (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_file_form: ~6 rows (approximately)
DELETE FROM `tbl_file_form`;
/*!40000 ALTER TABLE `tbl_file_form` DISABLE KEYS */;
INSERT INTO `tbl_file_form` (`id_file`, `nama_file`, `path`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
	(1, 'FORMAT_2__PROFIL_PRODUSEN.docx', 'upload/file_form/FORMAT_2__PROFIL_PRODUSEN.docx', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan', '2019-10-02 05:23:40', '2019-10-02 05:23:40'),
	(2, 'download_FORMAT_1__SURAT_PERMOHONAN.docx', 'upload/file_form/download_FORMAT_1__SURAT_PERMOHONAN.docx', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan', '2019-10-02 05:24:02', '2019-10-02 05:24:02'),
	(3, 'download_FORMAT_4__SURAT_PERJANJIAN_KERJASAMA_PRODUSEN_BENIH.docx', 'upload/file_form/download_FORMAT_4__SURAT_PERJANJIAN_KERJASAMA_PRODUSEN_BENIH.docx', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan', '2019-10-02 05:24:09', '2019-10-02 05:24:09'),
	(4, 'download_FORMAT_5__SURAT_PERNYATAAN_MEMATUHI_PERUNDANGAN.docx', 'upload/file_form/download_FORMAT_5__SURAT_PERNYATAAN_MEMATUHI_PERUNDANGAN.docx', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan', '2019-10-02 05:24:16', '2019-10-02 05:24:16'),
	(5, 'Rab_Penawaran_Aplikasi_Android_Web.pdf', 'upload/file_form/Rab_Penawaran_Aplikasi_Android_Web.pdf', 'Super Admin', 'Super Admin', '2022-03-13 08:51:09', '2022-03-13 08:51:09'),
	(6, 'invoice-AMD202101001.pdf', 'upload/file_form/invoice-AMD202101001.pdf', 'Super Admin', 'Super Admin', '2022-03-13 09:00:58', '2022-03-13 09:05:26');
/*!40000 ALTER TABLE `tbl_file_form` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_form_rekomendasi
DROP TABLE IF EXISTS `tbl_form_rekomendasi`;
CREATE TABLE IF NOT EXISTS `tbl_form_rekomendasi` (
  `id_rekomendasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `kode` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `kode_rekomendasi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `kode_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_pemohon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_pemohon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_usaha` text CHARACTER SET utf8 DEFAULT NULL,
  `alamat_usaha` text CHARACTER SET utf8 DEFAULT NULL,
  `no_telp` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `folder` int(255) DEFAULT NULL COMMENT 'Folder untuk multiple image',
  `nama_surat_nib` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_nib` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_rekomendasi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_rekomendasi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_kerjasama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_kerjasama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_patuh` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_patuh` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `tipe` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `nama_file_rekom` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_rekom` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id_rekomendasi`),
  UNIQUE KEY `id_rekomendasi` (`id_rekomendasi`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_form_rekomendasi: ~0 rows (approximately)
DELETE FROM `tbl_form_rekomendasi`;
/*!40000 ALTER TABLE `tbl_form_rekomendasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_form_rekomendasi` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_history
DROP TABLE IF EXISTS `tbl_history`;
CREATE TABLE IF NOT EXISTS `tbl_history` (
  `id_history` bigint(255) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `kode` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `kode_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status_baca` int(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id_history`),
  UNIQUE KEY `id_history` (`id_history`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_history: ~24 rows (approximately)
DELETE FROM `tbl_history`;
/*!40000 ALTER TABLE `tbl_history` DISABLE KEYS */;
INSERT INTO `tbl_history` (`id_history`, `id_user`, `kode`, `kode_iup`, `status`, `keterangan`, `created_date`, `created_by`, `status_baca`, `tanggal`) VALUES
	(1, 1, 'SRF202203001', 'IUP-11/12-2022', 'Pengajuan', 'user <b>Moch Wahyu Sugiarto</b> Sedang melakukan proses pengajuan Sertifikasi <b></b> dengan jenis Komoditas <b>Kakao</b> ke Dinas Perkebunan Jawa Timur dengan Kode IUP<b> </b>', '2022-03-05 19:54:30', 'System', 1, '2022-03-05'),
	(2, 1, 'SRF202203001', 'IUP-11/12-2022', 'Belum Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Belum Approve </b>kepada pengajuan user <b>Moch Wahyu Sugiarto</b> karena ada beberapa berkas atau file dokumen yang belum lengkap, harap di baca di catatan pengajuan ', '2022-03-05 20:21:29', 'System', 1, '2022-03-05'),
	(3, 1, 'SRF202203001', 'IUP-11/12-2022', 'Belum Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Belum Approve </b>kepada pengajuan user <b>Moch Wahyu Sugiarto</b> karena ada beberapa berkas atau file dokumen yang belum lengkap, harap di baca di catatan pengajuan ', '2022-03-08 10:28:07', 'System', 1, '2022-03-08'),
	(4, 1, 'SRF202203001', 'IUP-11/12-2022', 'Revisi', 'user <b></b> Sedang melakukan revisi dan proses pengajuan ulang Sertifikasi dengan jenis Komoditas <b>Kakao</b> ke Dinas Perkebunan Jawa Timur', '2022-03-08 13:36:54', 'System', 1, '2022-03-08'),
	(5, 1, 'SRF202203001', 'IUP-11/12-2022', 'Survey', '<b>Admin Dinas Perkebunan</b> Sedang menjadwalkan proses <b>Survey </b>kepada user <b>Moch Wahyu Sugiarto S.Kom</b> pada tanggal <b>01-03-2022</b>', '2022-03-08 13:43:40', 'System', 1, '2022-03-08'),
	(6, 1, 'SRF202203001', '', 'Selesai Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Selesai Approve </b>serta Pengiriman Dokumen ( kurang lebih ) 1 minggu setelah proses Approve ', '2022-03-08 13:43:57', 'System', 1, '2022-03-08'),
	(7, 1, 'SRF202203001', 'IUP-11/12-2022', 'Selesai Survey', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Selesai Survey </b>kepada usaha yang dimiliki user <b>Moch Wahyu Sugiarto S.Kom</b> dan wajib melakukan pembayaran sejumlah Nominal yang tertera di surat tagihan retribusi dan melakukan upload bukti Transfer di Sistem PRIMA PSBP ', '2022-03-08 13:44:48', 'System', 1, '2022-03-08'),
	(8, 1, 'SRF202203001', '', 'Upload Bukti Transfer', 'user <b>Moch Wahyu Sugiarto S.Kom</b> Sudah melakukan pembayaran dan telah melakukan proses upload bukti transfer STS', '2022-03-08 13:55:21', 'System', 1, '2022-03-08'),
	(9, 1, 'SRF202203001', '', 'Selesai Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Selesai Approve </b>serta Pengiriman Dokumen ( kurang lebih ) 1 minggu setelah proses Approve ', '2022-03-08 14:22:30', 'System', 1, '2022-03-08'),
	(10, 1, 'SRF202203001', '', 'Pengajuan Pelabelan', 'user <b>Moch Wahyu Sugiarto S.Kom</b> Sedang melakukan proses pengajuan Pelabelan dengan jenis Komoditas <b>Kakao</b> ke Dinas Perkebunan Jawa Timur', '2022-03-09 00:00:00', 'System', 1, '2022-03-09'),
	(11, 1, 'SRF202203001', '', 'Pengajuan Pelabelan', 'user <b>Moch Wahyu Sugiarto S.Kom</b> Sedang melakukan proses pengajuan Pelabelan dengan jenis Komoditas <b>Kakao</b> ke Dinas Perkebunan Jawa Timur', '2022-03-09 00:00:00', 'System', 1, '2022-03-09'),
	(12, 1, 'SRF202203001', '', 'Belum Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Belum Approve </b>kepada pengajuan user <b></b> karena ada beberapa verifikasi data yang belum valid silahkan di cek di catatan pengajuan', '2022-03-10 09:42:19', 'System', 1, '2022-03-10'),
	(13, 1, 'SRF202203001', '', 'Revisi', 'user <b>Moch Wahyu Sugiarto S.Kom</b> Sedang melakukan revisi dan proses pengajuan pelabelan dengan jenis Komoditas <b>Kakao</b> ke Dinas Perkebunan Jawa Timur', '2022-03-10 10:33:47', 'System', 1, '2022-03-10'),
	(14, 1, 'SRF202203001', '', 'Approve Pelabelan', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Approve Pelabelan </b>kepada pengajuan user <b></b> dan proses pelabelan segera di lakukan oleh dinas perkebunan provinsi jawa timur', '2022-03-10 10:36:31', 'System', 1, '2022-03-10'),
	(15, 1, '', '', 'Approve', '<b>Admin Dinas Perkebunan</b> Telah Menyatakan <b>Approve </b>Berkas kepada user <b>Moch Wahyu Sugiarto</b> silahkan mulai melakukan pengajuan.', '2022-03-10 15:09:22', 'System', 1, '2022-03-10'),
	(16, 2, NULL, NULL, 'Belum Approve', '<b>Super Admin</b> Telah Menyatakan <b>Belum Approve </b>Berkas kepada user <b>satrio anggardha</b> karena ada berapa berkas yang belum valid silahkan dicek di catatan profil.', '2022-03-13 09:23:15', 'System', NULL, '2022-03-13'),
	(17, 1, 'SRF202203001', 'IUP-11/12-2022', '', '<b>Super Admin</b> Sedang menjadwalkan proses <b> </b>kepada user <b>Moch Wahyu Sugiarto S.Kom</b> pada tanggal <b>01-03-2022</b>', '2022-03-13 09:34:04', 'System', NULL, '2022-03-13'),
	(18, 1, 'SRF202203001', 'IUP-11/12-2022', '', '<b>Super Admin</b> Sedang menjadwalkan proses <b> </b>kepada user <b>Moch Wahyu Sugiarto S.Kom</b> pada tanggal <b>01-03-2022</b>', '2022-03-13 09:37:15', 'System', NULL, '2022-03-13'),
	(19, 1, 'SRF202203001', NULL, '', '<b>Super Admin</b> Telah Menyatakan <b> </b>serta Pengiriman Dokumen ( kurang lebih ) 1 minggu setelah proses Approve ', '2022-03-13 13:40:34', 'System', NULL, '2022-03-13'),
	(20, 1, 'SRF202203001', NULL, '', '<b>Super Admin</b> Telah Menyatakan <b> </b>serta Pengiriman Dokumen ( kurang lebih ) 1 minggu setelah proses Approve ', '2022-03-13 13:41:07', 'System', NULL, '2022-03-13'),
	(21, 1, 'SRF202203001', NULL, 'Approve Pelabelan', '<b>Super Admin</b> Telah Menyatakan <b>Approve Pelabelan </b>kepada pengajuan user <b></b> dan proses pelabelan segera di lakukan oleh dinas perkebunan provinsi jawa timur', '2022-03-13 13:48:19', 'System', NULL, '2022-03-13'),
	(22, 1, 'SRF202203001', NULL, 'Approve Pelabelan', '<b>Super Admin</b> Telah Menyatakan <b>Approve Pelabelan </b>kepada pengajuan user <b></b> dan proses pelabelan segera di lakukan oleh dinas perkebunan provinsi jawa timur', '2022-03-13 13:52:56', 'System', NULL, '2022-03-13'),
	(23, NULL, NULL, NULL, '4', '<b>Super Admin</b> Telah Menyatakan <b>4 </b>kepada pengajuan user <b></b> karena ada beberapa verifikasi data yang belum valid silahkan di cek di catatan pengajuan', '2022-03-13 19:14:16', 'System', NULL, '2022-03-13'),
	(24, 1, 'SRF202203002', 'IUP-11/12-2022', 'Pengajuan', 'user <b>Moch Wahyu Sugiarto</b> Sedang melakukan proses pengajuan Sertifikasi dengan jenis Komoditas <b>Tembakau</b> ke Dinas Perkebunan Jawa Timur dengan Kode IUP<b> IUP-11/12-2022</b>', '2022-03-15 02:37:34', 'System', NULL, '2022-03-15');
/*!40000 ALTER TABLE `tbl_history` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_komoditi
DROP TABLE IF EXISTS `tbl_komoditi`;
CREATE TABLE IF NOT EXISTS `tbl_komoditi` (
  `id_komoditi` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis` int(10) DEFAULT NULL,
  PRIMARY KEY (`id_komoditi`),
  UNIQUE KEY `id_komoditi` (`id_komoditi`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_komoditi: ~14 rows (approximately)
DELETE FROM `tbl_komoditi`;
/*!40000 ALTER TABLE `tbl_komoditi` DISABLE KEYS */;
INSERT INTO `tbl_komoditi` (`id_komoditi`, `nama`, `jenis`) VALUES
	(1, 'Tebu', 1),
	(2, 'Kopi', 2),
	(3, 'Kakao', 2),
	(4, 'Tembakau', 2),
	(5, 'Jambu mete', 2),
	(6, 'Cengkeh', 2),
	(7, 'Kelapa', 2),
	(9, 'Nilam', 1),
	(10, 'Nilam', 2),
	(11, 'Kopi', 3),
	(12, 'Kakao', 3),
	(13, 'Tembakau', 3),
	(14, 'Tembakau', 1),
	(16, 'Cengkeh', 3);
/*!40000 ALTER TABLE `tbl_komoditi` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_koordinat
DROP TABLE IF EXISTS `tbl_koordinat`;
CREATE TABLE IF NOT EXISTS `tbl_koordinat` (
  `id_koordinat` bigint(20) NOT NULL AUTO_INCREMENT,
  `latitude` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `gambar` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_komoditas` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `kode_sertifikasi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `ket` text CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_koordinat`),
  UNIQUE KEY `id_koordinat` (`id_koordinat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_koordinat: ~4 rows (approximately)
DELETE FROM `tbl_koordinat`;
/*!40000 ALTER TABLE `tbl_koordinat` DISABLE KEYS */;
INSERT INTO `tbl_koordinat` (`id_koordinat`, `latitude`, `longitude`, `gambar`, `jenis_komoditas`, `kode_sertifikasi`, `jenis_usaha`, `ket`, `created_date`) VALUES
	(1, '          2', '          2', 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_1646484870.jpg', 'Badan Usaha ( Firma, CV, UD, PD)', 'SRF202203001', '', 'Ket1', '2022-03-08 13:36:54'),
	(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-13 13:40:34'),
	(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-13 13:41:07'),
	(4, '1', '2', 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_1647286654.jpg', 'Tembakau', 'SRF202203002', 'Badan Usaha ( Firma, CV, UD, PD)', 'asd', '2022-03-15 02:37:34');
/*!40000 ALTER TABLE `tbl_koordinat` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_label
DROP TABLE IF EXISTS `tbl_label`;
CREATE TABLE IF NOT EXISTS `tbl_label` (
  `id_pengajuan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `kode_sertifikasi` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `no_sertifikat` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_pemohon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_pemohon` text CHARACTER SET utf8 DEFAULT NULL,
  `nama_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_usaha` text CHARACTER SET utf8 DEFAULT NULL,
  `no_telp` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_usaha` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_komoditas` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `jumlah_label` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `catatan` text CHARACTER SET utf8 DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_pengajuan`),
  UNIQUE KEY `id_pengajuan` (`id_pengajuan`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dinas_perkebunan_db.tbl_label: ~1 rows (approximately)
DELETE FROM `tbl_label`;
/*!40000 ALTER TABLE `tbl_label` DISABLE KEYS */;
INSERT INTO `tbl_label` (`id_pengajuan`, `id_user`, `kode_sertifikasi`, `no_sertifikat`, `nama_pemohon`, `alamat_pemohon`, `nama_usaha`, `alamat_usaha`, `no_telp`, `email`, `jenis_usaha`, `jenis_komoditas`, `jumlah_label`, `status`, `catatan`, `created_date`, `updated_date`, `updated_by`) VALUES
	(1, 1, 'SRF202203001', 'DISBUN12-11/II/2022', 'Moch Wahyu Sugiarto S.Kom', 'Perum Modopuro Perkasa No 41, Ds Mojosari - Mojoketo , Jawa Timur', 'CV Angkasa Muda Digital', 'Jl Pakis Gunung 1/68 Surabaya', '089675773470', 'wahyupunk10@gmail.com', 'Badan Usaha ( Firma, CV, UD, PD)', 'Kakao', '15', 'Approve Pelabelan', '- jumlah cetak sertifikat  tidak sesuai dengan pengajuan', '2022-03-09', '2022-03-13', 'Super Admin');
/*!40000 ALTER TABLE `tbl_label` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_login
DROP TABLE IF EXISTS `tbl_login`;
CREATE TABLE IF NOT EXISTS `tbl_login` (
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `password` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `gambar` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `tipe` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `last_login_user` datetime DEFAULT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_login: ~3 rows (approximately)
DELETE FROM `tbl_login`;
/*!40000 ALTER TABLE `tbl_login` DISABLE KEYS */;
INSERT INTO `tbl_login` (`email`, `password`, `gambar`, `tipe`, `nama`, `status`, `last_login_user`) VALUES
	('fatoni@gmail.com', 'ZmF0b25p', '', 'opd', 'Muhammad Fathoni', 'Aktif', '0000-00-00 00:00:00'),
	('satrio.job@gmail.com', 'cmFoYXNpYQ==', '', 'opd', 'satrio anggardha', 'Aktif', '0000-00-00 00:00:00'),
	('wahyupunk10@gmail.com', 'd2FoeXU=', '', 'opd', 'Moch Wahyu Sugiarto', 'Aktif', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tbl_login` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_menu
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `kode_menu` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `menu_file` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `last_created_date` datetime DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `created_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `last_update_by` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  UNIQUE KEY `id_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_menu: ~10 rows (approximately)
DELETE FROM `tbl_menu`;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `parent`, `kode_menu`, `menu_file`, `urutan`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`) VALUES
	(1, 'Dashboard', 'Dashboard', 'fa fa-home', 0, 'admin', 'view', 1, '2018-04-20 09:17:58', '2018-05-25 13:41:58', '', 'Moch Wahyu Sugiarto'),
	(2, 'Upload File', 'file-pengajuan', 'fa fa-list', 0, 'file', 'view,add,edit,del', 2, '2019-01-17 03:45:14', '2019-09-18 03:19:52', 'Admin Hippam', 'Admin Dinas Perkebunan'),
	(3, 'Master User', 'master-opd', 'fa fa-user-plus', 0, 'master-opd', 'view,add,edit,del', 3, '2019-07-30 05:14:52', '2019-08-27 10:46:37', 'Admin Biro Hukum', 'Admin Dinas Perkebunan'),
	(4, 'Master Sertifikasi', 'master-sertifikasi', 'fa fa-file-text-o', 0, 'kode_sertifikasi', 'view,edit,del', 4, '2019-08-31 04:17:36', '2019-08-31 04:18:45', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan'),
	(5, 'Master Retribusi', 'master-sts', 'fa fa-calculator', 0, 'kode-sts', 'view,edit,del', 5, '2019-08-31 09:08:19', '2019-08-31 09:20:47', 'Admin Dinas Perkebunan', 'Admin Dinas Perkebunan'),
	(6, 'Master Pelabelan', 'master-label', 'fa fa-bookmark-o', 0, 'kode-label', 'view,edit,del', 6, '2022-03-09 21:25:10', '0000-00-00 00:00:00', 'Admin Dinas Perkebunan', ''),
	(7, 'Setting', '', 'fa fa-wrench', 0, 'setting', 'view', 7, '2018-04-20 09:19:49', '2018-05-02 12:47:51', '', 'Moch Wahyu Sugiarto'),
	(8, 'User', 'user', 'fa fa-user', 7, 'user', 'view,add,edit,del', 8, '2018-04-20 10:29:57', '0000-00-00 00:00:00', '', ''),
	(9, 'Master Menu', 'menu', 'fa fa-th-list', 7, 'menu-master', 'view,add,edit,del', 9, '2018-04-20 09:22:00', '2018-04-20 09:54:37', '', ''),
	(10, 'Group', 'user-grup', 'fa fa-users', 7, 'user-grup', 'view,add,edit,del', 10, '2018-04-20 09:23:34', '2018-04-20 09:54:06', '', '');
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_sertifikasi
DROP TABLE IF EXISTS `tbl_sertifikasi`;
CREATE TABLE IF NOT EXISTS `tbl_sertifikasi` (
  `id_sertifikasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `kode` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `no_sertifikat` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `kode_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `qrcode` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `nama_pemohon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_pemohon` text CHARACTER SET utf8 DEFAULT NULL,
  `nama_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_usaha` text CHARACTER SET utf8 DEFAULT NULL,
  `no_telp` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_sertifikat` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_komoditas` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `file_sts` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_sts` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_permohonan_sertifikat` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_pemohon` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `surat_benih` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nama_surat_benih` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `bukti_pembayaran` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `catatan` text CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `tanggal_survey` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_sertifikasi`),
  UNIQUE KEY `id_sertifikasi` (`id_sertifikasi`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_sertifikasi: ~2 rows (approximately)
DELETE FROM `tbl_sertifikasi`;
/*!40000 ALTER TABLE `tbl_sertifikasi` DISABLE KEYS */;
INSERT INTO `tbl_sertifikasi` (`id_sertifikasi`, `id_user`, `kode`, `no_sertifikat`, `kode_iup`, `qrcode`, `nama_pemohon`, `alamat_pemohon`, `nama_usaha`, `alamat_usaha`, `no_telp`, `email`, `jenis_usaha`, `jenis_sertifikat`, `jenis_komoditas`, `file_sts`, `nama_sts`, `surat_permohonan_sertifikat`, `nama_surat_pemohon`, `surat_benih`, `nama_surat_benih`, `bukti_pembayaran`, `catatan`, `created_date`, `updated_date`, `updated_by`, `tanggal_survey`, `tanggal`, `status`) VALUES
	(1, 1, 'SRF202203001', 'DISBUN12-11/II/2022', 'IUP-11/12-2022', 'http://localhost/dinas_perkebunan/qrcode-results/SRF202203001', 'Moch Wahyu Sugiarto S.Kom', 'Perum Modopuro Perkasa No 41, Ds Mojosari - Mojoketo , Jawa Timur', 'CV Angkasa Muda Digital', 'Jl Pakis Gunung 1/68 Surabaya', '089675773470', 'wahyupunk10@gmail.com', 'Badan Usaha ( Firma, CV, UD, PD)', 'Mutu Benih', 'Kakao', 'upload/sts/file_sts_1646721888.pdf', 'file_sts_1646721888.pdf', 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_1646717636.pdf', 'berkas_kelengkapan_1646717636.pdf', 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_1646718446.pdf', 'berkas_kelengkapan_1646718446.pdf', 'upload/transfer/bukti_tf_1646722521.jpeg', '- ada beberapa file yang belum lengkap\r\n- di bagian map pengajuan kop tidak jelas dan alamat', '2022-03-05 19:54:30', '2022-03-13 13:41:07', 'Super Admin', '2022-03-01', '2022-03-13', ''),
	(2, 1, 'SRF202203002', NULL, 'IUP-11/12-2022', NULL, 'Moch Wahyu Sugiarto', 'Perum Modopuro Perkasa No A-41, Ds Mojosari - Mojoketo , Jawa Timur', 'CV Angkasa Muda Digital', 'Jl Pakis Gunung 1/68 Surabaya', '089675773470', 'wahyupunk10@gmail.com', 'Badan Usaha ( Firma, CV, UD, PD)', NULL, 'Tembakau', NULL, NULL, 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_1647286654.pdf', 'berkas_kelengkapan_1647286654.pdf', 'upload/kelengkapan_sertifikasi/berkas_kelengkapan_16472866541.pdf', 'berkas_kelengkapan_16472866541.pdf', NULL, NULL, '2022-03-15 02:37:34', '2022-03-15 02:37:34', NULL, NULL, '2022-03-15', 'Pengajuan');
/*!40000 ALTER TABLE `tbl_sertifikasi` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_sertifikat
DROP TABLE IF EXISTS `tbl_sertifikat`;
CREATE TABLE IF NOT EXISTS `tbl_sertifikat` (
  `id_sertifikat` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_sertifikat`),
  UNIQUE KEY `id_sertifikat` (`id_sertifikat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_sertifikat: ~3 rows (approximately)
DELETE FROM `tbl_sertifikat`;
/*!40000 ALTER TABLE `tbl_sertifikat` DISABLE KEYS */;
INSERT INTO `tbl_sertifikat` (`id_sertifikat`, `nama`) VALUES
	(1, 'Kebun Benih Sumber'),
	(2, 'Mutu Benih'),
	(3, 'Laboratorium');
/*!40000 ALTER TABLE `tbl_sertifikat` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_usaha
DROP TABLE IF EXISTS `tbl_usaha`;
CREATE TABLE IF NOT EXISTS `tbl_usaha` (
  `id_usaha` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_usaha`),
  UNIQUE KEY `id_usaha` (`id_usaha`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_usaha: ~4 rows (approximately)
DELETE FROM `tbl_usaha`;
/*!40000 ALTER TABLE `tbl_usaha` DISABLE KEYS */;
INSERT INTO `tbl_usaha` (`id_usaha`, `nama`) VALUES
	(1, 'Instansi Pemerintah (Dinas, Puslit, Balit, Dll)'),
	(2, 'Badan Hukum ( PT, Yayasan, Koperasi )'),
	(3, 'Badan Usaha ( Firma, CV, UD, PD)'),
	(4, 'Perseorangan (Nama individu, KT, Asosiasi,Gapoktan )');
/*!40000 ALTER TABLE `tbl_usaha` ENABLE KEYS */;

-- Dumping structure for table dinas_perkebunan_db.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_iup` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `no_surat` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `tgl_surat` date DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(300) CHARACTER SET utf8 DEFAULT '0',
  `folder` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Folder untuk multiple image',
  `nama` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `nama_lengkap` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `alamat` text CHARACTER SET utf8 DEFAULT NULL,
  `nama_usaha` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `alamat_usaha` text CHARACTER SET utf8 DEFAULT NULL,
  `telp` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `status_berkas` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `ip_address` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_by` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `catatan` text CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dinas_perkebunan_db.tbl_user: ~2 rows (approximately)
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id_user`, `kode_iup`, `no_surat`, `tgl_surat`, `email`, `password`, `folder`, `nama`, `nama_lengkap`, `alamat`, `nama_usaha`, `alamat_usaha`, `telp`, `latitude`, `longitude`, `status`, `status_berkas`, `ip_address`, `user_agent`, `created_by`, `created_date`, `updated_date`, `catatan`) VALUES
	(1, 'IUP-11/12-2022', '12345', '2022-03-10', 'wahyupunk10@gmail.com', 'd2FoeXU=', '1646656785', 'Moch Wahyu Sugiarto', 'Moch Wahyu Sugiarto', 'Perum Modopuro Perkasa No A-41, Ds Mojosari - Mojoketo , Jawa Timur', 'CV Angkasa Muda Digital', 'Jl Pakis Gunung 1/68 Surabaya', '089675773470', '-7.285968', '112.726100', 'Aktif', 'Approve', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 'Admin Dinas Perkebunan', '0000-00-00', '2022-03-10', '- tes'),
	(2, '', '', NULL, 'satrio.job@gmail.com', 'cmFoYXNpYQ==', '', 'satrio anggardha', 'satrio anggardha', 'tenggilis mejoyo', 'asd', 'jl. jalan', '123', '-7.319430', '112.755386', 'Aktif', 'Belum Approve', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 'Super Admin', '0000-00-00', '2022-03-13', '');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
